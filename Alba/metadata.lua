return PlaceObj('ModDef', {
	'title', "Alba",
	'description', "A white planet, Alba, for a new challenge in a cold region.\n\nFurther gameplay and balance changes and even new buildings and resources may be added depending on my time, suggestions are welcome.\n\nAdded:\n+ Bonfire - used for relaxation, increases the cold resistance after use (available in all regions)\n+ Snowman kicking - relaxation and a way to destroy those snowmen (available in all regions)\n+ Geysers - will increase the temperature around them and damage nearby creatures when they erupt\n- Energy Crystals grow only around those and can't be planted on fields, only on hydroponic racks\n\nChanges:\n- warmer clothes equipped at start, mainly spaceship set\n- changed taming food for Ulfens and Drakkas on Alba\n\n[url=paypal.me/LukeHro][b]Paypal Support[/b][/url]",
	'image', "Mod/LH_Alba/UI/Alba.png",
	'last_changes', "Removed change for torches, minor changes",
	'id', "LH_Alba",
	'author', "lukeh_ro",
	'version_major', 1,
	'version_minor', 22,
	'version', 12,
	'lua_revision', 233360,
	'saved_with_revision', 352677,
	'entities', {
		"Bonfire",
		"Geyser",
	},
	'code', {
		"Code/Alba.lua",
		"Code/AlbaVegetation.lua",
		"Code/AlbaGeysers.lua",
		"Building/FarmField_FrostTree.lua",
		"Building/FarmField_Snowmelon.lua",
		"Building/FarmField_ArcticCactus.lua",
		"Building/FarmField_Iceworm.lua",
		"Building/FarmField_Berrycone.lua",
		"Building/FarmField_Winterleaf.lua",
		"Building/FieldFarm_Wintergrass.lua",
		"Entities/Bonfire.lua",
		"Building/Bonfire.lua",
		"Entities/Geyser.lua",
	},
	'has_data', true,
	'saved', 1740903745,
	'code_hash', -8714235815761999661,
	'steam_id', "3302266996",
	'TagGameplay', true,
	'TagOther', true,
	'TagPlants', true,
})