local function Alba_InitVegetation()
    Plants["Siliconleaf"].AlbaPlant = true
    Plants["GlitterCap"].AlbaPlant = true
    Plants["Smokeleaf"].AlbaPlant = true
    Plants["Skinbark"].AlbaPlant = true
    Plants["Heptagonia"].AlbaPlant = true
    Plants["EnergyCrystals"].AlbaPlant = true
    Plants["FrostTree"].AlbaPlant = true
    Plants["ArcticCactus"].AlbaPlant = true
    Plants["Snowmelon"].AlbaPlant = true
    Plants["Iceworm"].AlbaPlant = true
    Plants["Berrycone"].AlbaPlant = true
    Plants["Winterleaf"].AlbaPlant = true
    Plants["Wintergrass"].AlbaPlant = true
end

function OnMsg.LoadGame()
    Alba_InitVegetation()
end

function Alba_ReplaceVegetation(settings)
    Alba_InitVegetation()

    local vegetation_random = BraidRandomCreate(settings.seed_text)

    TerrainRestoreSuspended = true

    MapForEach("map", "Plant", function(obj)
        if not obj:GetPlantDef() then
            obj:delete()
        end
    end)

    MapForEach("map", "Tree", function(obj)
        obj:delete()
    end)
    MapForEach("map", "Bush", function(obj)
        if obj:GetPlantDef().id ~= "Siliconleaf" then
            obj:delete()
        end
    end)
    MapForEach("map", "ForestDebris", function(obj)
        local rand = vegetation_random(100)
        if rand > 50 then
            local plant = PlacePlant("FrostTree", obj:GetPos(), obj:GetAngle(), true, false)
            plant:SetGameFlags(const.gofPermanent)
            PlantSetTimeAlive(plant, vegetation_random(Plants["FrostTree"].GrowthTime))
            obj:delete()
        end
    end)
    MapForEach("map", "Plant", function(obj)
        if obj:GetPlantDef().id == "Heptagonia" then
            local rand = vegetation_random(100)
            if rand > 75 then
                local plant = PlacePlant("EnergyCrystals", obj:GetPos(), obj:GetAngle(), true, false)
                plant:SetGameFlags(const.gofPermanent)
                PlantSetTimeAlive(plant, vegetation_random(Plants["EnergyCrystals"].GrowthTime))
            end
            obj:delete()
        end
    end)
    MapForEach("map", "Plant", function(obj)
        if obj:GetPlantDef().id == "Clothblossom" then
            local rand = vegetation_random(100)
            if rand > 75 then
                local plant = PlacePlant("ArcticCactus", obj:GetPos(), obj:GetAngle(), true, false)
                plant:SetGameFlags(const.gofPermanent)
                PlantSetTimeAlive(plant, vegetation_random(Plants["ArcticCactus"].GrowthTime))
            end
            obj:delete()
        elseif obj:GetPlantDef().id == "Buttermelon" then
            local plant = PlacePlant("Snowmelon", obj:GetPos(), obj:GetAngle(), true, false)
            plant:SetGameFlags(const.gofPermanent)
            PlantSetTimeAlive(plant, vegetation_random(Plants["Snowmelon"].GrowthTime))
            obj:delete()
        elseif obj:GetPlantDef().id == "Smokeleaf" then
            local plant = PlacePlant("Berrycone", obj:GetPos(), obj:GetAngle(), true, false)
            plant:SetGameFlags(const.gofPermanent)
            PlantSetTimeAlive(plant, vegetation_random(Plants["Berrycone"].GrowthTime))
            obj:delete()
        elseif obj:GetPlantDef().id == "Beefberries" then
            local plant = PlacePlant("Heptagonia", obj:GetPos(), obj:GetAngle(), true, false)
            plant:SetGameFlags(const.gofPermanent)
            PlantSetTimeAlive(plant, vegetation_random(Plants["Heptagonia"].GrowthTime))
            obj:delete()
        elseif obj:GetPlantDef().id == "GrainGrass" or obj:GetPlantDef().id == "GrainGrass_Wild" or obj:GetPlantDef().id == "Graincob" then
            local plant = PlacePlant("Iceworm", obj:GetPos(), obj:GetAngle(), true, false)
            plant:SetGameFlags(const.gofPermanent)
            PlantSetTimeAlive(plant, vegetation_random(Plants["Iceworm"].GrowthTime))
            obj:delete()
        elseif obj:GetPlantDef().id == "GiantGrass" or obj:GetPlantDef().id == "BladeGrass" then
            local rand = vegetation_random(100)
            if rand > 50 then
                local plant = PlacePlant("Winterleaf", obj:GetPos(), obj:GetAngle(), true, false)
                plant:SetGameFlags(const.gofPermanent)
                PlantSetTimeAlive(plant, vegetation_random(Plants["Winterleaf"].GrowthTime))
                obj:delete()
            else
                local plant = PlacePlant("Wintergrass", obj:GetPos(), obj:GetAngle(), true, false)
                plant:SetGameFlags(const.gofPermanent)
                PlantSetTimeAlive(plant, vegetation_random(Plants["Wintergrass"].GrowthTime))
                obj:delete()
            end
        end
    end)
    TerrainRestoreSuspended = false
end

function Alba_FixVegetation()
    if Game.region == "Alba" then
        TerrainRestoreSuspended = true
        MapForEach("map", "Plant", function(obj)
            if not obj:GetPlantDef().AlbaPlant then
                obj:delete()
            end
        end)
        TerrainRestoreSuspended = false
    end
end

function OnMsg.SurvivorsPlaced()
    Alba_InitVegetation()
    Alba_FixVegetation()
end

--avoid getting unwanted fields as start game bonus
local CompleteResearch_Orig = CompleteResearch
function CompleteResearch(tech_id, player, unit)
    if Game.region == "Alba" and IsTechFieldResearch(tech_id) then
        if tech_id == "FieldTreeTeaball" then
            return CompleteResearch_Orig("FieldFrostTree", player, unit)
        elseif tech_id == "FieldButtermelon" then
            return CompleteResearch_Orig("FieldSnowmelon", player, unit)
        elseif tech_id == "FieldCactusBarrel" then
            return CompleteResearch_Orig("FieldArcticCactus", player, unit)
        elseif tech_id == "FieldSynthplant" then
            return CompleteResearch_Orig("FieldIceworm", player, unit)
        elseif tech_id == "FieldMulchtube" then
            return CompleteResearch_Orig("FieldBerrycone", player, unit)
        elseif tech_id == "FieldPurpleLeaf" then
            return CompleteResearch_Orig("FieldWinterleaf", player, unit)
        elseif tech_id == "FieldBladegrass" then
            return CompleteResearch_Orig("FieldWintergrass", player, unit)
        end
    end
    return CompleteResearch_Orig(tech_id, player, unit)
end

function OnMsg.FieldResearchCompleted(unit, target, tech)
    if Game.region == "Alba" and tech.id == "FieldEnergyCrystals" then
        AddPresetLockStateReason("BuildingCompositeDef", "Farms", "FarmField_EnergyCrystals", "hidden", "AlbaGeysers")
        AddPresetLockStateReason("BuildingCompositeDef", "Farms", "FarmField_EnergyCrystals", "locked", "AlbaGeysers")
    end
end

function SavegameFixups.TerrainRecoveryCompatibility()
    Alba_InitVegetation()
    Alba_FixVegetation()
end

local function ChangePlantsGrowthTemperature()
    local isAlba = Game.region == "Alba"
    for plant, plant_def in pairs(Plants) do
        plant_def.vanilla_AcceptableTemperatureMin = plant_def.vanilla_AcceptableTemperatureMin or plant_def.AcceptableTemperatureMin
        plant_def.vanilla_GrowthTemperatureMin = plant_def.vanilla_GrowthTemperatureMin or plant_def.GrowthTemperatureMin
        if (plant_def.vanilla_GrowthTemperatureMin > -10) then
            plant_def.AcceptableTemperatureMin = isAlba and -30 or plant_def.vanilla_AcceptableTemperatureMin
            plant_def.GrowthTemperatureMin = isAlba and (plant == "EnergyCrystals" and plant_def.vanilla_GrowthTemperatureMin
                                                    or plant == "Heptagonia" and -16
                                                    or plant == "Siliconleaf" and -14
                                                    or plant == "Skinbark" and -12
                                                    or -10)
                    or plant_def.vanilla_GrowthTemperatureMin
        end

    end
    PlantsReloadDefs(Plants)
end

--avoid find those with NaturalCuriosity trait in expeditions
local function LockReplacedFieldTechs()
    if Game.region == "Alba" then
        UIPlayer.lock_states.Tech.Field.FieldTreeTeaball = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldButtermelon = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldCactusBarrel = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldSynthplant = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldMulchtube = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldPurpleLeaf = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldBladegrass = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldGraingrass = "alba_locked"
        UIPlayer.lock_states.Tech.Field.FieldGraincob = "alba_locked"
    end
end

local function ChangeCrops()
    Resources.CactusFruit.SpecificToRegions.Alba = true
    Resources.Potato.SpecificToRegions.Alba = true
    Resources.Tomato.SpecificToRegions.Alba = true
    Resources.CactusFruit.SpecificToRegions.Alba = true
    --add cactus related
    if Game.region == "Alba" then
        table.insert_unique(Resources.CactusFruit.in_groups, "Fruits")
        table.insert_unique(GroupResourceIds.Fruits, "CactusFruit")
        GroupResourceIds.Fruits.CactusFruit = true
    else
        table.remove_value(Resources.CactusFruit.in_groups, "Fruits")
        table.remove_value(GroupResourceIds.Fruits, "CactusFruit")
        GroupResourceIds.Fruits.CactusFruit = false
    end
end

OnMsg.GameTimeStart = ChangePlantsGrowthTemperature
OnMsg.PostLoadGame = ChangePlantsGrowthTemperature

OnMsg.GameTimeStart = LockReplacedFieldTechs
OnMsg.PostLoadGame = LockReplacedFieldTechs

OnMsg.GameTimeStart = ChangeCrops
OnMsg.PostLoadGame = ChangeCrops