DefineClass.Geyser = {
    __parents = {"SyncObject", "UpdateObject", "ObstructsConstruction"},
    flags = {gofSnowDisabled = true, gofPermanent = true, efCollision = false, efWalkable = false, efApplyToGrids = false, efSelectable = false},
    allowed_eruption = 0,
    end_eruption = 0,
}

function Geyser:GameInit()
    self:CreateHeatSource()
    self.allowed_eruption = const.HourDuration * self:Random(5)
    PlayFX("Geyser_Steam", "start", self)
end

function Geyser:CreateHeatSource()
    local heatSrc = PlaceObjectAt(g_Classes.HeatSource, nil, self:GetPos())
    heatSrc:MakeSync()
    heatSrc:SetProperty("PowerTemperature", 40000)
    heatSrc:SetProperty("RadiationTemperature", 40000)
    heatSrc:SetProperty("RadiationRange", 6000)
    self:Attach(heatSrc)
    self.heat_source = heatSrc
end

function Geyser:OnObjUpdate(t, dt)
    if self.damage_thread and self.end_eruption == 0 then
        DeleteThread(self.damage_thread)
        self.damage_thread = nil
    end
    if self.allowed_eruption <= GameTime() then
        if self:Random(100) < 10 then
            self:Erupt()
        end
    end
end

function Geyser:Erupt()
    self.damage_thread = CreateGameTimeThread(function(self, weapon_def)
        PlayFX("Geyser_Water", "start", self)
        self.allowed_eruption = GameTime() + 6 * const.HourDuration
        self.end_eruption = GameTime() + weapon_def.AttackCooldown * 5
        local attack_cooldown, cooldown_rand_pct = weapon_def.AttackCooldown, weapon_def.AttackCooldownRand
        local rand_cooldown = attack_cooldown * cooldown_rand_pct / 100
        local rand_sleep = 0
        local seed = rand_cooldown > 0 and self:RandSeed("attack")
        while GameTime() <= self.end_eruption do
            SuspendPassEdits("Geyser")
            WorldChangeBegin("Geyser")
            DoDamageCone(weapon_def, self, self, weapon_def.AttackRange, nil, nil,
                    function(obj) return not obj:IsKindOf("Plant") end)
            WorldChangeEnd("Geyser")
            ResumePassEdits("Geyser")
            if seed then
                rand_sleep, seed = BraidRandom(seed, rand_cooldown)
            end
            Sleep(Max(0, attack_cooldown + rand_sleep))
        end
        PlayFX("Geyser_Water", "end", self)
        self.end_eruption = 0
    end, self, Resources[self:GetAttackWeapons()])
end

function Geyser:GetAttackWeapons()
    return "GeyserBurn"
end

function Geyser:SelectWeapon(...)
    return self:GetAttackWeapons()
end

function Geyser:OnAttackSuccess(target)
end

function Geyser:CanBeDamaged()
    return false
end

function Geyser:CanBeAttacked(attacker)
    return false
end

function Geyser:GetHitEffects(target, weapon_def)
end

function Geyser:IsObstructing(box)
    local radius = MulDivRound(1000, self:GetScale(), 100)
    return box:Dist2D2(self) < radius * radius
end

function PlaceGeyser(pos, random_func)
    local geyser = PlaceObject("Geyser")
    pos = pos:SetZ(terrain.GetHeight(pos))
    terrain.SetHeightCircle(pos, 1000, 2000,  pos:z())
    terrain.SetHeightCircle(pos, 400, 800,  pos:z() - 100)
    ChangeTerrainToSoilInCircle(pos, 1000, 3000)
    geyser:SetPos(pos)
    if random_func then
        geyser:SetAngle(random_func(360*60))
    end
    return geyser
end

function Alba_AddGeysers(settings)
    local geyser_random = BraidRandomCreate(settings.seed_text)
    TerrainRestoreSuspended = true
    MapForEach("map", "Rock", function(obj)
        if not obj:IsKindOf("MineableRock") then
            obj:delete()
        end
    end)
    MapForEach("map", "Plant", function(obj)
        if obj:GetPlantDef().id == "EnergyCrystals" then
            local pos = obj:GetPos()
            if not MapFindNearest(pos, pos, 5000, "Geyser") then
                local dx = geyser_random(3000)
                local dy = geyser_random(3000)
                local new_pos = pos:SetX(pos:x() + (-1 * geyser_random(2)) * (1100 + dx)):SetY(pos:y() + (-1 * geyser_random(2)) * (1100 + dy))
                if not MapFindNearest(new_pos, new_pos, 4000, "Geyser") then
                    PlaceGeyser(new_pos, geyser_random)
                end
            end
        end
    end)
    MapForEach("map", "Plant", function(obj)
        if obj:GetPlantDef().id == "EnergyCrystals" then
            if MapFindNearest(obj, obj, 1000, "Geyser") or not MapFindNearest(obj, obj, 5000, "Geyser") then
                obj:delete()
            end
        end
    end)
    MapForEach("map", "Geyser", function(obj)
        local box = obj:GetSurfacesBBox(EntitySurfaces.Collision)
        MapForEach(obj, 6000, "Rock", function(obj)
            if obj:IsObstructing(box) then
                obj:delete()
            end
        end)
    end)
    TerrainRestoreSuspended = false
end

function OnMsg.SurvivorsPlaced()
    TerrainRestoreSuspended = true
    MapForEach("map", "Geyser", function(obj)
        local pos = obj:GetPos()
        if terrain.GetWaterHeight(pos) > terrain.GetHeight(pos) then
            obj:delete()
        end
    end)
    TerrainRestoreSuspended = false
end

function SavegameFixups.AddGeysers()
    CreateRealTimeThread(function()
        if WaitQuestion(terminal.desktop, T("Alba Update: Geysers"),
                T("Add geysers to the current map?\n(around Energy Crystals)"),
                T("Yes"), T("No")) == "ok"
        then
            Alba_AddGeysers(Game)
        else
            return
        end
    end)
end