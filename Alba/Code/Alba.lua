function Alba_InitLightModel()
    SnowCover = 255
    FrostCover = 255
    AdvanceTerrainSeason(nil, 255)
    UpdateLightModel(0)
    terrain.InvalidateType()
end

PeriodicRepeatInfo["Alba_SnowUpdate"] = false
MapGameTimeRepeat("Alba_SnowUpdate", const.HourDuration / 4, function(dt)
    SnowCover = 255
    FrostCover = 255
end)

local function GainsBetterColdResistance(equipped, candidate)
    local currentMod = equipped and table.find_value(equipped.Modifiers, "prop", "TemperatureLow")
    return not currentMod or currentMod.add >= table.find_value(candidate.Modifiers, "prop", "TemperatureLow").add
end

local ApplyDef_Human = Human.ApplyDef
function Human:ApplyDef(forced)
    local result = ApplyDef_Human(self, forced)
    local def = self.human_def
    if def and Game.region == "Alba" and not self:HasUnitTag("Android") then
        if GainsBetterColdResistance(self:GetEquipment("Jacket"), Resources["Jacket_Spaceship"]) then
            self:Unequip("Jacket")
            UnitEquipment.Equip(self, "Jacket_Spaceship")
        end
        if GainsBetterColdResistance(self:GetEquipment("Pants"), Resources["Pants_Spaceship"]) then
            self:Unequip("Pants")
            UnitEquipment.Equip(self, "Pants_Spaceship")
        end
        if GainsBetterColdResistance(self:GetEquipment("Shirt"), Resources["Shirt_Heavy_Good"]) then
            self:Unequip("Shirt")
            UnitEquipment.Equip(self, "Shirt_Heavy_Good")
        end
        if GainsBetterColdResistance(self:GetEquipment("Hat"), Resources["Hat_Light_Good"]) then
            self:Unequip("Hat")
            UnitEquipment.Equip(self, "Hat_Light_Good")
        end
    end
    return result
end

local Init_UnitAnimal = UnitAnimal.Init
function UnitAnimal:Init()
    local result = Init_UnitAnimal(self)
    if Game.region == "Alba" then
        if self:IsKindOf("Ulfen") then
            self.TamingFood = "Tomato"
        end
        if self:IsKindOf("Draka") then
            self.TamingFood = "CactusFruit"
        end
    end
    return result
end

AppendClass.Snowman = {
    __parents = {"RelaxationObject"},
    melting_speed = 6000,
}

local vanilla_GetPosForSnowman = GetPosForSnowman
function GetPosForSnowman(...)
    return MapCount("map", "Snowman") < 5 and vanilla_GetPosForSnowman(...)
end

local OnObjUpdate_Snowman = Snowman.OnObjUpdate
function Snowman:OnObjUpdate(t, dt)
    if self.Kicked then
        local melted = self.melting_speed * dt / const.HourDuration
        self:ChangeHealth(-melted)
    end
    return OnObjUpdate_Snowman(self, t, dt)
end

function Snowman:ChangeHealth(change, reason)
    if (reason == "combat") then
        self.Kicked = true
        return IntegrityProps.ChangeHealth(self, -self.melting_speed, reason)
    end
    return IntegrityProps.ChangeHealth(self, change, reason)
end

function SavegameFixups.ChangeUlfenTamingFood()
    if Game.region == "Alba" then
        MapForEach("map", "Ulfen", function(obj) obj.TamingFood = "Tomato" end)
        MapForEach("map", "Draka", function(obj) obj.TamingFood = "CactusFruit" end)
    end
end