UndefineClass('FarmField_Snowmelon')
DefineClass.FarmField_Snowmelon = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_FoodFarms",
	display_name = T(427627999410, --[[ModItemBuildingCompositeDef FarmField_Snowmelon display_name]] "Snowmelon field"),
	menu_display_name = T(177739445620, --[[ModItemBuildingCompositeDef FarmField_Snowmelon menu_display_name]] "Snowmelon"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_buttermelon",
	BuildMenuPos = 30,
	display_name_pl = T(433738728694, --[[ModItemBuildingCompositeDef FarmField_Snowmelon display_name_pl]] "Snowmelon fields"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "Snowmelon",
	InvulnerableComponent = true,
}

