UndefineClass('FieldFarm_Wintergrass')
DefineClass.FieldFarm_Wintergrass = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_BushFarms",
	display_name = T(205237496057, --[[ModItemBuildingCompositeDef FieldFarm_Wintergrass display_name]] "Wintergrass field"),
	menu_display_name = T(111696430258, --[[ModItemBuildingCompositeDef FieldFarm_Wintergrass menu_display_name]] "Wintergrass"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_blade_grass",
	display_name_pl = T(775980530554, --[[ModItemBuildingCompositeDef FieldFarm_Wintergrass display_name_pl]] "Wintergrass fields"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "Wintergrass",
	InvulnerableComponent = true,
}

