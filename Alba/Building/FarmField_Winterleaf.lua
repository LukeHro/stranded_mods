UndefineClass('FarmField_Winterleaf')
DefineClass.FarmField_Winterleaf = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_BushFarms",
	display_name = T(772795699215, --[[ModItemBuildingCompositeDef FarmField_Winterleaf display_name]] "Winterleaf field"),
	menu_display_name = T(509061699650, --[[ModItemBuildingCompositeDef FarmField_Winterleaf menu_display_name]] "Winterleaf"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_purpleleaf",
	BuildMenuPos = 2,
	display_name_pl = T(992925093350, --[[ModItemBuildingCompositeDef FarmField_Winterleaf display_name_pl]] "Winterleaf fields"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "Winterleaf",
	InvulnerableComponent = true,
}

