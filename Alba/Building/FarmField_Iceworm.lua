UndefineClass('FarmField_Iceworm')
DefineClass.FarmField_Iceworm = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_IndustrialFarms",
	display_name = T(880618228439, --[[ModItemBuildingCompositeDef FarmField_Iceworm display_name]] "Iceworm field"),
	menu_display_name = T(671413007933, --[[ModItemBuildingCompositeDef FarmField_Iceworm menu_display_name]] "Iceworm"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_synthplant",
	BuildMenuPos = 30,
	display_name_pl = T(407371403600, --[[ModItemBuildingCompositeDef FarmField_Iceworm display_name_pl]] "Iceworm fields"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "Iceworm",
	InvulnerableComponent = true,
}

