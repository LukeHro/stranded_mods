UndefineClass('FarmField_Berrycone')
DefineClass.FarmField_Berrycone = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_FoodFarms",
	display_name = T(843159450917, --[[ModItemBuildingCompositeDef FarmField_Berrycone display_name]] "Berrycone field"),
	menu_display_name = T(186614670417, --[[ModItemBuildingCompositeDef FarmField_Berrycone menu_display_name]] "Berrycone"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_mulchtube",
	BuildMenuPos = 1030,
	display_name_pl = T(587343551870, --[[ModItemBuildingCompositeDef FarmField_Berrycone display_name_pl]] "Berrycone fields"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "Berrycone",
	InvulnerableComponent = true,
}

