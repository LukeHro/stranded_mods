UndefineClass('FarmField_ArcticCactus')
DefineClass.FarmField_ArcticCactus = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_FoodFarms",
	display_name = T(449366238126, --[[ModItemBuildingCompositeDef FarmField_ArcticCactus display_name]] "Arctic cactus field"),
	menu_display_name = T(845464728963, --[[ModItemBuildingCompositeDef FarmField_ArcticCactus menu_display_name]] "Arctic cactus"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_barrel_cactus",
	BuildMenuPos = 50,
	display_name_pl = T(702962641047, --[[ModItemBuildingCompositeDef FarmField_ArcticCactus display_name_pl]] "Arctic cactus fields"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "ArcticCactus",
	InvulnerableComponent = true,
}

