UndefineClass('FarmField_SnowCactus')
DefineClass.FarmField_SnowCactus = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_FoodFarms",
	display_name = T(213680704851, --[[ModItemBuildingCompositeDef FarmField_SnowCactus display_name]] "Snow cactus field"),
	menu_display_name = T(425030695096, --[[ModItemBuildingCompositeDef FarmField_SnowCactus menu_display_name]] "White cactus"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_barrel_cactus",
	BuildMenuPos = 50,
	display_name_pl = T(424179021956, --[[ModItemBuildingCompositeDef FarmField_SnowCactus display_name_pl]] "Snow cactus fields"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "WhiteCactus",
	InvulnerableComponent = true,
}

