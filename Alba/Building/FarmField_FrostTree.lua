UndefineClass('FarmField_FrostTree')
DefineClass.FarmField_FrostTree = {
	__parents = { "FieldBase", "InvulnerableComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",

	flags = { efAttackable = false, gofDamageable = false, },

	object_class = "FieldBase",
	LockState = "hidden",
	BuildMenuCategory = "sub_TreeFarms",
	display_name = T(809166617163, --[[ModItemBuildingCompositeDef FarmField_FrostTree display_name]] "Frost forest"),
	menu_display_name = T(462628257377, --[[ModItemBuildingCompositeDef FarmField_FrostTree menu_display_name]] "Frost trees"),
	BuildMenuIcon = "UI/Icons/Build Menu/Plants/plant_teaball_tree",
	BuildMenuPos = 35,
	display_name_pl = T(441379731007, --[[ModItemBuildingCompositeDef FarmField_FrostTree display_name_pl]] "Frost forests"),
	can_be_deconstructed = false,
	Health = 0,
	MaxHealth = 0,
	can_be_copied = false,
	SkirtSize = 0,
	EntityHeight = 0,
	can_be_moved = false,
	affected_by_disasters = set(),
	Crop = "FrostTree",
	InvulnerableComponent = true,
}

