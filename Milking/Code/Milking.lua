AppendClass.UnitAnimal = {
    milking_allowed = true
}

function OnMsg.ModsReloaded()
    PlaceObj('AnimalPerk', {
        Description = T([[Can be milked once a day.
*Milk can be collected from tamed, mature females.]]),
        DisplayName = T("Milk producer"),
        id = "MilkProducer",
    })
    table.insert_unique(Ulfen.AnimalPerks, "MilkProducer")
    table.insert_unique(Camel.AnimalPerks, "MilkProducer")
    table.insert_unique(Noth.AnimalPerks, "MilkProducer")

    Presets.ActivitySet.Default.Work.Activities.Milking = true
    table.insert_unique(Presets.ActivityGroup.Default.Ranch.Activities, "Milking")
end

function OnMsg.ModsReloaded()
    if table.find(ModsLoaded, "id", "kfWMs8F") then
        print("Warning... mod Animal ULFEN and CAMEL produce MILK loaded, there would be conflicts...")
    end
end

local OnObjUpdate_UnitAnimal = UnitAnimal.OnObjUpdate
function UnitAnimal:OnObjUpdate(...)
    if not self:CanBeMilked() then
        self:CancelMilkingActivity()
    else
        self:CreateMilkingActivityObject()
    end
    return OnObjUpdate_UnitAnimal(self, ...)
end

local GatherDirectOrders_UnitAnimal = UnitAnimal.GatherDirectOrders
function UnitAnimal:GatherDirectOrders(unit, orders, ...)
    if self:CanBeMilked() and self.milking_allowed then
        CreateDirectOrderButtons(orders, DirectOrders.MilkAnimal, self)
    end
    return GatherDirectOrders_UnitAnimal(self, unit, orders, ...)
end

function UnitAnimal:CanBeMilked()
    if self:IsDead() or self:IsUnconscious() or self.berserk_state or not self:IsTamed() then
        return
    end
    if self.Gender == "female" and not self:IsGrowingUp() and table.find(self.AnimalPerks, "MilkProducer") then
        return true
    end
end

function UnitAnimal:CreateMilkingActivityObject(player)
    if not (player or self.player) then
        return --not tamed
    end
    local activity_obj = self:GetMilkingActivityObject()
    if IsValid(activity_obj) then
        return activity_obj
    end
    self.milking_cooldown = GameTime() + const.DayDuration - 2 * const.HourDuration
    activity_obj = CreateActivityObject("MilkingActivityObject", {
        player = player or self.player
    }, self, self:GetCenterOfSpots("Pet"))
    activity_obj:SetActivityAvailable(true)
    ObjModified(self)
    return activity_obj
end

function UnitAnimal:GetMilkingActivityObject()
    return self:GetAttach("MilkingActivityObject")
end

function UnitAnimal:CancelMilkingActivity(reset)
    local activity = self:GetMilkingActivityObject()
    if not activity then
        return
    end
    local player = activity.player
    DoneObject(activity)
    if reset then
        self:CreateMilkingActivityObject(player)
    end
    ObjModified(self)
end

DefineClass.MilkingActivityObject = {
    __parents = {
        "AnimalInteractionActivityObject"
    },
    activity_id = "Milking",
    ActivityDuration = const.HourDuration,
    remove_on_depleted_uses = true
}

function MilkingActivityObject:CanUnitPerformActivity(unit)
    local animal = self:GetActivityTarget()
    return animal.milking_cooldown < GameTime() and animal:CanBeMilked() and animal.milking_allowed and animal:CanBeInteractedWith(unit)
end

function MilkingActivityObject:Activate()
    self:SetActivityAvailable(true, "force_update")
end

function MilkingActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

function MilkingActivityObject:InteractWithAnimal(unit, activity, skill_level, efficiency)
    local animal = self:GetActivityTarget()
    animal:TrySetCommand("WaitToBeMilked", unit)
    local activity_time = MulDivRound(const.HourDuration, 100, efficiency)
    unit:PlayPrg(PrgAmbientLife.Milking, activity_time, animal)
    unit:AddActivityExperience(activity, 100)
end

function UnitAnimal:WaitToBeMilked(unit)
    self:PushDestructor(function(animal)
        animal:SetCommand("Idle")
    end)
    local eat_idle_anim = self:RandAnim(self.EatIdleAnim)
    local anim_duration = self:GetAnimDuration(eat_idle_anim)
    while unit:VisitTimeLeft() > 0 do
        self:PlayMomentTrackedAnim(eat_idle_anim, 1, nil, 200, Min(unit:VisitTimeLeft(), anim_duration))
    end
    self:PopAndCallDestructor()
end

function PrgAmbientLife.Milking(unit, target)
    unit:PushDestructor(function(unit)
        unit:PlayMomentTrackedAnim("butchering_End", 1, nil, 200, unit:GetAnimDuration("butchering_End"))
    end)
    unit:StatusUIUpdateData("activity", {
        bar1_start_time = GameTime(),
        bar1_end_time = GameTime() + unit:VisitTimeLeft()
    })
    local anim_duration = unit:GetAnimDuration("butchering_Start")
    unit:Face(target, anim_duration)
    unit:PlayMomentTrackedAnim("butchering_Start", 1, nil, 200, anim_duration)
    anim_duration = unit:GetAnimDuration("butchering_Idle")
    while unit:VisitTimeLeft() > 0 do
        unit:PlayMomentTrackedAnim("butchering_Idle", 1, nil, 200, Min(unit:VisitTimeLeft(), anim_duration))
        if unit.visit_restart then
            return
        end
    end
    PlaceResourcePile(unit, "Milk", 10 * const.ResourceScale)
    unit:PopAndCallDestructor()
end

function SavegameFixups.RemovePermanentMilkCondition()
    for _, survivor in ipairs(AllSurvivors) do
        if survivor.id ~= "Daniel" then
            survivor:RemoveHealthConditions("IrritableBowel_1_Mild")
        end
    end
end

function UnitAnimal:rfnToggleAllowedMilking(force_allow)
    local allowed = self.milking_allowed
    if force_allow ~= nil then
        self.milking_allowed = force_allow
    else
        self.milking_allowed = not allowed
    end
    if allowed then
        self:CancelMilkingActivity()
    elseif self.milking_cooldown <= GameTime() then
        self:CreateMilkingActivityObject(UIPlayer)
    end
    ObjModified(self)
end

function UnitAnimal:GetToggleAllowedMilkingTextRollover()
    return self.milking_allowed and T("Forbid survivors to milk this animal.") or T("Allow survivors to milk this animal.")
end

function OnMsg.ClassesBuilt()
    local parent = table.find_value(Presets.XTemplate["Infopanel Actions"].IPActionsAnimals, "__context_of_kind", "UnitAnimal")
    table.remove_entry(parent, "ActionId", "idToggleAllowedMilking")
    parent[#parent + 1] = PlaceObj('XTemplateAction', {
        'RolloverTemplate', "Rollover",
        'RolloverText', T("<ToggleAllowedMilkingTextRollover>"),
        'ActionId', "idToggleAllowedMilking",
        'ActionSortKey', "20",
        'ActionName', T("Milk"),
        'ActionIcon', CurrentModPath .. "UI/Icons/Infopanels/animal_allow_train",
        'ActionToolbar', "ip_actions",
        'ActionState', function (self, host)
            local obj = ResolvePropObj(host.context)
            self.ActionIcon = CurrentModPath
                    .. (obj.milking_allowed and "UI/Icons/Infopanels/animal_allow_milk" or "UI/Icons/Infopanels/animal_forbid_milk")
            return "enabled"
        end,
        'OnAction', function (self, host, source, ...)
            local obj = ResolvePropObj(host.context)
            NetSyncEvent("ObjFunc", obj, "rfnToggleAllowedMilking")
        end,
        '__condition', function (parent, context) return context:CanBeMilked() end,
    })
end

local CreateBinActions_UnitAnimalAutoResolve = UnitAnimalAutoResolve.CreateBinActions
function UnitAnimalAutoResolve.CreateBinActions(bin, host, skip_bin_actions_for_work_actions)
    XAction:new({
        ActionId = "idToggleAllowedMilking",
        ActionSortKey = "10",
        ActionName = T("Milk"),
        ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/animal_allow_train",
        ActionToolbar = "ip_actions",
        RolloverTemplate = "Rollover",
        ActionState = function(self)
            for _, obj in ipairs(bin) do
                if IsValid(obj) and obj:CanBeMilked() then
                    self.RolloverText = obj:GetToggleAllowedMilkingTextRollover()
                    self.ActionIcon = CurrentModPath
                            .. (obj.milking_allowed and "UI/Icons/Infopanels/animal_allow_milk" or "UI/Icons/Infopanels/animal_forbid_milk")
                    return "enabled"
                end
            end
            return "hidden"
        end,
        OnAction = function()
            local milkable_animals
            for _, obj in ipairs(bin) do
                if IsValid(obj) and obj:CanBeMilked() then
                    milkable_animals = table.create_add(milkable_animals, obj)
                end
            end
            if milkable_animals then
                NetSyncEvent("MultiObjFunc", milkable_animals, "rfnToggleAllowedMilking", not milkable_animals[1].milking_allowed)
            end
        end,
    }, host)
    return CreateBinActions_UnitAnimalAutoResolve(bin, host, skip_bin_actions_for_work_actions)
end

local UpdateSkillInclinations_UnitSkill = UnitSkill.UpdateSkillInclinations
function UnitSkill:UpdateSkillInclinations(...)
    if self.activity_forbidden_reasons then
        self.activity_forbidden_reasons["Milking"] = self.activity_forbidden_reasons["Ranching"]
    end
    return UpdateSkillInclinations_UnitSkill(self, ...)
end
UnitSkill.SetActivityAllowedReason = UnitSkill.UpdateSkillInclinations

function SavegameFixups.FixHopeMilking()
    if Hope then
        Hope:UpdateSkillInclinations()
    end
end