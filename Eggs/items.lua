return {
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(973308611172, --[[ModItemActivityType CollectingEggs ActivityStatusText |gender-variants]] "Collecting eggs"),
	ActivityText = T(188886155282, --[[ModItemActivityType CollectingEggs ActivityText |gender-variants]] "Collect eggs"),
	AddDeliveryToBatch = true,
	Description = T(885426506279, --[[ModItemActivityType CollectingEggs Description]] "Collect eggs from bird nests."),
	DisplayName = T(867535345761, --[[ModItemActivityType CollectingEggs DisplayName]] "Collect eggs"),
	EfficiencyBoostFromHarvestingTools = true,
	EfficiencyFromConsciousness = true,
	EfficiencyFromManipulation = true,
	EfficiencyFromSkill = 50,
	GetIPDisplayedStatusText = function (self, unit)
		return T("Collecting eggs")
	end,
	PickClosest = true,
	QueueStatusText = T(783467619884, --[[ModItemActivityType CollectingEggs QueueStatusText |gender-variants]] "Collect eggs"),
	Skill = "Farming",
	SortKey = 2605,
	StatusIcon = "UI/Hud/st_eggs.dds",
	id = "CollectingEggs",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(691422900010, --[[ModItemActivityType BoilingEggs ActivityStatusText |gender-variants]] "Boiling"),
	ActivityText = T(734696139800, --[[ModItemActivityType BoilingEggs ActivityText |gender-variants]] "Boil <target.DisplayName>"),
	AddDeliveryToBatch = true,
	Description = T(459917963778, --[[ModItemActivityType BoilingEggs Description]] "Processing raw ingredients into cooked meals."),
	DisplayName = T(628459016198, --[[ModItemActivityType BoilingEggs DisplayName]] "Boil"),
	EfficiencyFromManipulation = true,
	EfficiencyFromSkill = 50,
	GetActivityQueueStatusText = function (self, target, unit, param1, param2)
		local target = IsValid(target) and target:GetActivityTarget()
		if IsValid(target) then
			local recipe = Recipes[target.selected_recipe.id]
			local text = { T{116123475078, "<status> ", status = GetTByGender(self.QueueStatusText, unit) } }
			for _, output in ipairs(recipe and recipe.OutputResources) do
				text[#text + 1] = T{424460929843, "<res_icon(res)>", res = output.resource }
			end
			return table.concat(text)
		end
		return GetTByGender(self.QueueStatusText, unit)
	end,
	GetDeliveryRes = function (self, target, unit)
		-- get the resoure from the recipe output
		if not IsValid(target) then return end
		local recipe_def = Recipes[target.recipe_id]
		local output = recipe_def and recipe_def.OutputResources
		if #(output or "") == 0 then return end
		return true, output[1].resource, target:GetActivityTarget()
	end,
	GetDisplayedStatusText = function (self, unit, short)
		local activity_target = unit.current_act_target
		local target = IsValid(activity_target) and activity_target:GetActivityTarget()
		if IsValid(target) then
			local selected = target.selected_recipe
			if selected then
				local recipe = Recipes[selected.id]
				local input_icons, output_icons = {}, {}
				for _, input in ipairs(recipe and recipe.InputResources) do
					input_icons[#input_icons + 1] = T{424460929843, "<res_icon(res)>", res = input.resource }
				end
				for _, output in ipairs(recipe and recipe.OutputResources) do
					output_icons[#output_icons + 1] = T{424460929843, "<res_icon(res)>", res = output.resource }
				end
				
				if unit:IsWorkerTaskInProgress() and not unit:IsDoingActivityObject() then
					return T{550125990911, --[[|gender-variants]] "Getting <input_res> for <output_res>", input_res = table.concat(input_icons), output_res = table.concat(output_icons), TGender = unit}
				end
				return T{968433297330, "<status> <output_res>", status = GetTByGender(self.ActivityStatusText, unit), output_res = table.concat(output_icons) }
			end
		end
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PickClosest = true,
	PriorityDisplayName = T(162347105597, --[[ModItemActivityType BoilingEggs PriorityDisplayName]] "Boiling"),
	QueueStatusText = T(149342884808, --[[ModItemActivityType BoilingEggs QueueStatusText |gender-variants]] "Cook"),
	Skill = "Cooking",
	SortKey = 3300,
	StatusIcon = "UI/Hud/st_cook",
	id = "BoilingEggs",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Animals",
	BuildMenuIcon = "Images/TamedBirdNest.png",
	BuildMenuPos = 10,
	ChangeOwnerIcon = "UI/Icons/Infopanels/animal_change_owner",
	ChangeOwnerRolloverText = T(626643706445, --[[ModItemBuildingCompositeDef TamedBirdNest ChangeOwnerRolloverText]] "Decide which animal is allowed to sleep on this spot."),
	ConstructIgnore = set( "Flooring" ),
	CustomMaterial = "BedSoil",
	Filter = function (obj)
		return table.find(obj.AnimalPerks, "EggsProducer")
	end,
	ForwardDir = 5400,
	LockState = "hidden",
	MovingAs = "entity",
	OwnedComponent = true,
	OwnerLabel = "TamedAnimals",
	attack_attraction = 1,
	construction_cost = PlaceObj('ConstructionCost', {
		Hay = 10000,
		Sticks = 10000,
	}),
	construction_points = 5000,
	description = T(878935729436, --[[ModItemBuildingCompositeDef TamedBirdNest description]] "Dedicated sleeping spot for birds to sleep and lay eggs."),
	display_name = T(493865934663, --[[ModItemBuildingCompositeDef TamedBirdNest display_name]] "Bird nest"),
	display_name_pl = T(655722579213, --[[ModItemBuildingCompositeDef TamedBirdNest display_name_pl]] "Bird nests"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Gujo_Nest",
	fx_actor_base_class = "AnimalBed",
	id = "TamedBirdNest",
	labels = {
		"BirdSleepingSpot",
	},
	lock_block_box = box(-1500, -1500, 0, 1500, 1500, 700),
	lock_pass_box = box(-1500, -1500, 0, 1500, 1500, 700),
	object_class = "EggsBuilding",
	ownership_class = "AnimalBedSmall",
}),
PlaceObj('ModItemCode', {
	'name', "EggsGathering",
	'CodeFileName', "Code/EggsGathering.lua",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, ...)
		        local activity_id = self.ActivityId
		        local activity = ActivityTypes[activity_id]
		        local skill_level, efficiency = unit:GetSkillAndEfficiency(activity)
		        local activity_target = target:GetAttach("CollectingEggsActivityObject")
		        unit:EnqueueAndTryExecuteActivity(activity_id, activity_target, skill_level, efficiency, clear_queue)
	end,
	ActivityId = "CollectingEggs",
	CanTransfer = function (self, old_unit, new_unit, target, param, queue_cleared)
		        return self:GetAssignedUnit(target, param)
	end,
	EnableCanceling = false,
	GetAssignedUnit = function (self, target, param)
		        return target:GetAssignedSurvivor()
	end,
	GetButtonText = function (self, unit, target, param, status_text)
		        return GetTByGender(self.Text, unit)
	end,
	GetErrorStatus = function (self, unit, target, param)
		        if target.eggs <= 0 then
		            return T("No eggs to collect")
		        end
		        if target.reserved_by then
		            return T("Bird is using it")
		        end
				        return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	RequireUnit = true,
	Transfer = function (self, old_unit, new_unit, target, param, queue_cleared)
		        local activity_target = target:GetAttach("CollectingEggsActivityObject")
		        local activity_unit = activity_target.activity_unit or activity_target.enqueued
		        if activity_unit == target.assigned_unit then
		            activity_target.transferring = true
		        end
		        self:Activate(new_unit, target, param, queue_cleared)
	end,
	id = "CollectingEggs",
}),
PlaceObj('ModItemEntity', {
	'name', "Eggs",
	'fade_category', "Never",
	'entity_name', "Eggs",
}),
PlaceObj('ModItemHealthCondition', {
	AffectableBodyParts = {
		PlaceObj('HealthConditionBodyParts', {
			BodyPart = "TorsoStomach",
			BodyPartGroup = "ChestOrgans",
			param_bindings = false,
		}),
	},
	Description = T(823466136362, --[[ModItemHealthCondition Ate_raw_eggs Description]] "An temporary condition."),
	DisplayName = T(625969754497, --[[ModItemHealthCondition Ate_raw_eggs DisplayName]] "Ate raw eggs"),
	Expiration = true,
	ExpirationRandom = 80000,
	ExpirationTime = 160000,
	FloatingTextType = "Display name",
	PainModifier = 10000,
	Type = "Disease",
	id = "Ate_raw_eggs",
}),
PlaceObj('ModItemRecipe', {
	Activity = "BoilingEggs",
	ActivityDuration = 10000,
	ActivityXPGrade = "Cooking_Low",
	BuildCategory = "RecipesQuick",
	Description = T(920423525529, --[[ModItemRecipe BoiledEggs Description]] "Boil eggs for immediate consumption."),
	DestroyOnFail = false,
	DisplayName = T(417118310950, --[[ModItemRecipe BoiledEggs DisplayName]] "Boil eggs"),
	HasIntermediateResult = false,
	Icon = "UI/Icons/Items/eggs.dds",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "Eggs",
			'amount', 2000,
		}),
	},
	LockState = "hidden",
	ManualWork = true,
	OnOutputResourceCompleted = function (self, device, unit, res, amount, res_info)
		return OnEggBoilingCompleted(self, device, unit, res, amount, res_info)
	end,
	OutputDecayFromInput = true,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "Eggs",
			'amount', 0,
		}),
	},
	RequiredDeviceInterfaces = {
		"CauldronInterface",
		"PotInterface",
	},
	SortKey = 22,
	group = "Food",
	id = "BoiledEggs",
}),
PlaceObj('ModItemRecipe', {
	Activity = "Cooking",
	ActivityDuration = 40000,
	ActivityXPGrade = "Cooking_Low",
	BuildCategory = "RecipesQuick",
	Description = T(695274662608, --[[ModItemRecipe ScotchEggs Description]] "Hard-boiled eggs wrapped in meat."),
	DestroyOnFail = false,
	DisplayName = T(562718109830, --[[ModItemRecipe ScotchEggs DisplayName]] "Scotch eggs"),
	FailChance = 20,
	GuaranteedSuccessLevel = 2,
	Icon = "Mod/LH_Eggs/UI/Icons/Items/scotch_eggs.dds",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "RawMeats",
			'amount', 10000,
		}),
		PlaceObj('ResAmount', {
			'resource', "Eggs",
			'amount', 4000,
		}),
	},
	ManualWork = true,
	OutputDecayFromInput = true,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "ScotchEggs",
			'amount', 2000,
		}),
	},
	RequiredDeviceInterfaces = {
		"CauldronInterface",
		"PotInterface",
	},
	SortKey = 10,
	group = "Food",
	id = "ScotchEggs",
}),
PlaceObj('ModItemRecipe', {
	Activity = "Cooking",
	ActivityDuration = 40000,
	ActivityXPGrade = "Cooking_Low",
	BuildCategory = "RecipesQuick",
	Description = T(390502201339, --[[ModItemRecipe CheeseOmelette Description]] "A classic breakfast."),
	DestroyOnFail = false,
	DisplayName = T(243286247623, --[[ModItemRecipe CheeseOmelette DisplayName]] "Cheese omelette"),
	FailChance = 20,
	GuaranteedSuccessLevel = 2,
	Icon = "Mod/LH_Eggs/UI/Icons/Items/cheese_omelette.dds",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "Cheese",
			'amount', 10000,
		}),
		PlaceObj('ResAmount', {
			'resource', "Eggs",
			'amount', 4000,
		}),
	},
	ManualWork = true,
	OutputDecayFromInput = true,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "CheeseOmelette",
			'amount', 2000,
		}),
	},
	Prerequisites = {
		PlaceObj('CheckExpression', {
			Expression = function (self, obj) return table.find(ModsLoaded, "id", "LH_Milk") end,
		}),
	},
	RequiredDeviceInterfaces = {
		"CauldronInterface",
		"PotInterface",
	},
	SortKey = 10,
	group = "Food",
	id = "CheeseOmelette",
}),
PlaceObj('ModItemResource', {
	ConsumerTags = set( "Human" ),
	DecayRateInside = 1042,
	DecayRateOutside = 2083,
	DecayRateRefrigerated = 208,
	DecayTimeInside = 3840000,
	DecayTimeOutside = 1920000,
	DecayTimeRefrigerated = 19200000,
	LockState = "hidden",
	MinStorageCondition = 4,
	ShowDiscoveryNotification = false,
	SortKey = 200,
	alt_icon = "UI/Icons/Items/eggs.dds",
	anim_pile_load = "standing_PickUp_HandsClose",
	anim_pile_unload = "standing_DropDown_HandsClose_High",
	carry_amount = 25000,
	carry_entity = "Eggs",
	carry_spot = "Tool",
	carry_type = "HandsClose",
	comment = "copied from palmfruits",
	consumable = true,
	decay_fx = true,
	description = T(634287579719, --[[ModItemResource Eggs description]] "Eggs collected from tamed birds."),
	display_name = T(830053559474, --[[ModItemResource Eggs display_name]] "Egg"),
	display_name_pl = T(405824716139, --[[ModItemResource Eggs display_name_pl]] "Eggs"),
	food_entity = "Meal_04",
	group = "FoodRaw",
	icon = "UI/Icons/Resources/res_eggs.dds",
	id = "Eggs",
	in_groups = {
		"Food",
		"FoodEdible",
		"FoodRaw",
		"FoodRaw_NoGroups",
	},
	money_value = 150000,
	preference = 10,
	progress = 200,
	serving_amount = 2000,
	stack_entity = "Eggs",
	stack_idle_state_count = 3,
	stack_size = 25000,
	trade_amount_multiplier = 25000,
	use_unfinished_item = "UnfinishedFood",
	vegetarian = true,
	visible = false,
}),
PlaceObj('ModItemResource', {
	Comment = "Quick",
	ConsumerTags = set( "Human" ),
	DecayRateInside = 1042,
	DecayRateOutside = 2083,
	DecayRateRefrigerated = 521,
	DecayTimeInside = 3840000,
	DecayTimeOutside = 1920000,
	DecayTimeRefrigerated = 7680000,
	LockState = "hidden",
	MinStorageCondition = 4,
	SortKey = 120,
	alt_icon = "Mod/LH_Eggs/UI/Icons/Items/scotch_eggs.dds",
	anim_pile_load = "standing_PickUp_HandsClose",
	anim_pile_unload = "standing_DropDown_HandsClose_High",
	carry_entity = "ResourceBulk_Pot",
	carry_rotation = 5400,
	carry_spot = "Tool",
	carry_type = "HandsClose",
	decay_fx = true,
	description = T(668165443527, --[[ModItemResource ScotchEggs description]] "Hard-boiled eggs wrapped in meat."),
	display_name = T(974828023490, --[[ModItemResource ScotchEggs display_name]] "Scotch eggs"),
	display_name_pl = T(317036261735, --[[ModItemResource ScotchEggs display_name_pl]] "Scotch eggs"),
	food_entity = "Meal_03",
	group = "FoodProcessed",
	happiness_factor = "AteCookedFoodGood_1",
	icon = "Mod/LH_Eggs/UI/Icons/Resources/res_scotch_eggs.dds",
	id = "ScotchEggs",
	in_groups = {
		"FoodProcessed_Quick",
		"FoodProcessed",
		"Food",
		"FoodEdible",
		"EmergencyRationMaterials",
	},
	item_metas = {
		"exquisite",
	},
	money_value = 300000,
	preference = 100,
	progress = 8000,
	quality = 25,
	serving_nutrition = 1500000,
	stack_entity = "ResourceBulk_Pot",
	use_unfinished_item = "UnfinishedFood",
	visible = false,
}),
PlaceObj('ModItemResource', {
	Comment = "Quick",
	ConsumerTags = set( "Human" ),
	DecayRateInside = 1042,
	DecayRateOutside = 2083,
	DecayRateRefrigerated = 521,
	DecayTimeInside = 3840000,
	DecayTimeOutside = 1920000,
	DecayTimeRefrigerated = 7680000,
	LockState = "hidden",
	MinStorageCondition = 4,
	Prerequisites = {
		PlaceObj('CheckExpression', {
			Expression = function (self, obj) return table.find(ModsLoaded, "id", "LH_Milk") end,
		}),
	},
	SortKey = 120,
	alt_icon = "Mod/LH_Eggs/UI/Icons/Items/cheese_omelette.dds",
	anim_pile_load = "standing_PickUp_HandsClose",
	anim_pile_unload = "standing_DropDown_HandsClose_High",
	carry_entity = "ResourceBulk_Pan",
	carry_rotation = 5400,
	carry_spot = "Tool",
	carry_type = "HandsClose",
	decay_fx = true,
	description = T(307679058474, --[[ModItemResource CheeseOmelette description]] "A classic breakfast."),
	display_name = T(481840913362, --[[ModItemResource CheeseOmelette display_name]] "Cheese omelette"),
	display_name_pl = T(130659724940, --[[ModItemResource CheeseOmelette display_name_pl]] "Cheese omelette"),
	food_entity = "Meal_03",
	group = "FoodProcessed",
	happiness_factor = "AteCookedFoodGood_1",
	icon = "Mod/LH_Eggs/UI/Icons/Resources/res_cheese_omelette.dds",
	id = "CheeseOmelette",
	in_groups = {
		"FoodProcessed_Quick",
		"FoodProcessed",
		"Food",
		"FoodEdible",
		"EmergencyRationMaterials",
	},
	item_metas = {
		"exquisite",
	},
	money_value = 300000,
	preference = 100,
	progress = 8000,
	quality = 25,
	serving_nutrition = 1500000,
	stack_entity = "ResourceBulk_Pot",
	use_unfinished_item = "UnfinishedFood",
	vegetarian = true,
	visible = false,
}),
PlaceObj('ModItemXTemplate', {
	group = "Infopanel Sections",
	id = "tabOverview_TamedBirdNest",
	PlaceObj('XTemplateTemplate', {
		'__context_of_kind', "TamedBirdNest",
		'__template', "InfopanelSection",
	}, {
		PlaceObj('XTemplateWindow', {
			'__class', "XText",
			'Padding', box(0, 5, 7, 5),
			'FoldWhenHidden', true,
			'HandleMouse', false,
			'TextStyle', "InfopanelText",
			'Translate', true,
			'Text', T(377545834149, --[[ModItemXTemplate tabOverview_TamedBirdNest Text]] "<IPEggsInfo>"),
			'HideOnEmpty', true,
		}),
		PlaceObj('XTemplateWindow', {
			'__class', "XText",
			'Padding', box(0, 5, 7, 5),
			'FoldWhenHidden', true,
			'HandleMouse', false,
			'TextStyle', "InfopanelText",
			'Translate', true,
			'Text', T(108398032253, --[[ModItemXTemplate tabOverview_TamedBirdNest Text]] "<IPEggsLog>"),
			'HideOnEmpty', true,
		}),
		}),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "SpawnDef",
	'TargetId', "Gujos_Daily",
	'TargetProp', "Regions",
	'EditType', "Code",
	'TargetFunc', function (self, value, default)
		return false
	end,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "SpawnDef",
	'TargetId', "Gujos_Initial",
	'TargetProp', "Regions",
	'EditType', "Code",
	'TargetFunc', function (self, value, default)
		return false
	end,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "SpawnDef",
	'TargetId', "GujoNest_NearHumans",
	'TargetProp', "Regions",
	'EditType', "Code",
	'TargetFunc', function (self, value, default)
		return false
	end,
}),
}
