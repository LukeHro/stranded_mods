UndefineClass('TamedBirdNest')
DefineClass.TamedBirdNest = {
	__parents = { "EggsBuilding", "OwnedComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "EggsBuilding",
	LockState = "hidden",
	ForwardDir = 5400,
	BuildMenuCategory = "Animals",
	display_name = T(493865934663, --[[ModItemBuildingCompositeDef TamedBirdNest display_name]] "Bird nest"),
	description = T(878935729436, --[[ModItemBuildingCompositeDef TamedBirdNest description]] "Dedicated sleeping spot for birds to sleep and lay eggs."),
	BuildMenuIcon = "Images/TamedBirdNest.png",
	BuildMenuPos = 10,
	display_name_pl = T(655722579213, --[[ModItemBuildingCompositeDef TamedBirdNest display_name_pl]] "Bird nests"),
	entity = "Gujo_Nest",
	labels = {
		"BirdSleepingSpot",
	},
	construction_cost = PlaceObj('ConstructionCost', {
		Hay = 10000,
		Sticks = 10000,
	}),
	construction_points = 5000,
	lock_block_box = box(-1500, -1500, 0, 1500, 1500, 700),
	lock_pass_box = box(-1500, -1500, 0, 1500, 1500, 700),
	ConstructIgnore = set( "Flooring" ),
	MovingAs = "entity",
	fx_actor_base_class = "AnimalBed",
	CustomMaterial = "BedSoil",
	attack_attraction = 1,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	OwnedComponent = true,
	ownership_class = "AnimalBedSmall",
	ChangeOwnerIcon = "UI/Icons/Infopanels/animal_change_owner",
	ChangeOwnerRolloverText = T(626643706445, --[[ModItemBuildingCompositeDef TamedBirdNest ChangeOwnerRolloverText]] "Decide which animal is allowed to sleep on this spot."),
	OwnerLabel = "TamedAnimals",
	Filter = function (obj)
		return table.find(obj.AnimalPerks, "EggsProducer")
	end,
}

