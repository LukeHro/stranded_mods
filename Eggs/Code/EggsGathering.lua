function OnMsg.ModsReloaded()
    PlaceObj('AnimalPerk', {
        Description = T([[Can lay eggs after upon waking up.
*Eggs can be collected from hand made nests after tamed, mature females sleep in those.]]),
        DisplayName = T("Eggs producer"),
        id = "EggsProducer",
    })
    table.insert_unique(Gujo.AnimalPerks, "EggsProducer")

    Presets.ActivitySet.Default.Work.Activities.CollectingEggs = true
    table.insert_unique(Presets.ActivityGroup.Default.Ranch.Activities, "CollectingEggs")
end

function SavegameFixups.UnlockNests()
    if UIPlayer.research_center:IsTechResearched("AnimalTaming") then
        print("fixing...", GetPresetLockStateKey("BuildingCompositeDef", "Default", "TamedBirdNest"))
        RemovePresetLockStateReason("BuildingCompositeDef", "Default", "TamedBirdNest", "hidden", false)
    end
end

function OnMsg.ResearchCompleted(tech, player)
    if tech and tech.id == "AnimalTaming" then
        RemovePresetLockStateReason("BuildingCompositeDef", "Default", "TamedBirdNest", "hidden", false, player)
    end
end

function TamedBirdNest:Reserve(bird)
    self.sleep_time = GameTime()
    return ReservedObject.Reserve(self, bird)
end

function TamedBirdNest:CancelReservation(bird)
    if (self.sleep_time ~= 0 and GameTime() - self.sleep_time > bird.SleepDurationMin) then
        self:LayEggs(bird)
    end
    self.sleep_time = 0
    return ReservedObject.CancelReservation(self, bird)
end

DefineClass.EggsBuilding = {
    __parents = { "CompositeBuilding" },
    sleep_time = 0,
    eggs = 0,
    enhanced = 0,
}

function EggsBuilding:OnObjUpdate(time)
    if not IsConstructionFinished(self) then
        return
    end
    self:CreateCollectingEggsActivity()
end

function EggsBuilding:CreateCollectingEggsActivity()
    if not IsValid(self) or IsBeingDestructed(self) then
        return
    end
    local obj = self:GetAttach("CollectingEggsActivityObject")
    if IsValid(obj) then
        return obj
    end
    obj = CreateActivityObject("CollectingEggsActivityObject", {
        player = self.player
    }, self)
    obj:SetActivityAvailable(true)
    return obj
end

function EggsBuilding:RemoveCollectingEggsActivity()
    local obj = self:GetAttach("CollectingEggsActivityObject")
    if IsValid(obj) then
        obj:delete()
    end
end

function EggsBuilding:GatherDirectOrders(unit, orders)
    if unit and self:CanWork() then
        CreateDirectOrderButtons(orders, DirectOrders.CollectingEggs, self)
    end
end

function EggsBuilding:GetAssignedSurvivor()
    local activity = self:GetAttach("CollectingEggsActivityObject")
    return activity and activity.activity_unit
end

function EggsBuilding:LayEggs(bird)
    if IsAliveAndConscious(bird) and bird.Gender == "female" and not bird:IsGrowingUp() then
        self.eggs = self.eggs + 2 + self.enhanced
        self.enhanced = self.eggs > 3 and -1 or Min(0, self.enhanced)
        ObjModified(self)
    end
end

function EggsBuilding:CollectEggs(unit, skill_level)
    local eggs = self.eggs
    self.eggs = 0
    if (self:Random(const.SkillMaxLevel, "EnhanceEgging") < skill_level) then
        self.enhanced = 1
    elseif (self:Random(const.SkillMaxLevel, "EnhanceEgging") > skill_level) then
        self.enhanced = -1
    end
    eggs = eggs * const.ResourceScale
    if eggs > 0 then
        PlaceResourcePile(unit, "Eggs", eggs)
    end
    ObjModified(self)
    return eggs
end

function EggsBuilding:CheatAddEggs()
    return self:LayEggs(self)
end

function EggsBuilding:GetIPEggsInfo()
    return T{"There are <res(eggs,amount)> here", eggs = "Eggs", amount = self.eggs * const.ResourceScale}
end

function EggsBuilding:GetIPEggsLog()
    return self.enhanced > 0 and T("Nicely set up nest, the next bird using it will be more productive!")
            or self.enhanced < 0 and T("Messed up nest, birds won't be that comfortable and will produce less eggs!")
            or T("Skilled farmers can nicely arrange the nest after collecting so birds will lay more eggs. Or otherwise they may mess it...")
end

DefineClass.CollectingEggsActivityObject = {
    __parents = {
        "ActivityObject"
    },
    activity_id = "CollectingEggs",
    retry_time = false,
}

function CollectingEggsActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

function CollectingEggsActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local building = self:GetActivityTarget()
    if not building then
        return
    end
    local dtors = unit:PushDestructor(function(self)
        self.fx_anim_target = nil
    end)
    unit.fx_anim_target = building
    local activity_time = MulDivRound(const.HourDuration, 20, efficiency)
    unit:PlayPrg(PrgAmbientLife.CollectingEggs, activity_time, building)
    building:CollectEggs(unit, skill_level)
    unit:PopAndCallDestructor(dtors)
    self:delete()
    return true
end

function CollectingEggsActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    local target = self:GetActivityTarget()
    if target.reserved_by or not target.eggs or target.eggs <= 0 then
        return
    end
    if (self.retry_time or 0) > GameTime() then
        return
    end
    local area_is_locked = not target:IsSafeForApproaching(unit)
    if area_is_locked then
        self.retry_time = GameTime() + const.HourDuration
        return
    end
    return true
end

function CollectingEggsActivityObject:Done()
end

function PrgAmbientLife.CollectingEggs(unit, target)
    unit:PushDestructor(function(unit)
        unit:PlayMomentTrackedAnim("butchering_End", 1, nil, 200, unit:GetAnimDuration("butchering_End"))
    end)
    unit:StatusUIUpdateData("activity", {
        bar1_start_time = GameTime(),
        bar1_end_time = GameTime() + unit:VisitTimeLeft()
    })
    local anim_duration = unit:GetAnimDuration("butchering_Start")
    unit:Face(target, anim_duration)
    unit:PlayMomentTrackedAnim("butchering_Start", 1, nil, 200, anim_duration)
    anim_duration = unit:GetAnimDuration("butchering_Idle")
    while unit:VisitTimeLeft() > 0 do
        unit:PlayMomentTrackedAnim("butchering_Idle", 1, nil, 200, Min(unit:VisitTimeLeft(), anim_duration))
        if unit.visit_restart then
            return
        end
    end
    unit:PopAndCallDestructor()
end

local GetAnimalBed_UnitAnimal = UnitAnimal.GetAnimalBed
function UnitAnimal:GetAnimalBed(...)
    local nest = self:GetOwnedObject("AnimalBedSmall") or self:GetOwnedObject("AnimalBedLarge")
    if nest then
        return nest
    end
    if self:IsTamed() and table.find(self.AnimalPerks, "EggsProducer") then
        for _, bed in ipairs(self.player.labels.BirdSleepingSpot) do
            if IsValid(bed) and not bed:IsVirtual() and self:CheckConnectivity(bed) and not bed.reserved_by
                    and terrain.IsPassable(bed, self.pfclass) and not self:IsTargetUnreachable(bed)
                    and IsCloser(bed, self, const.Gameplay.MaxAnimalBuildingFindDistance) then
                if not bed:GetOwner() or bed:GetOwner() == self then
                    nest = bed
                end
            end
        end
    end
    return nest or GetAnimalBed_UnitAnimal(self, ...)
end

function OnEggBoilingCompleted(recipe, device, unit, res, amount, res_info)
    device.selected_recipe = false
    device:AddRes(res, 2 * const.ResourceScale, res_info)
    device:UpdateResPolicies()
    local policy = device.res_policies and device.res_policies[res]
    local supply_req = policy and policy.supply_req
    if supply_req and supply_req:CanAssignUnit() then
        unit.boiled_eggs = true
        unit:EnqueueActivityInFront("Eating", {supply_req, 2 * const.ResourceScale})
    end
end

function GetFreeBoilingDevice(unit)
    return MapFindNearest(unit, unit, 10 * guim, "ProductionDeviceComponent", nil, nil, function(obj)
        local interfaces = obj.interfaces or {}
        return obj.working and (not obj.assigned_unit or obj.assigned_unit == unit)
                and (not obj:IsSingleWorkAreaObject() or unit.command_center == obj.command_center)
                and (table.find(interfaces, "PotInterface") or table.find(interfaces, "CauldronInterface"))
    end)
end

function ProductionDeviceComponent:SetBoilingEggsRecipe()
    self.selected_recipe = {amount = 1, handle = -1, id = "BoiledEggs", production_type = "make_once"}
    local interface = table.find(self.interfaces, "CauldronInterface") or table.find(self.interfaces, "PotInterface")
    self.selected_interface = DeviceInterfaces[self.interfaces[interface]]
    return CreateActivityObject("ProductionActivityObject", {
        player = self.player,
        building = self,
        recipe_id = "BoiledEggs",
        recipe_handle = -1,
        recipe_def = Recipes["BoiledEggs"],
        production_count = 1,
        activity_id = "BoilingEggs",
    }, self)
end

local TryReserveAndApproach_Unit = Unit.TryReserveAndApproach
function Unit:TryReserveAndApproach(...)
    if self.eating_resource == "Eggs" and not self.boiled_eggs and self.res_amounts.Eggs and self.res_amounts.Eggs >= 2000 then
        local boiling_device = GetFreeBoilingDevice(self)
        if boiling_device then
            local activity_obj = boiling_device:SetBoilingEggsRecipe()
            local skill_level, efficiency =  self:GetSkillAndEfficiency("BoilingEggs")
            self:EnqueueActivity("BoilingEggs", activity_obj, skill_level, efficiency, true)
            self:TryInterruptActivityCommand()
        end
    end
    local chair = TryReserveAndApproach_Unit(self, ...)
    if self.eating_resource == "Eggs" and not self.boiled_eggs then
        self:AddHealthCondition("Ate_raw_eggs", "ate raw eggs")
    end
    self.boiled_eggs = nil
    return chair
end

local IsResAccepted_UnitEnergy = UnitEnergy.IsResAccepted
function UnitEnergy:IsResAccepted(res, ...)
    local result = IsResAccepted_UnitEnergy(self, res, ...)
    if not self:IsCommandThread() then
        return result
    end
    if result and (res == "Eggs") then
        return GetFreeBoilingDevice(self)
    end
    return result
end

local Cauldron_full_anim = PrgAmbientLife["Cauldroninterface"]
PrgAmbientLife["Cauldroninterface"] = function(unit, bld, ...)

    if unit:VisitTimeLeft() > const.HourDuration / 2 then
        return Cauldron_full_anim(unit, bld, ...)
    end

    local pot, spatula

    unit:PushDestructor(function(unit)
        if IsValid(pot) then
            DoneObject(pot)
        end
        if IsValid(pot) then
            pot:Detach()
        end
        if IsValid(spatula) then
            spatula:Detach()
        end
        if IsValid(spatula) then
            DoneObject(spatula)
        end
    end)

    local unit_orig_scale = unit:GetScale()
    unit:SetScale(100, unit:GetAnimDuration("standing_Cook_Cauldron_Start"))
    unit:PushDestructor(function(unit)
        DoneObject(spatula)
        unit:SetScale(unit_orig_scale, unit:GetAnimDuration("standing_Cook_Cauldron_End"))
        unit:PlayMomentTrackedAnim("standing_Cook_Cauldron_End", 1, nil, 200, nil, "hit", function()
            if IsValid(pot) then
                unit:Attach(pot, unit:GetRandomSpot("Cauldron"))
            end
        end)
    end)

    unit:PlayMomentTrackedAnim("standing_Cook_Cauldron_Start", 1, nil, 200, nil, "hit", function()
        pot = PlaceObject("Cauldron", nil, const.cofComponentAttach)
        NetTempObject(pot)
        bld:Attach(pot, bld:GetRandomSpot("Cauldron"))
    end)
    spatula = PlaceObject("CoockingSpatula", nil, const.cofComponentAttach)
    NetTempObject(spatula)
    unit:Attach(spatula, unit:GetRandomSpot("Spatula"))
    local end_offset = unit:GetAnimDuration("standing_Cook_Cauldron_End")
    while unit:VisitTimeLeft() - end_offset > 0 do
        local remaining = unit:VisitTimeLeft() - end_offset
        local duration = Min(remaining, 1 * unit:GetAnimDuration("standing_Cook_Cauldron_MediumSkill"))
        unit:PlayMomentTrackedAnim("standing_Cook_Cauldron_MediumSkill", 1, nil, 200, duration)
        if unit.visit_restart then unit:PopAndCallDestructor() return end
    end
    unit:PopAndCallDestructor()
    unit:PopAndCallDestructor()
end

local GetConsumeDuration_UnitEnergy = UnitEnergy.GetConsumeDuration
function UnitEnergy:GetConsumeDuration(manipulation, resource)
    local duration = GetConsumeDuration_UnitEnergy(self, manipulation, resource)
    if resource == "Eggs" then
        return DivRound(duration, 2)
    end
    return duration
end

local Pot_full_anim = PrgAmbientLife["Potinterface"]
PrgAmbientLife["Potinterface"] = function(unit, bld, ...)

    if unit:VisitTimeLeft() > const.HourDuration / 2 then
        return Pot_full_anim(unit, bld, ...)
    end

    local pot, spatula

    unit:PushDestructor(function(unit)
        if IsValid(pot) then
            DoneObject(pot)
        end
        if IsValid(pot) then
            pot:Detach()
        end
        if IsValid(spatula) then
            spatula:Detach()
        end
        if IsValid(spatula) then
            DoneObject(spatula)
        end
    end)

    local unit_orig_scale = unit:GetScale()
    unit:SetScale(100, unit:GetAnimDuration("standing_Cook_Pot_Start"))
    unit:PushDestructor(function(unit)
        DoneObject(spatula)
        unit:SetScale(unit_orig_scale, unit:GetAnimDuration("standing_Cook_Pot_End"))
        unit:PlayMomentTrackedAnim("standing_Cook_Pot_End", 1, nil, 200, nil, "hit", function()
            if IsValid(pot) then
                unit:Attach(pot, unit:GetRandomSpot("Pot"))
            end
        end)
    end)

    unit:PlayMomentTrackedAnim("standing_Cook_Pot_Start", 1, nil, 200, nil, "hit", function()
        pot = PlaceObject("IntDecor_TinPot_01", nil, const.cofComponentAttach)
        NetTempObject(pot)
        bld:Attach(pot, bld:GetRandomSpot("Pot"))
    end)
    spatula = PlaceObject("CoockingSpatula", nil, const.cofComponentAttach)
    NetTempObject(spatula)
    unit:Attach(spatula, unit:GetRandomSpot("Spatula"))
    local end_offset = unit:GetAnimDuration("standing_Cook_Pot_End")
    while unit:VisitTimeLeft() - end_offset > 0 do
        local remaining = unit:VisitTimeLeft() - end_offset
        local duration = Min(remaining, 1 * unit:GetAnimDuration("standing_Cook_Pot_MediumSkill"))
        unit:PlayMomentTrackedAnim("standing_Cook_Pot_MediumSkill", 1, nil, 200, duration)
        if unit.visit_restart then unit:PopAndCallDestructor() return end
    end
    unit:PopAndCallDestructor()
    unit:PopAndCallDestructor()
end

local UpdateSkillInclinations_UnitSkill = UnitSkill.UpdateSkillInclinations
function UnitSkill:UpdateSkillInclinations(...)
    if self.activity_forbidden_reasons then
        self.activity_forbidden_reasons["CollectingEggs"] = self.activity_forbidden_reasons["Ranching"]
        self.activity_forbidden_reasons["BoilingEggs"] = self.activity_forbidden_reasons["Cooking"]
    end
    return UpdateSkillInclinations_UnitSkill(self, ...)
end
UnitSkill.SetActivityAllowedReason = UnitSkill.UpdateSkillInclinations

function SavegameFixups.FixHopeEggsGathering()
    if Hope then
        Hope:UpdateSkillInclinations()
    end
end