return {
PlaceObj('ModItemActionFXParticles', {
	Action = "working",
	Actor = "Boiler",
	Attach = true,
	DetailLevel = 100,
	EndRules = {
		PlaceObj('ActionFXEndRule', {
			'EndAction', "working",
			'EndMoment', "end",
		}),
	},
	Moment = "start",
	Offset = point(100, 0, 0),
	Particles = "Fire_Stove",
	behaviors = {
		PlaceObj('ActionFXBehavior', nil),
	},
	group = "FXParticles Constructable Devices",
	handle = "2W6CzYE5",
	id = "2W6CzYE5",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(261440985010, --[[ModItemActivityType WashPond ActivityStatusText |gender-variants]] "Washing"),
	ActivityText = T(963912986348, --[[ModItemActivityType WashPond ActivityText |gender-variants]] "Wash"),
	Cmd = "CmdWashAtPond",
	DefaultPriority = 2,
	Description = T(821643401952, --[[ModItemActivityType WashPond Description]] "Increases hygiene."),
	DestlockImportance = 1,
	DisplayName = T(100440282473, --[[ModItemActivityType WashPond DisplayName]] "Wash"),
	FindTarget = function (self, unit, range, prev_target)
		if unit.wash_at_pond and not unit:HasUnitTag("Android") and unit.Hygiene <= unit.DirtyThreshold then
			local pos = FindNearestWater(unit:GetPos())
			return unit:CanReach(pos) and IsCloser2D(unit:GetPos(), pos, range) and pos
		end
	end,
	GetDisplayedStatusText = function (self, unit, short)
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PriorityDisplayName = T(205251677385, --[[ModItemActivityType WashPond PriorityDisplayName]] "Washing"),
	QueueStatusText = T(433851444407, --[[ModItemActivityType WashPond QueueStatusText |gender-variants]] "Wash"),
	SortKey = 1460,
	StatusIcon = "UI/Hud/st_wash.dds",
	id = "WashPond",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(942876460833, --[[ModItemActivityType WashHandPump ActivityStatusText |gender-variants]] "Washing"),
	ActivityText = T(859859166014, --[[ModItemActivityType WashHandPump ActivityText |gender-variants]] "Wash"),
	DefaultPriority = 2,
	Description = T(632097342405, --[[ModItemActivityType WashHandPump Description]] "Increases hygiene."),
	DestlockImportance = 1,
	DisplayName = T(660310704641, --[[ModItemActivityType WashHandPump DisplayName]] "Wash"),
	GetDisplayedStatusText = function (self, unit, short)
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PickClosest = true,
	PriorityDisplayName = T(738264744887, --[[ModItemActivityType WashHandPump PriorityDisplayName]] "Washing"),
	QueueStatusText = T(666140272252, --[[ModItemActivityType WashHandPump QueueStatusText |gender-variants]] "Wash"),
	SortKey = 1460,
	StatusIcon = "UI/Hud/st_wash.dds",
	id = "WashHandPump",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(100172728865, --[[ModItemActivityType Bath ActivityStatusText |gender-variants]] "Bathing"),
	ActivityText = T(671644412951, --[[ModItemActivityType Bath ActivityText |gender-variants]] "Bath"),
	DefaultPriority = 1,
	Description = T(464334187619, --[[ModItemActivityType Bath Description]] "Increases hygiene."),
	DestlockImportance = 1,
	DisplayName = T(703928406367, --[[ModItemActivityType Bath DisplayName]] "Bath"),
	GetDisplayedStatusText = function (self, unit, short)
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PriorityDisplayName = T(673421423243, --[[ModItemActivityType Bath PriorityDisplayName]] "Bathing"),
	QueueStatusText = T(640677560824, --[[ModItemActivityType Bath QueueStatusText |gender-variants]] "Bath"),
	SortKey = 1460,
	StatusIcon = "UI/Hud/st_wash.dds",
	id = "Bath",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(239587213965, --[[ModItemActivityType Shower ActivityStatusText |gender-variants]] "Shower"),
	ActivityText = T(430937532276, --[[ModItemActivityType Shower ActivityText |gender-variants]] "Shower"),
	DefaultPriority = 1,
	Description = T(412035931692, --[[ModItemActivityType Shower Description]] "Increases hygiene."),
	DestlockImportance = 1,
	DisplayName = T(167468101282, --[[ModItemActivityType Shower DisplayName]] "Shower"),
	GetDisplayedStatusText = function (self, unit, short)
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PickClosest = true,
	PriorityDisplayName = T(441525480719, --[[ModItemActivityType Shower PriorityDisplayName]] "Showering"),
	QueueStatusText = T(468965998846, --[[ModItemActivityType Shower QueueStatusText |gender-variants]] "Shower"),
	SortKey = 1460,
	StatusIcon = "UI/Hud/st_wash.dds",
	id = "Shower",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(298100677656, --[[ModItemActivityType WashSink ActivityStatusText |gender-variants]] "Washing hands"),
	ActivityText = T(896607490383, --[[ModItemActivityType WashSink ActivityText |gender-variants]] "Wash hands"),
	AllowAutoRun = false,
	DefaultPriority = 2,
	Description = T(830007214339, --[[ModItemActivityType WashSink Description]] "Increases hygiene."),
	DestlockImportance = 1,
	DisplayName = T(552453243840, --[[ModItemActivityType WashSink DisplayName]] "Wash hands"),
	FindTarget = function (self, unit, range, prev_target)
		return unit.command_center:GetActivityObject(self.id, unit, range, prev_target)
	end,
	GetDisplayedStatusText = function (self, unit, short)
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PickClosest = true,
	PriorityDisplayName = T(193294471435, --[[ModItemActivityType WashSink PriorityDisplayName]] "Washing hands"),
	QueueStatusText = T(283218819454, --[[ModItemActivityType WashSink QueueStatusText |gender-variants]] "Wash hands"),
	SortKey = 1460,
	StatusIcon = "UI/Hud/st_wash.dds",
	id = "WashSink",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Water",
	BuildMenuIcon = "Mod/LH_Hygiene/UI/Icons/Build Menu/metal_tub.dds",
	BuildMenuPos = 120,
	ChangeOwnerIcon = "Mod/LH_Hygiene/UI/Icons/Infopanels/metal_tub_change_owner.dds",
	EntityHeight = 667,
	Health = 160000,
	HeatSourceComponent = true,
	LockState = "hidden",
	ManageActiveState = true,
	MaxHealth = 160000,
	OwnedComponent = true,
	PowerTemperature = 30000,
	RadiationRange = 1000,
	RadiationTemperature = 30000,
	RelaxationDeviceComponent = true,
	RoomPlacement = "indoors",
	SkirtSize = 10,
	attack_attraction = 5,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 30000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {}),
	description = T(547324341449, --[[ModItemBuildingCompositeDef MetalTub description]] "Take a relaxing bath. Warm water heated by a Boiler must be available."),
	display_name = T(236376959891, --[[ModItemBuildingCompositeDef MetalTub display_name]] "Bath tub"),
	display_name_pl = T(870006916204, --[[ModItemBuildingCompositeDef MetalTub display_name_pl]] "Bath tubs"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "MetalTub",
	group = "Water",
	id = "MetalTub",
	lock_block_box = box(-300, -300, 0, 1500, 300, 700),
	object_class = "MetalTubBuilding",
	orient_to_terrain = true,
	ownership_class = "MetalTub",
	update_interval = 5000,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Water",
	BuildMenuIcon = "UI/Icons/Build Menu/shower.dds",
	BuildMenuPos = 125,
	CanPlaceInShelter = false,
	ChangeOwnerIcon = "Mod/LH_Hygiene/UI/Icons/Infopanels/shower_change_owner.dds",
	EntityHeight = 1115,
	Filter = function (obj)
		return not obj:HasUnitTag("Android")
	end,
	Health = 60000,
	HeatSourceComponent = true,
	LockState = "hidden",
	MaxHealth = 60000,
	OwnedComponent = true,
	PowerTemperature = 30000,
	RadiationRange = 500,
	RadiationTemperature = 30000,
	RoomPlacement = "indoors",
	SkirtSize = 228,
	attached_to_wall = true,
	attack_attraction = 5,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 30000,
	}),
	construction_points = 40000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
		PlaceObj('ConstructionCheckRoomPlacement', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	description = T(819309511444, --[[ModItemBuildingCompositeDef Shower description]] "Take a relaxing shower. Warm water heated by a Boiler must be available."),
	display_name = T(286258901247, --[[ModItemBuildingCompositeDef Shower display_name]] "Shower"),
	display_name_pl = T(137700435569, --[[ModItemBuildingCompositeDef Shower display_name_pl]] "Showers"),
	display_name_short = T(884749797461, --[[ModItemBuildingCompositeDef Shower display_name_short]] "Shower"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Shower",
	group = "Water",
	id = "Shower",
	lock_block_box = box(0, 0, 350, 900, 300, 2100),
	object_class = "ShowerBuilding",
	ownership_class = "ShowerBuilding",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Water",
	BuildMenuIcon = "Mod/LH_Hygiene/UI/Icons/Build Menu/fire_boiler.dds",
	BuildMenuPos = 130,
	CustomMaterial = "Metal-Construction",
	EntityHeight = 1372,
	HeatSourceComponent = true,
	LightDirection = point(4096, 0, 0),
	LightFov = 10800,
	LightRadius = 2000,
	LightingComponent = true,
	LockState = "hidden",
	PowerOutput = 2000,
	PowerTemperature = 90000,
	RadiationRange = 4000,
	RadiationTemperature = 10000,
	ResConsumerDeviceComponent = true,
	RoomPlacement = "indoors",
	SkirtSize = 166,
	attack_attraction = 5,
	can_turn_off = true,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
		Sticks = 5000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 20000,
	}),
	description = T(175596877835, --[[ModItemBuildingCompositeDef Boiler description]] "Warm and helps distributing water inside buildings. Can service a single bathing facility at one time."),
	display_name = T(690620621214, --[[ModItemBuildingCompositeDef Boiler display_name]] "Boiler"),
	display_name_pl = T(646576748253, --[[ModItemBuildingCompositeDef Boiler display_name_pl]] "Boilers"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Interior_Stove_01",
	fx_actor_base_class = "Stove",
	group = "Water",
	id = "Boiler",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 600, 300, 1400),
	object_class = "BoilerBuilding",
	res_consumed = "Sticks",
	res_operate_time = 1920000,
	res_start_fueled = true,
	res_stored = 5000,
	show_name_in_direct_order = false,
	turn_off_description = T(604775777853, --[[ModItemBuildingCompositeDef Boiler turn_off_description]] "Task the survivors to extinguish the fire, saving the remaining fuel."),
	turn_off_icon = "UI/Icons/Infopanels/common_extinguish",
	turn_off_text = T(197332281803, --[[ModItemBuildingCompositeDef Boiler turn_off_text |gender-variants]] "Extinguish"),
	turn_on_description = T(725442781996, --[[ModItemBuildingCompositeDef Boiler turn_on_description]] "Task the survivors to reignite the fire."),
	turn_on_icon = "UI/Icons/Infopanels/common_ignite",
	turn_on_text = T(980165870127, --[[ModItemBuildingCompositeDef Boiler turn_on_text |gender-variants]] "Start fire"),
	turning_off_text = T(979831999438, --[[ModItemBuildingCompositeDef Boiler turning_off_text]] "Waiting to be extinguished"),
	turning_on_text = T(485606910343, --[[ModItemBuildingCompositeDef Boiler turning_on_text]] "Waiting to be ignited"),
	unload_anim_hands = "standing_DropDown_Hands",
	unload_anim_wood = "standing_PutIn_Wood",
	update_interval = 1000,
	working_during_construction = true,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Water",
	BuildMenuIcon = "Mod/LH_Hygiene/UI/Icons/Build Menu/electric_boiler.dds",
	BuildMenuPos = 135,
	CanPlaceInShelter = false,
	EntityHeight = 1115,
	Health = 60000,
	IsPowerConsumer = true,
	LockState = "hidden",
	MaxHealth = 60000,
	PowerComponent = true,
	PowerConsumption = 20000,
	RoomPlacement = "indoors",
	SkirtSize = 228,
	attached_to_wall = true,
	attack_attraction = 5,
	can_change_ownership = false,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
		ScrapElectronics = 2000,
	}),
	construction_points = 40000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
		PlaceObj('ConstructionCheckRoomPlacement', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		Metal = 5000,
		ScrapElectronics = 1000,
	}),
	description = T(749326591829, --[[ModItemBuildingCompositeDef ElectricBoiler description]] "Warm and helps distributing water inside buildings. Can service all the bathing facilities inside a house."),
	display_name = T(106177514328, --[[ModItemBuildingCompositeDef ElectricBoiler display_name]] "Electric boiler"),
	display_name_pl = T(264483901273, --[[ModItemBuildingCompositeDef ElectricBoiler display_name_pl]] "Electric boilers"),
	display_name_short = T(275926848858, --[[ModItemBuildingCompositeDef ElectricBoiler display_name_short]] "Electric boiler"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Boiler",
	group = "Water",
	id = "ElectricBoiler",
	lock_block_box = box(0, -300, 0, 600, 300, 1750),
	object_class = "ElectricBoilerBuilding",
}),
PlaceObj('ModItemCode', {
	'name', "BoilerBuilding",
	'CodeFileName', "Code/BoilerBuilding.lua",
}),
PlaceObj('ModItemCode', {
	'name', "HandPumpBuilding",
	'CodeFileName', "Code/HandPumpBuilding.lua",
}),
PlaceObj('ModItemCode', {
	'name', "HygieneScript",
	'CodeFileName', "Code/HygieneScript.lua",
}),
PlaceObj('ModItemCode', {
	'name', "MetalTubBuilding",
	'CodeFileName', "Code/MetalTubBuilding.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ShowerBuilding",
	'CodeFileName', "Code/ShowerBuilding.lua",
}),
PlaceObj('ModItemCode', {
	'name', "SinkBuilding",
	'CodeFileName', "Code/SinkBuilding.lua",
}),
PlaceObj('ModItemCode', {
	'name', "UnitHygiene",
	'CodeFileName', "Code/UnitHygiene.lua",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, from_transfer, ...)
		        local activity_id = self.ActivityId
		        local activity = ActivityTypes[activity_id]
		        local skill_level, efficiency = unit:GetSkillAndEfficiency(activity)
		        local activity_target = target:CreateWashHandPumpActivityObject()
		        unit:EnqueueAndTryExecuteActivity(activity_id, activity_target, skill_level, efficiency, clear_queue)
	end,
	ActivityId = "WashHandPump",
	EnableCanceling = false,
	GetAssignedUnit = function (self, target, param)
		        return target:GetAssignedSurvivor()
	end,
	GetButtonText = function (self, unit, target, param, status_text)
		        return GetTByGender(self.Text, unit)
	end,
	GetErrorStatus = function (self, unit, target, param)
		if unit.Hygiene > unit.CleanThreshold then
			return T("Clean enough")
		end
		local survivor = target:GetAssignedSurvivor()
		if IsValid(survivor) then
			return T{"used by <unit_name>", unit_name = survivor:GetDisplayNameNoStatus()}
		end
		        return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	RequireUnit = true,
	id = "WashHandPump",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, from_transfer, ...)
		local activity_id = self.ActivityId
		local activity = ActivityTypes[activity_id]
		local skill_level, efficiency = unit:GetSkillAndEfficiency(activity)
		local activity_target = target:CreateWashSinkActivityObject()
		unit:EnqueueAndTryExecuteActivity(activity_id, activity_target, skill_level, efficiency, clear_queue)
	end,
	ActivityId = "WashSink",
	EnableCanceling = false,
	GetAssignedUnit = function (self, target, param)
		return target:GetAssignedSurvivor()
	end,
	GetButtonText = function (self, unit, target, param, status_text)
		return GetTByGender(self.Text, unit)
	end,
	GetErrorStatus = function (self, unit, target, param)
		if unit.Hygiene > unit.CleanThreshold then
			return T("Clean enough")
		end
		local survivor = target:GetAssignedSurvivor()
		if IsValid(survivor) then
			return T{"used by <unit_name>", unit_name = survivor:GetDisplayNameNoStatus()}
		end
		return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	RequireUnit = true,
	id = "WashSink",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, from_transfer, ...)
		local activity_id = self.ActivityId
		local activity = ActivityTypes[activity_id]
		local skill_level, efficiency = unit:GetSkillAndEfficiency(activity)
		local activity_target = target:CreateIndoorHygieneActivityObject()
		unit:EnqueueAndTryExecuteActivity(activity_id, activity_target, skill_level, efficiency, clear_queue)
	end,
	ActivityId = "Bath",
	EnableCanceling = false,
	GetAssignedUnit = function (self, target, param)
		return target:GetAssignedSurvivor()
	end,
	GetButtonText = function (self, unit, target, param, status_text)
		return GetTByGender(self.Text, unit)
	end,
	GetErrorStatus = function (self, unit, target, param)
		if not GetConnectedBoiler(target) then
			return T("No working boiler")
		end
		if unit.Hygiene > unit.CleanThreshold then
			return T("Clean enough")
		end
		local survivor = target:GetAssignedSurvivor()
		if IsValid(survivor) then
			return T{"used by <unit_name>", unit_name = survivor:GetDisplayNameNoStatus()}
		end
		return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	RequireUnit = true,
	id = "Bath",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, from_transfer, ...)
		        local activity_id = self.ActivityId
		        local activity = ActivityTypes[activity_id]
		        local skill_level, efficiency = unit:GetSkillAndEfficiency(activity)
		        local activity_target = target:CreateIndoorHygieneActivityObject()
		        unit:EnqueueAndTryExecuteActivity(activity_id, activity_target, skill_level, efficiency, clear_queue)
	end,
	ActivityId = "Shower",
	EnableCanceling = false,
	GetAssignedUnit = function (self, target, param)
		        return target:GetAssignedSurvivor()
	end,
	GetButtonText = function (self, unit, target, param, status_text)
		        return GetTByGender(self.Text, unit)
	end,
	GetErrorStatus = function (self, unit, target, param)
		if not GetConnectedBoiler(target) then
			return T("No working boiler")
		end
		if unit.Hygiene > unit.CleanThreshold then
			return T("Clean enough")
		end
		local survivor = target:GetAssignedSurvivor()
		if IsValid(survivor) then
			return T{"used by <unit_name>", unit_name = survivor:GetDisplayNameNoStatus()}
		end
		return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	RequireUnit = true,
	id = "Shower",
}),
PlaceObj('ModItemEntity', {
	'name', "Boiler",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "Boiler",
	'material_type', "Metal",
}),
PlaceObj('ModItemEntity', {
	'name', "Shower",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "Shower",
	'material_type', "Metal",
}),
PlaceObj('ModItemEntity', {
	'name', "ShowerWater",
	'class_parent', "AutoAttachObject",
	'entity_name', "ShowerWater",
}),
PlaceObj('ModItemHappinessFactor', {
	Description = T(175305709763, --[[ModItemHappinessFactor CleanHygiene Description]] '"The human heart and brain long for beauty, after all."'),
	DisplayName = T(526297699945, --[[ModItemHappinessFactor CleanHygiene DisplayName]] "Clean"),
	DisplayNameShort = T(262749797103, --[[ModItemHappinessFactor CleanHygiene DisplayNameShort]] "Hygiene"),
	Happiness = 20,
	RemoveStatusEffects = {
		"DirtyHygiene",
		"SmellyHygiene",
	},
	StackLimit = 1,
	id = "CleanHygiene",
}),
PlaceObj('ModItemHappinessFactor', {
	Description = T(991164060848, --[[ModItemHappinessFactor SmellyHygiene Description]] "Hygiene starts to be a problem here."),
	DisplayName = T(427024351486, --[[ModItemHappinessFactor SmellyHygiene DisplayName]] "Smelly"),
	DisplayNameShort = T(741381606051, --[[ModItemHappinessFactor SmellyHygiene DisplayNameShort]] "Hygiene"),
	Happiness = -10,
	RemoveStatusEffects = {
		"CleanHygiene",
		"DirtyHygiene",
	},
	StackLimit = 1,
	id = "SmellyHygiene",
}),
PlaceObj('ModItemHappinessFactor', {
	Description = T(881554234516, --[[ModItemHappinessFactor DirtyHygiene Description]] "Need to do something about the hygiene immediately."),
	DisplayName = T(284094047564, --[[ModItemHappinessFactor DirtyHygiene DisplayName]] "Dirty"),
	DisplayNameShort = T(777155478862, --[[ModItemHappinessFactor DirtyHygiene DisplayNameShort]] "Hygiene"),
	Happiness = -20,
	RemoveStatusEffects = {
		"CleanHygiene",
		"SmellyHygiene",
	},
	StackLimit = 1,
	id = "DirtyHygiene",
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Animals",
	},
	Description = T(378685733111, --[[ModItemTech HotWater Description]] "Use warmed water so the people can enjoy a bath inside the house.\n\n<style TechSubtitleBlue>Unlocks</style>\n   <color TextEmphasis>Metal tub</color>: 30<image 'UI/Icons/Resources/res_metal_ingot' 1100>\n   <color TextEmphasis>Boiler</color>: 5<image 'UI/Icons/Resources/res_sticks' 1100> 20<image 'UI/Icons/Resources/res_metal_ingot' 1100>"),
	DisplayName = T(295672158924, --[[ModItemTech HotWater DisplayName]] "Hot water"),
	Icon = "Mod/LH_Hygiene/UI/Icons/Research/sanitation.dds",
	LockPrerequisites = {
		PlaceObj('CheckTech', {
			Tech = "BasicSanitation",
		}),
		PlaceObj('CheckTech', {
			Tech = "BasicArchitecture",
		}),
	},
	ResearchPoints = 48000,
	SortKey = 37,
	group = "Construction",
	id = "HotWater",
	money_value = 30000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Water",
		LockState = "hidden",
		PresetId = "Boiler",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Water",
		LockState = "hidden",
		PresetId = "MetalTub",
	}),
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Animals",
	},
	Description = T(570970163551, --[[ModItemTech AdvancedSanitation Description]] "Warm water using electric boilers so the people can enjoy a proper shower.\n\n<style TechSubtitleBlue>Unlocks</style>\n   <color TextEmphasis>Shower</color>: 30<image 'UI/Icons/Resources/res_metal_ingot' 1100>\n   <color TextEmphasis>Electric Boiler</color>: 2<image 'UI/Icons/Resources/res_electronic_components' 1100> 20<image 'UI/Icons/Resources/res_metal_ingot' 1100>"),
	DisplayName = T(912088649360, --[[ModItemTech AdvancedSanitation DisplayName]] "Advanced sanitation"),
	Icon = "Mod/LH_Hygiene/UI/Icons/Research/advanced_sanitation.dds",
	LockPrerequisites = {
		PlaceObj('CheckTech', {
			Tech = "BasicSanitation",
		}),
		PlaceObj('CheckTech', {
			Tech = "BasicElectricity",
		}),
	},
	ResearchPoints = 96000,
	SortKey = 38,
	group = "Construction",
	id = "AdvancedSanitation",
	money_value = 30000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Water",
		LockState = "hidden",
		PresetId = "ElectricBoiler",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Water",
		LockState = "hidden",
		PresetId = "Shower",
	}),
}),
}
