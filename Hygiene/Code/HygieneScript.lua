local function ShowError()
    if not table.find(ModsLoaded, "id", "LH_Water") then
        CreateRealTimeThread(CreateMessageBox, nil, T("Missing dependency!"),
                T("Hygiene mod now requires Water mod.\nPlease subscribe to that it before continuing playing!"), T("OK"))
    end
end

OnMsg.GameTimeStart = ShowError
OnMsg.PostLoadGame = ShowError

function OnMsg:ModsReloaded()
    Presets.ActivitySet.Default.Relax.Activities.Bath = true
    Presets.ActivitySet.Default.Relax.Activities.Shower = true
    Presets.ActivitySet.Default.Relax.Activities.WashPond = true
    Presets.ActivitySet.Default.Relax.Activities.WashHandPump = true
end

PlaceObj('InfopanelStatus', {
    SortKey = 600,
    Text = T("Not in a fully built room."),
    group = "Default",
    id = "ShowerNotInRoom",
})

PlaceObj('InfopanelStatus', {
    SortKey = 600,
    Text = T("No working boiler in house."),
    group = "Default",
    id = "ShowerNoBoiler",
})

table.insert(Human.UnitRestrictions, {
    id = "wash_at_pond",
    display_name = T("Wash at water ponds"),
    rollover_title = T("Wash at water ponds"),
    rollover_text = T("Allow survivors to wash themselves at the nearest natural body of water."),
    default = false,
})

PlaceObj('RoomRole', {
    Prerequisites = {
        PlaceObj('CheckFurniture', {Furniture = {"Shower",},}),
        PlaceObj('CheckFurniture', {Furniture = {"Table",},Negate = true,}),
        PlaceObj('CheckFurniture', {Furniture = {"Bed",},Negate = true,}),
    },
    SortKey = 3100,
    display_name = T("Bathroom"),
    group = "Default",
    id = "Bathroom",
})

function SavegameFixups.IndoorHygieneBuildingUpdateWorking()
    MapForEach(true, "IndoorHygieneBuilding", function(building)
        building:UpdateWorking(true)
    end)
end

DefineClass.IndoorHygieneBuilding = {
    __parents = { "CompositeBuilding" },
}

function IndoorHygieneBuilding:CanActivate()
    return GetConnectedBoiler(self)
end

function IndoorHygieneBuilding:OnObjUpdate(...)
    if not IsConstructionFinished(self) then
        return
    end
    self:CreateIndoorHygieneActivityObject()
end

function IndoorHygieneBuilding:CreateIndoorHygieneActivityObject()
    if not self.hygiene_activity_object_class or not IsValid(self) or IsBeingDestructed(self) then
        return
    end

    local obj = self:GetAttach(self.hygiene_activity_object_class)
    if IsValid(obj) then
        return obj
    end
    obj = CreateActivityObject(self.hygiene_activity_object_class, { player = self.player }, self)
    obj:SetActivityAvailable(true)
    return obj
end

function IndoorHygieneBuilding:RemoveIndoorHygieneActivityObject()
    local obj = self.hygiene_activity_object_class and self:GetAttach(self.hygiene_activity_object_class)
    if IsValid(obj) then
        obj:delete()
    end
end

function IndoorHygieneBuilding:GetAssignedSurvivor()
    local obj = self.hygiene_activity_object_class and self:GetAttach(self.hygiene_activity_object_class)
    return obj and obj.activity_unit
end