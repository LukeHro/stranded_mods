function HandPumpBuilding:OnObjUpdate(...)
    if not IsConstructionFinished(self) then
        return
    end
    self:CreateWashHandPumpActivityObject()
    self:CreateWashSinkActivityObject()
end

function HandPumpBuilding:CreateWashHandPumpActivityObject()
    if not IsValid(self) or IsBeingDestructed(self) then
        return
    end
    local obj = self:GetAttach("WashHandPumpActivityObject")
    if IsValid(obj) then
        return obj
    end
    obj = CreateActivityObject("WashHandPumpActivityObject", { player = self.player }, self)
    obj:SetActivityAvailable(true)
    return obj
end

function HandPumpBuilding:RemoveWashHandPumpActivityObject()
    local obj = self:GetAttach("WashHandPumpActivityObject")
    if IsValid(obj) then
        obj:delete()
    end
end

function HandPumpBuilding:CreateWashSinkActivityObject()
    if not IsValid(self) or IsBeingDestructed(self) then
        return
    end
    local obj = self:GetAttach("WashSinkActivityObject")
    if IsValid(obj) then
        return obj
    end
    obj = CreateActivityObject("WashSinkActivityObject", { player = self.player }, self)
    obj:SetActivityAvailable(true)
    return obj
end

function HandPumpBuilding:RemoveWashSinkActivityObject()
    local obj = self:GetAttach("WashSinkActivityObject")
    if IsValid(obj) then
        obj:delete()
    end
end

function HandPumpBuilding:GatherDirectOrders(unit, orders)
    if unit and not unit:HasUnitTag("Android") and self:CanWork() then
        CreateDirectOrderButtons(orders, DirectOrders.WashHandPump, self)
        CreateDirectOrderButtons(orders, DirectOrders.WashSink, self)
    end
end

function HandPumpBuilding:GetAssignedSurvivor()
    local activity = self:GetAttach("WashHandPumpActivityObject")
    local activityHands = self:GetAttach("WashSinkActivityObject")
    return activity and activity.activity_unit or activityHands and activityHands.activity_unit
end

DefineClass.WashHandPumpActivityObject = {
    __parents = { "ActivityObject" },
    activity_id = "WashHandPump",
    retry_time = false,
}

function WashHandPumpActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

function WashHandPumpActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local pump = self:GetActivityTarget()
    if not pump then
        return
    end
    unit:PushDestructor(function()
        pump:RemoveWater()
        pump:CreateWashHandPumpActivityObject()
    end)
    unit:Face(pump:GetSpotPos(pump:GetSpotBeginIndex("Pump")))
    pump:SetCommand("CmdPumpWater", 3)
    unit:PlayMomentTrackedAnim("bend_Cut_Tool", 1, nil, 200, const.HourDuration / 10)
    unit:Face(pump:GetSpotPos(pump:GetSpotBeginIndex("Watersurface")))
    unit:WashSelf(unit:CalcWashingDuration(), pump)
    unit:PopAndCallDestructor()
    self:delete()
    return true
end

function WashHandPumpActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    if unit:HasUnitTag("Android") then
        return false
    end
    if direct_order then
        return true
    end
    if (self.retry_time or 0) > GameTime() then
        return
    end
    local target = self:GetActivityTarget()
    if not CheckHygieneBuildingAccess(unit, target) then
        return
    end
    local area_is_locked = not target:IsSafeForApproaching(unit)
    if area_is_locked then
        self.retry_time = GameTime() + const.HourDuration
        return
    end
    local indoor_hygiene_building = MapFindNearest(unit, unit, 200 * guim, "IndoorHygieneBuilding", nil, nil, function(obj)
        return GetConnectedBoiler(obj) and obj:CreateIndoorHygieneActivityObject():IsActivityFree()
    end)
    local threshold = indoor_hygiene_building and unit.DirtyThreshold or unit.SmellyThreshold
    return unit.Hygiene <= threshold
end

function WashHandPumpActivityObject:Done()
end