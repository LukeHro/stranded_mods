local NoIdlePowerConsumption = false

function OnMsg:ModsReloaded()
    if table.find(ModsLoaded, "id", "LH_IdlePowerConsumption") then
        print("Using NoIdlePowerConsumption for boilers.")
        NoIdlePowerConsumption = true
    end
end

--TODO some sort of cashing
function GetConnectedBoiler(consumer)
    if not consumer.room then
        return
    end
    for _, boiler in ipairs(consumer.player.labels.Boiler) do
        local used = next(boiler.water_consumers)
        if boiler.room == consumer.room and boiler:CanWork() and (not used or used == consumer) then
            return boiler
        end
    end
    for _, boiler in ipairs(consumer.player.labels.ElectricBoiler) do
        if boiler.room == consumer.room and boiler:CanWork() then
            return boiler
        end
    end
    local rooms = {}
    consumer.room:GatherAllNeighbourhoodRooms(rooms)
    for _, boiler in ipairs(consumer.player.labels.Boiler) do
        local used = next(boiler.water_consumers)
        if boiler.room and boiler:CanWork() and table.find(rooms, boiler.room) and (not used or used == consumer) then
            return boiler
        end
    end
    for _, boiler in ipairs(consumer.player.labels.ElectricBoiler) do
        if boiler.room and boiler:CanWork() and table.find(rooms, boiler.room) then
            return boiler
        end
    end
end

DefineClass.BoilerBase = {
    water_consumers = false,
}

function BoilerBase:Init()
    self.water_consumers = {}
end

function BoilerBase:AddWaterConsumer(consumer)
    self.water_consumers = self.water_consumers or {}
    table.insert_unique(self.water_consumers, consumer)
    if self:IsKindOf("PowerComponent") then
        self:UpdateNoIdlePowerConsumption()
    end
end

function BoilerBase:RemoveWaterConsumer(consumer)
    self.water_consumers = self.water_consumers or {}
    table.remove_entry(self.water_consumers, consumer)
    if self:IsKindOf("PowerComponent") then
        self:UpdateNoIdlePowerConsumption()
    end
end

DefineClass.ElectricBoilerBuilding = {
    __parents = { "BoilerBase", "CompositeBuilding" },
}

function ElectricBoilerBuilding:UpdateNoIdlePowerConsumption()
    if (NoIdlePowerConsumption) then
        PowerComponent.UpdatePowerElement(self)
        self:UpdateWorking(true)
        self:GetPowerGrid():ForceUpdateGrid("update_consumers")
        ObjModified(self)
    end
end

local CalcPowerConsumption_PowerComponent = PowerComponent.CalcPowerConsumption
--can't have BoilerBuilding:CalcPowerConsumption() because multiple inheritance
function PowerComponent:CalcPowerConsumption()
    if NoIdlePowerConsumption and self:IsKindOf("ElectricBoilerBuilding") then
        if self.water_consumers and #self.water_consumers == 0 then
            self.power_idle = true
            return 0
        else
            self.power_idle = nil
        end
    end
    return CalcPowerConsumption_PowerComponent(self)
end

DefineClass.BoilerBuilding = {
    __parents = { "BoilerBase", "StoveBase" },
    UpdateClipPlane = empty_func,
    boiler = false,
}

function BoilerBuilding:Init()
    self.boiler = PlaceObject("BoilerAttach", nil, const.cofComponentAttach)
    self:Attach(self.boiler)
    self:SetClipPlane(PlaneFromPoints(0, 0, 1000, 0, 1, 1000, 1, 0, 1000, true))
    self.boiler:SetClipPlane(PlaneFromPoints(0, 0, -100, 1, 0, -100, 0, 1, -100, true))
end

function BoilerBuilding:Done()
    if IsValid(self.boiler) then
        DoneObject(self.boiler)
    end
end

function SavegameFixups:RemoveOldBoilersFromGrid()
    MapForEach(true, "BoilerBuilding", function(building)
        if building.power_element and building.power_element.grid then
            print("removing old boiler consumption...")
            building.power_element.grid:RemoveElement(building.power_element)
            building.power_element = nil
        end
    end)
end

function BoilerBuilding:OnConsume(...)
    print("fixup didn't work") --some people claim that
    self.power_element.grid:RemoveElement(self.power_element)
end

function BoilerBuilding:SetConsumption(...)
end

function BoilerBuilding:SetPowerGrid(...)
end