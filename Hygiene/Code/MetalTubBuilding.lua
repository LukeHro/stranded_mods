DefineClass.MetalTubBuilding = {
    __parents = { "IndoorHygieneBuilding" },
    hygiene_activity_type = "Bath",
    hygiene_activity_object_class = "MetalTubActivityObject",
    water_flow = false,
    water_full = false,
}

function MetalTubBuilding:Init()
    self.water_flow = PlaceObject("MetalTubWaterFlow", nil, const.cofComponentAttach)
    self.water_full = PlaceObject("MetalTubWaterFull", nil, const.cofComponentAttach)
    self:Attach(self.water_flow)
    self:Attach(self.water_full)
    self.water_flow:SetVisible(false)
    self.water_full:SetVisible(false)
end

function MetalTubBuilding:DoneObject()
    if IsValid(self.water_flow) then
        DoneObject(self.water_flow)
    end
end

function MetalTubBuilding:GatherDirectOrders(unit, orders)
    if unit and not unit:HasUnitTag("Android") and self:CanWork() then
        CreateDirectOrderButtons(orders, DirectOrders.Bath, self)
    end
end

function MetalTubBuilding:GatherIPStatuses(statuses, suspended)
    if not self.room then
        statuses[InfopanelStatuses.ShowerNotInRoom] = true
    elseif not GetConnectedBoiler(self) then
        statuses[InfopanelStatuses.ShowerNoBoiler] = true
    end
end

function MetalTubBuilding:GetIPMetalTubInfo()
    if not self.room then
        return T("Not working, not in a fully built room.")
    elseif not GetConnectedBoiler(self) then
        return T("Not working, no working boiler in house.")
    end
end

DefineClass.MetalTubActivityObject = {
    __parents = { "ActivityObject" },
    activity_id = "Bath",
    retry_time = false,
}

function MetalTubActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

function MetalTubActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local tub = self:GetActivityTarget()
    local boiler = tub and GetConnectedBoiler(tub)
    if not boiler then
        return
    end
    local scale = unit:GetScale()
    unit:PushDestructor(function()
        local anim_duration = unit:GetAnimDuration("sleepGround_To_Standing")
        unit:SetScale(scale, anim_duration)
        unit:PlayMomentTrackedAnim("sleepGround_To_Standing", 1, nil, 200, anim_duration)
        unit:DressWashed()
        unit.is_washing = false
        if IsValid(boiler) then
            boiler:RemoveWaterConsumer(tub)
        end
        tub.water_flow:SetVisible(false)
        tub.water_full:SetVisible(false)
        tub:CreateIndoorHygieneActivityObject()
    end)
    boiler:AddWaterConsumer(tub)
    local spot = tub:GetSpotPos(tub:GetSpotBeginIndex("Bath"))
    tub.water_flow:SetVisible(true)
    unit:UndressWash(tub)
    local anim_duration = unit:GetAnimDuration("standing_To_SleepGround") / 2
    unit:SetScale(100, anim_duration)
    unit:SetPos(spot, anim_duration)
    unit:PlayMomentTrackedAnim("standing_To_SleepGround", 1, nil, 200, anim_duration)
    local time = unit:CalcWashingDuration()
    unit.is_washing = true
    local end_time = GameTime() + time
    unit:StatusUIUpdateData("activity", {
        bar1_start_time = GameTime(),
        bar1_end_time = end_time
    })
    anim_duration = unit:GetAnimDuration("sitGround2_Idle_Talk")
    local remaining = end_time - GameTime()
    tub.water_flow:SetVisible(false)
    tub.water_full:SetVisible(true)
    while remaining > 0 do
        unit:PlayMomentTrackedAnim("sitGround2_Idle_Talk", 1, nil, 200, Min(remaining, anim_duration))
        remaining = end_time - GameTime()
        if unit.visit_restart then
            return
        end
    end
    self:delete()
    unit:PopAndCallDestructor()
    return true
end

function MetalTubActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    if unit:HasUnitTag("Android") then
        return false
    end
    local target = self:GetActivityTarget()
    if not GetConnectedBoiler(target) then
        return false
    end
    if direct_order then
        return true
    end
    if (self.retry_time or 0) > GameTime() then
        return
    end
    if not CheckHygieneBuildingAccess(unit, target) then
        return
    end
    local area_is_locked = not target:IsSafeForApproaching(unit)
    if area_is_locked then
        self.retry_time = GameTime() + const.HourDuration
        return
    end
    return unit.Hygiene <= unit.SmellyThreshold
end

function MetalTubActivityObject:Done()
end