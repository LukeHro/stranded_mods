DefineClass.ShowerBuilding = {
    __parents = { "IndoorHygieneBuilding", "CommandObject" },
    hygiene_activity_type = "Shower",
    hygiene_activity_object_class = "ShowerActivityObject",
    water_flow = false,
}

function ShowerBuilding:Init()
    self.water_flow = self.water_flow or PlaceObject("ShowerWater", nil, const.cofComponentAttach)
    self:Attach(self.water_flow, self:GetRandomSpot("Water"))
    self.active = false
    self.water_flow:SetVisible(false)
end

function ShowerBuilding:DoneObject()
    if IsValid(self.water_flow) then
        DoneObject(self.water_flow)
    end
end

function ShowerBuilding:GatherDirectOrders(unit, orders)
    if unit and not unit:HasUnitTag("Android") and self:CanWork() then
        CreateDirectOrderButtons(orders, DirectOrders.Shower, self)
    end
end

function ShowerBuilding:GatherIPStatuses(statuses, suspended)
    if not self.room then
        statuses[InfopanelStatuses.ShowerNotInRoom] = true
    elseif not GetConnectedBoiler(self) then
        statuses[InfopanelStatuses.ShowerNoBoiler] = true
    end
end

function ShowerBuilding:GetIPShowerInfo()
    if not self.room then
        return T("Not working, not in a fully built room.")
    elseif not GetConnectedBoiler(self) then
        return T("Not working, no working boiler in house.")
    end
end

function ShowerBuilding:CmdRunWater(time)
    self:PushDestructor(function()
        self.active = false
        self:UpdateOperationMode()
        self.water_flow:SetVisible(false)
    end)
    self.water_flow:SetOpacity(0)
    self.water_flow:SetVisible(true)
    self.active = true
    self:UpdateOperationMode()
    local delta = time / 12
    self.water_flow:SetOpacity(50, delta)
    Sleep(delta)
    for _ = 1, 5 do
        self.water_flow:SetOpacity(20, delta)
        Sleep(delta)
        self.water_flow:SetOpacity(50, delta)
        Sleep(delta)
    end
    self.water_flow:SetOpacity(0, delta)
    Sleep(delta)
    self:PopAndCallDestructor()
end

DefineClass.ShowerActivityObject = {
    __parents = { "ActivityObject" },
    ActivityDuration = 20000,
    activity_id = "Shower",
    retry_time = false,
}

function ShowerActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

function ShowerActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local shower = self:GetActivityTarget()
    local boiler = shower and GetConnectedBoiler(shower)
    if not boiler then
        return
    end
    unit:PushDestructor(function(unit)
        unit:DressWashed()
        unit.is_washing = false
        if IsValid(shower) then
            shower:SetCommand("Idle")
        end
        if IsValid(boiler) then
            boiler:RemoveWaterConsumer(shower)
        end
        shower:CreateIndoorHygieneActivityObject()
    end)
    boiler:AddWaterConsumer(shower)
    local time = unit:CalcWashingDuration()
    unit.is_washing = true
    local end_time = GameTime() + time
    unit:StatusUIUpdateData("activity", {
        bar1_start_time = GameTime(),
        bar1_end_time = end_time
    })
    unit:UndressWash(shower)
    shower:SetCommand("CmdRunWater", time)
    local anim_duration = unit:GetAnimDuration("standing_Equip")
    local remaining = end_time - GameTime()
    while remaining > 0 do
        if not IsValid(boiler) or not boiler:CanWork() then
            unit:PopAndCallDestructor()
            return
        end
        unit:PlayMomentTrackedAnim("standing_Equip", 1, nil, 200, Min(remaining, anim_duration))
        remaining = end_time - GameTime()
        if unit.visit_restart then
            return
        end
    end
    unit:PopAndCallDestructor()
end

function ShowerActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    if unit:HasUnitTag("Android") then
        return false
    end
    local target = self:GetActivityTarget()
    if not GetConnectedBoiler(target) then
        return false
    end
    if direct_order then
        return true
    end
    if (self.retry_time or 0) > GameTime() then
        return
    end
    if not CheckHygieneBuildingAccess(unit, target) then
        return
    end
    local area_is_locked = not target:IsSafeForApproaching(unit)
    if area_is_locked then
        self.retry_time = GameTime() + const.HourDuration
        return
    end
    return unit.Hygiene <= unit.SmellyThreshold
end

function ShowerActivityObject:Done()
end