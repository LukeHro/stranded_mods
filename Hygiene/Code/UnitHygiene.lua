AppendClass.Human = {
    __parents = {"UnitHygiene"}
}

function OnMsg.ModsReloaded()
    local parent = XTemplates.tabOverview_200_HumanBars[1][1]
    if parent.hygiene_added then
        return
    end
    parent[#parent + 1] = PlaceObj('XTemplateWindow', nil, {
        PlaceObj('XTemplateWindow', {
            '__class', "XImage",
            'Margins', box(0, 0, 5, 0),
            'Dock', "left",
            'Image', CurrentModPath .. "UI/Hud/st_wash",
        }),
        PlaceObj('XTemplateWindow', {
            '__context', function (parent, context) return SubContext(context, {context:GetPropContext("Hygiene")}) end,
            '__class', "FrameProgress",
            'RolloverTemplate', "Rollover",
            'RolloverText', T("Survivors need to <em>keep themselves clean</em>. They will need water to wash."),
            'RolloverTitle', T("Hygiene"),
            'Id', "idHygieneBar",
            'RolloverOnFocus', true,
            'RelativeFocusOrder', "new-line",
            'FXMouseIn', "ButtonHoverBasic",
            'BindTo', "HygienePct",
            'BarColor', RGBA(96, 116, 127, 255),
            'Text', T("Hygiene<right><percent(Progress,MaxProgress)>"),
        }, {
            PlaceObj('XTemplateWindow', {
                '__class', "XFrame",
                'Id', "idRollover",
                'Dock', "box",
                'Visible', false,
                'FoldWhenHidden', true,
                'Image', "UI/Hud/ip_bar_selection",
                'FrameBox', box(15, 5, 15, 12),
            }),
        }),
    })
    parent.hygiene_added = true
end

DefineClass.UnitHygiene = {
    __parents = { "UnitComponent" },
    properties = {
        { category = "Hygiene", id = "Hygiene", editor = "number", scale = 1000, default = 100000, min = 0, max = 100000 },
        { category = "Hygiene", id = "HygieneLossPerDay", name = T("Daily hygiene loss"), editor = "number", modifiable = true, scale = 1000, default = 50000 },
        { category = "Hygiene", id = "HygieneGainPerHour", name = T("Wash hygiene gain"), editor = "number", modifiable = true, scale = 1000, default = 150000 },
        { category = "Hygiene", id = "CleanThreshold", name = T("Clean threshold"), editor = "number", modifiable = true, scale = 1000, default = 75000, min = 0, max = 100000 },
        { category = "Hygiene", id = "SmellyThreshold", name = T("Smelly threshold"), editor = "number", modifiable = true, scale = 1000, default = 50000, min = 0, max = 100000 },
        { category = "Hygiene", id = "DirtyThreshold", name = T("Dirty threshold"), editor = "number", modifiable = true, scale = 1000, default = 25000, min = 0, max = 100000 },
    },
    is_washing = false,
    washing_timeout = false,
}

function UnitHygiene:IsDesperatelyDoingSomething()
    return self.command == "CmdDesperateWash"
end

AutoResolveMethods.ChangeHygiene = "call"
function UnitHygiene:ChangeHygiene(hygiene)
    if self:HasUnitTag("Android") then
        return
    end
    local old_hygiene = self.Hygiene
    hygiene = Clamp(old_hygiene + (hygiene or 0), 0, 100000)
    if hygiene ~= old_hygiene then
        self.Hygiene = hygiene
        if hygiene / 1000 ~= old_hygiene / 1000 then
            self:NetUpdateHash("UnitChangeHygiene", hygiene, old_hygiene)
            Msg("UnitChangeHygiene", self, hygiene, old_hygiene)
            ObjModified(self:GetPropContext("Hygiene"))
        end
        if (self.Hygiene > self.CleanThreshold) then
            self:AddHappinessFactor("CleanHygiene", "hygiene")
        elseif (self.Hygiene < self.DirtyThreshold) then
            self:AddHappinessFactor("DirtyHygiene", "hygiene")
        elseif (self.Hygiene < self.SmellyThreshold) then
            self:AddHappinessFactor("SmellyHygiene", "hygiene")
        else
            self:RemoveHappinessFactor("CleanHygiene", "hygiene")
            self:RemoveHappinessFactor("DirtyHygiene", "hygiene")
            self:RemoveHappinessFactor("SmellyHygiene", "hygiene")
        end
    end
    if self.Hygiene > 0 or self.is_washing
            or self:IsManuallyControlled() or self:IsSleeping() or self:IsAwaitingRescue() or self:IsOnExpedition()
            or self.current_activity == "Shower" or self.current_activity == "WashHandPump" or self.current_activity == "WashPond"
            or self:IsDesperatelyDoingSomething() then
        return
    end
    local place, activity = self:FindWashingPlace()
    if place and activity then
        self:TrySetCommand("CmdDesperateWash", place, activity)
    end
end

function UnitHygiene:FindWashingPlace()
    local current_time = GameTime()
    if self.washing_timeout and (self.washing_timeout > current_time) then
        return
    end
    self.washing_timeout = current_time + DivRound(const.HourDuration, 4)
    local indoor_hygiene_building = self:GetOwnedObject("ShowerBuilding") or self:GetOwnedObject("MetalTubBuilding")
    if indoor_hygiene_building and GetConnectedBoiler(indoor_hygiene_building) and CheckHygieneBuildingAccess(self, indoor_hygiene_building)
            and indoor_hygiene_building:CreateIndoorHygieneActivityObject():IsActivityFree() then
        return indoor_hygiene_building, indoor_hygiene_building.hygiene_activity_type
    end
    indoor_hygiene_building = MapFindNearest(self, self, 500 * guim, "IndoorHygieneBuilding", nil, nil, function(obj)
        return GetConnectedBoiler(obj) and obj:CreateIndoorHygieneActivityObject():IsActivityFree() and CheckHygieneBuildingAccess(self, obj)
    end)
    local pump = MapFindNearest(self, self, 500 * guim, "HandPumpBuilding", nil, nil, function(obj)
        return obj.working and obj:CreateWashHandPumpActivityObject():IsActivityFree() and CheckHygieneBuildingAccess(self, obj)
    end)
    local pond = self.wash_at_pond and FindNearestWater(self:GetPos())

    local indoor_hygiene_dist = indoor_hygiene_building and self:GetVisualDist2D(indoor_hygiene_building)
    local pump_dist = pump and self:GetVisualDist2D(pump)
    local pond_dist = pond and self:GetVisualDist2D(pond)

    if pond_dist and (not pump_dist or pond_dist < pump_dist / 4) and (not indoor_hygiene_dist or pond_dist < indoor_hygiene_dist / 4) then
        return pond, "WashPond"
    elseif pump_dist and (not indoor_hygiene_dist or pump_dist < indoor_hygiene_dist / 4) then
        return pump:CreateWashHandPumpActivityObject(), "WashHandPump"
    elseif indoor_hygiene_dist then
        return indoor_hygiene_building:CreateIndoorHygieneActivityObject(), indoor_hygiene_building.hygiene_activity_type
    end
end

function UnitHygiene:CmdDesperateWash(place, activity)
    self:ForceSetActivity(activity, place, nil, nil, const.CommandImportance.CmdDesperateRest)
end

function UnitHygiene:CheatToggleGodMode()
    if not self.god_mode then
        self.ChangeHygiene = nil
        return
    end
    self:ChangeHygiene(100000)
    self.ChangeHygiene = empty_func
end

function UnitHygiene:CalcWashingDuration()
    return Max(MulDivRound(100000 - self.Hygiene, const.HourDuration, self.HygieneGainPerHour), DivRound(const.HourDuration, 4))
end

function UnitHygiene:CalcHygiene(interval)
    if self.is_washing then
        return MulDivRound(self.HygieneGainPerHour, interval, const.HourDuration)
    end
    if not self:IsSleeping() then
        return MulDivRound(-self.HygieneLossPerDay, interval, const.DayDuration)
    end
end

function UnitHygiene:OnObjUpdate(time, update_interval)
    if self.Health <= 0 then return end
    self:ChangeHygiene(self:CalcHygiene(update_interval))
end

function UnitHygiene:GetHygienePct()
    return Min(self.Hygiene / 1000, 100)
end

function UnitHygiene:WashSelf(duration, target)
    self:PushDestructor(function(unit)
        unit:PlayMomentTrackedAnim("bending_Unequip_Corpse_End", 1, nil, 200, unit:GetAnimDuration("butchering_End"))
        unit.is_washing = false
        unit:DressWashed()
    end)
    local end_time = GameTime() + duration
    self:StatusUIUpdateData("activity", { bar1_start_time = GameTime(), bar1_end_time = end_time })
    self:UndressWash(target)
    self.is_washing = true
    local anim_duration = self:GetAnimDuration("bending_Unequip_Corpse_Start")
    self:PlayMomentTrackedAnim("bending_Unequip_Corpse_Start", 1, nil, 200, anim_duration)
    anim_duration = self:GetAnimDuration("bend_DropDown_Basket_Low")
    local remaining = end_time - GameTime()
    while remaining > 0 do
        self:PlayMomentTrackedAnim("bend_DropDown_Basket_Low", 1, nil, 200, Min(remaining, anim_duration))
        remaining = end_time - GameTime()
        if self.visit_restart then
            return
        end
    end
    self:PopAndCallDestructor()
end

function UnitHygiene:CmdWashAtPond(activity, target)
    self:SafeGoto(target)
    self:WashSelf(self:CalcWashingDuration(), target)
    return true
end

local FindRelaxationRoutine_UnitRelaxation = UnitRelaxation.FindRelaxationRoutine
function UnitRelaxation:FindRelaxationRoutine(...)
    if self.Hygiene < self.DirtyThreshold then
        return --nothing, should prioritize washing
    end
    return FindRelaxationRoutine_UnitRelaxation(self, ...)
end

local clothes_slots = table.copy(EquipSlots)
table.remove_if(clothes_slots, function(slot)
    return table.find(WeaponSlots, slot) or table.find(ToolSlots, slot)
end)

function UnitHygiene:UndressWash(hygiene_building)
    if IsKindOf(hygiene_building, "IndoorHygieneBuilding") then
        self.FirstEffectById = function(unit, id)
            if id == "ClothesMissing" then
                return true
            end
            return StatusEffectsObject.FirstEffectById(unit, id)
        end
    end
    self.undressed = self.undressed or {}
    for _, slot in pairs(clothes_slots) do
        self.undressed[slot] = {}
        self.undressed[slot].item, self.undressed[slot].res = self:Unequip(slot)
    end
end

function UnitHygiene:DressWashed()
    for _, slot in pairs(clothes_slots) do
        local equipment = self.undressed[slot]
        if equipment and equipment.item and equipment.res then
            self:Equip(equipment.item.id, slot, equipment.res)
        end
        self.undressed[slot] = {}
    end
    self.FirstEffectById = nil
end