function SinkBuilding:OnObjUpdate(...)
    if not IsConstructionFinished(self) then
        return
    end
    self:CreateWashSinkActivityObject()
end

function SinkBuilding:CreateWashSinkActivityObject()
    if not IsValid(self) or IsBeingDestructed(self) then
        return
    end
    local obj = self:GetAttach("WashSinkActivityObject")
    if IsValid(obj) then
        return obj
    end
    obj = CreateActivityObject("WashSinkActivityObject", { player = self.player }, self)
    obj:SetActivityAvailable(true)
    return obj
end

function SinkBuilding:RemoveWashSinkActivityObject()
    local obj = self:GetAttach("WashSinkActivityObject")
    if IsValid(obj) then
        obj:delete()
    end
end

function SinkBuilding:GatherDirectOrders(unit, orders)
    if unit and not unit:HasUnitTag("Android") and self:CanWork() then
        CreateDirectOrderButtons(orders, DirectOrders.WashSink, self)
    end
end

function SinkBuilding:GetAssignedSurvivor()
    local activity = self:GetAttach("WashSinkActivityObject")
    return activity and activity.activity_unit
end

DefineClass.WashSinkActivityObject = {
    __parents = { "ActivityObject" },
    activity_id = "WashSink",
    retry_time = false,
}

function WashSinkActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

function WashSinkActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local sink = self:GetActivityTarget()
    if not sink then
        return
    end
    if sink:IsKindOf("SinkBuilding") then
        unit:PushDestructor(function()
            sink.water_flow:SetVisible(false)
        end)
        sink.water_flow:SetVisible(true)
        unit:PlayMomentTrackedAnim("standing_Build_Hammer_Idle", 1, nil, 200, 1000)
    elseif sink:IsKindOf("HandPumpBuilding") then
        unit:PushDestructor(function()
            sink:RemoveWater()
            sink:CreateWashHandPumpActivityObject()
        end)
        unit:Face(sink:GetSpotPos(sink:GetSpotBeginIndex("Pump")))
        sink:SetCommand("CmdPumpWater", 3)
        unit:PlayMomentTrackedAnim("bend_Cut_Tool", 1, nil, 200, const.HourDuration / 10)
    else
        print("unknown device for washing hands...")
        return
    end
    unit:ChangeHygiene(10000)
    unit:PopAndCallDestructor()
    self:delete()
    return true
end

function WashSinkActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    if unit:HasUnitTag("Android") then
        return false
    end
    if direct_order then
        return true
    end
    if (self.retry_time or 0) > GameTime() then
        return
    end
    local target = self:GetActivityTarget()
    if not CheckHygieneBuildingAccess(unit, target) then
        return
    end
    local area_is_locked = not target:IsSafeForApproaching(unit)
    if area_is_locked then
        self.retry_time = GameTime() + const.HourDuration
        return
    end
    return unit.Hygiene < 100000
end

function WashSinkActivityObject:Done()
end