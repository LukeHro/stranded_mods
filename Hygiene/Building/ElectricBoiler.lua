UndefineClass('ElectricBoiler')
DefineClass.ElectricBoiler = {
	__parents = { "ElectricBoilerBuilding", "PowerComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "ElectricBoilerBuilding",
	LockState = "hidden",
	BuildMenuCategory = "Water",
	display_name = T(106177514328, --[[ModItemBuildingCompositeDef ElectricBoiler display_name]] "Electric boiler"),
	description = T(749326591829, --[[ModItemBuildingCompositeDef ElectricBoiler description]] "Warm and helps distributing water inside buildings. Can service all the bathing facilities inside a house."),
	BuildMenuIcon = "Mod/LH_Hygiene/UI/Icons/Build Menu/electric_boiler.dds",
	BuildMenuPos = 135,
	display_name_pl = T(264483901273, --[[ModItemBuildingCompositeDef ElectricBoiler display_name_pl]] "Electric boilers"),
	display_name_short = T(275926848858, --[[ModItemBuildingCompositeDef ElectricBoiler display_name_short]] "Electric boiler"),
	entity = "Boiler",
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
		ScrapElectronics = 2000,
	}),
	construction_points = 40000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
		PlaceObj('ConstructionCheckRoomPlacement', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		Metal = 5000,
		ScrapElectronics = 1000,
	}),
	Health = 60000,
	MaxHealth = 60000,
	attached_to_wall = true,
	RoomPlacement = "indoors",
	CanPlaceInShelter = false,
	lock_block_box = box(0, -300, 0, 600, 300, 1750),
	SkirtSize = 228,
	EntityHeight = 1115,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	PowerComponent = true,
	can_change_ownership = false,
	IsPowerConsumer = true,
	PowerConsumption = 20000,
}

