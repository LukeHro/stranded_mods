UndefineClass('Shower')
DefineClass.Shower = {
	__parents = { "ShowerBuilding", "HeatSourceComponent", "OwnedComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "ShowerBuilding",
	LockState = "hidden",
	BuildMenuCategory = "Water",
	display_name = T(286258901247, --[[ModItemBuildingCompositeDef Shower display_name]] "Shower"),
	description = T(819309511444, --[[ModItemBuildingCompositeDef Shower description]] "Take a relaxing shower. Warm water heated by a Boiler must be available."),
	BuildMenuIcon = "UI/Icons/Build Menu/shower.dds",
	BuildMenuPos = 125,
	display_name_pl = T(137700435569, --[[ModItemBuildingCompositeDef Shower display_name_pl]] "Showers"),
	display_name_short = T(884749797461, --[[ModItemBuildingCompositeDef Shower display_name_short]] "Shower"),
	entity = "Shower",
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 30000,
	}),
	construction_points = 40000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
		PlaceObj('ConstructionCheckRoomPlacement', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	Health = 60000,
	MaxHealth = 60000,
	attached_to_wall = true,
	RoomPlacement = "indoors",
	CanPlaceInShelter = false,
	lock_block_box = box(0, 0, 350, 900, 300, 2100),
	SkirtSize = 228,
	EntityHeight = 1115,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	HeatSourceComponent = true,
	OwnedComponent = true,
	PowerTemperature = 30000,
	RadiationTemperature = 30000,
	RadiationRange = 500,
	ownership_class = "ShowerBuilding",
	ChangeOwnerIcon = "Mod/LH_Hygiene/UI/Icons/Infopanels/shower_change_owner.dds",
	Filter = function (obj)
		return not obj:HasUnitTag("Android")
	end,
}

