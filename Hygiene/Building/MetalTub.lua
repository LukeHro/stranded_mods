UndefineClass('MetalTub')
DefineClass.MetalTub = {
	__parents = { "MetalTubBuilding", "HeatSourceComponent", "OwnedComponent", "RelaxationDeviceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "MetalTubBuilding",
	LockState = "hidden",
	BuildMenuCategory = "Water",
	display_name = T(236376959891, --[[ModItemBuildingCompositeDef MetalTub display_name]] "Bath tub"),
	description = T(547324341449, --[[ModItemBuildingCompositeDef MetalTub description]] "Take a relaxing bath. Warm water heated by a Boiler must be available."),
	BuildMenuIcon = "Mod/LH_Hygiene/UI/Icons/Build Menu/metal_tub.dds",
	BuildMenuPos = 120,
	display_name_pl = T(870006916204, --[[ModItemBuildingCompositeDef MetalTub display_name_pl]] "Bath tubs"),
	entity = "MetalTub",
	update_interval = 5000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 30000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {}),
	Health = 160000,
	MaxHealth = 160000,
	RoomPlacement = "indoors",
	lock_block_box = box(-300, -300, 0, 1500, 300, 700),
	SkirtSize = 10,
	EntityHeight = 667,
	orient_to_terrain = true,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	HeatSourceComponent = true,
	OwnedComponent = true,
	RelaxationDeviceComponent = true,
	PowerTemperature = 30000,
	RadiationTemperature = 30000,
	RadiationRange = 1000,
	ownership_class = "MetalTub",
	ChangeOwnerIcon = "Mod/LH_Hygiene/UI/Icons/Infopanels/metal_tub_change_owner.dds",
	ManageActiveState = true,
}

