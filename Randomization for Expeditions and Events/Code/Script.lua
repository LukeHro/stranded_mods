RandomizableStoryBitSetCooldowns = {
    "Positive",
    "Negative"
}

local GetCooldown_CooldownObj = CooldownObj.GetCooldown
function CooldownObj:GetCooldown(cooldown_id)
    local result = GetCooldown_CooldownObj(self, cooldown_id)
    if not table.find(RandomizableStoryBitSetCooldowns, cooldown_id) or not result then
        return result
    end
    local last = self["LastCooldownSet_" .. cooldown_id] or result
    local skip = AsyncRand(100) > (result * 100 / last)
    if skip then
        return
    end
    return result
end

local SetCooldown_CooldownObj = CooldownObj.SetCooldown
function CooldownObj:SetCooldown(cooldown_id, time, max)
    local result = SetCooldown_CooldownObj(self, cooldown_id, time, max)
    self["LastCooldownSet_" .. cooldown_id] = GetCooldown_CooldownObj(self, cooldown_id)
    return result
end