return PlaceObj('ModDef', {
	'title', "Animals Drink",
	'description', "Adds Troughs for tamed animals, they need to drink too. Survivors will fill them with water from hand pumps or sinks.\n\nTroughs use the vanilla Feeder model while the feeders are changed by this mod to a smaller size holding 2 stacks of food instead of 3 in order to distinguish better between the two (and because we know they were holding too much food).\n\n[url=steamcommunity.com/sharedfiles/filedetails/?id=3152870766][b]Water[/b][/url] mod required!\n\n[url=paypal.me/LukeHro][b]Paypal Support[/b][/url]",
	'image', "Mod/LH_Troughs/UI/Trough.png",
	'last_changes', "Fixed overeating",
	'dependencies', {
		PlaceObj('ModDependency', {
			'id', "LH_Water",
			'title', "Water",
			'version_major', 1,
		}),
	},
	'id', "LH_Troughs",
	'author', "lukeh_ro",
	'version_major', 1,
	'version_minor', 1,
	'version', 4,
	'lua_revision', 233360,
	'saved_with_revision', 347716,
	'entities', {
		"AnimalFeeder",
	},
	'code', {
		"Building/AnimalTrough_Metal.lua",
		"Building/AnimalTrough_Stone.lua",
		"Building/AnimalTrough_Wood.lua",
		"Code/AnimalThirst.lua",
		"Code/Feeder.lua",
		"Code/Trough.lua",
		"Entities/AnimalFeeder.lua",
	},
	'has_data', true,
	'saved', 1707286190,
	'code_hash', -823291983400375893,
	'steam_id', "3153991722",
	'TagAnimals', true,
	'TagBuildings', true,
	'TagGameplay', true,
	'TagOther', true,
})