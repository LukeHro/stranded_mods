UndefineClass('AnimalTrough_Wood')
DefineClass.AnimalTrough_Wood = {
	__parents = { "TroughBuilding", "StorageDepotComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "TroughBuilding",
	LockState = "hidden",
	unload_anim_handsclose = "standing_DropDown_HandsClose_Low",
	load_anim_handsclose = "standing_PickUp_HandsClose_Low",
	TreatStorageAsDevice = true,
	show_amount_UI = true,
	BuildMenuCategory = "sub_AnimalTroughs",
	display_name = T(897311357484, --[[ModItemBuildingCompositeDef AnimalTrough_Wood display_name]] "Wooden trough"),
	description = T(829983245336, --[[ModItemBuildingCompositeDef AnimalTrough_Wood description]] "A drinking rack for domesticated animals, besides food they need water too."),
	menu_display_name = T(737261111134, --[[ModItemBuildingCompositeDef AnimalTrough_Wood menu_display_name]] "Wooden"),
	BuildMenuIcon = "UI/Icons/Build Menu/animal_feeder_wood",
	BuildMenuPos = 1,
	display_name_pl = T(757158006355, --[[ModItemBuildingCompositeDef AnimalTrough_Wood display_name_pl]] "Wooden troughs"),
	display_name_short = T(798163076766, --[[ModItemBuildingCompositeDef AnimalTrough_Wood display_name_short]] "Trough"),
	entity = "AnimalFeeder_Wood",
	labels = {
		"AnimalFeeder",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Wood = 15000,
	}),
	construction_points = 3750,
	Health = 60000,
	MaxHealth = 60000,
	upgrade_label = "Trough",
	lock_block_box = box(-450, -1650, 0, 450, 450, 350),
	SkirtSize = 138,
	EntityHeight = 0,
	orient_to_terrain = true,
	CustomMaterial = "Wood",
	attack_attraction = 5,
	prefer_in_buildmenu = true,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	StorageDepotComponent = true,
	stack_count = 2,
	accepted_res = {
		"ColdWater",
	},
	show_grouped_stacks = false,
	distribute_allowed_res = false,
	placement_spots = {
		"Resourceleft",
		"Resourceright",
	},
}

