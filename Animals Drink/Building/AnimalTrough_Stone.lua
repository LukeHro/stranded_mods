UndefineClass('AnimalTrough_Stone')
DefineClass.AnimalTrough_Stone = {
	__parents = { "TroughBuilding", "StorageDepotComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "TroughBuilding",
	LockState = "hidden",
	unload_anim_handsclose = "standing_DropDown_HandsClose_Low",
	load_anim_handsclose = "standing_PickUp_HandsClose_Low",
	TreatStorageAsDevice = true,
	show_amount_UI = true,
	BuildMenuCategory = "sub_AnimalTroughs",
	display_name = T(184077778595, --[[ModItemBuildingCompositeDef AnimalTrough_Stone display_name]] "Stone trough"),
	description = T(474948104620, --[[ModItemBuildingCompositeDef AnimalTrough_Stone description]] "A drinking rack for domesticated animals, besides food they need water too."),
	menu_display_name = T(414516277520, --[[ModItemBuildingCompositeDef AnimalTrough_Stone menu_display_name]] "Stone"),
	BuildMenuIcon = "UI/Icons/Build Menu/animal_feeder_stone",
	BuildMenuPos = 2,
	display_name_pl = T(446691176836, --[[ModItemBuildingCompositeDef AnimalTrough_Stone display_name_pl]] "Stone troughs"),
	display_name_short = T(459990543786, --[[ModItemBuildingCompositeDef AnimalTrough_Stone display_name_short]] "Trough"),
	entity = "AnimalFeeder_Stone",
	labels = {
		"AnimalFeeder",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Stone = 15000,
	}),
	construction_points = 6250,
	Health = 75000,
	MaxHealth = 75000,
	upgrade_label = "Trough",
	lock_block_box = box(-450, -1650, 0, 450, 450, 350),
	SkirtSize = 138,
	EntityHeight = 0,
	orient_to_terrain = true,
	ConstructionColorGetter = function (self)
		return Region.RockColor
	end,
	FinalColorGetter = function (self)
		return Region.RockColor
	end,
	CustomMaterial = "Stone",
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	StorageDepotComponent = true,
	stack_count = 2,
	accepted_res = {
		"ColdWater",
	},
	show_grouped_stacks = false,
	distribute_allowed_res = false,
	placement_spots = {
		"Resourceleft",
		"Resourceright",
	},
}

