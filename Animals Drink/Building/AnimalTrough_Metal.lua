UndefineClass('AnimalTrough_Metal')
DefineClass.AnimalTrough_Metal = {
	__parents = { "TroughBuilding", "StorageDepotComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "TroughBuilding",
	LockState = "hidden",
	unload_anim_handsclose = "standing_DropDown_HandsClose_Low",
	load_anim_handsclose = "standing_PickUp_HandsClose_Low",
	TreatStorageAsDevice = true,
	show_amount_UI = true,
	BuildMenuCategory = "sub_AnimalTroughs",
	display_name = T(585983273202, --[[ModItemBuildingCompositeDef AnimalTrough_Metal display_name]] "Metal trough"),
	description = T(927112133122, --[[ModItemBuildingCompositeDef AnimalTrough_Metal description]] "A drinking rack for domesticated animals, besides food they need water too."),
	menu_display_name = T(436817198539, --[[ModItemBuildingCompositeDef AnimalTrough_Metal menu_display_name]] "Metal"),
	BuildMenuIcon = "UI/Icons/Build Menu/animal_feeder_metal",
	BuildMenuPos = 3,
	display_name_pl = T(296002280792, --[[ModItemBuildingCompositeDef AnimalTrough_Metal display_name_pl]] "Metal troughs"),
	display_name_short = T(364465355337, --[[ModItemBuildingCompositeDef AnimalTrough_Metal display_name_short]] "Trough"),
	entity = "AnimalFeeder_Metal",
	labels = {
		"AnimalFeeder",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 15000,
	}),
	construction_points = 5000,
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 10000,
	}),
	Health = 90000,
	MaxHealth = 90000,
	upgrade_label = "Trough",
	lock_block_box = box(-450, -1650, 0, 450, 450, 350),
	SkirtSize = 138,
	EntityHeight = 0,
	orient_to_terrain = true,
	CustomMaterial = "Metal",
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	StorageDepotComponent = true,
	stack_count = 2,
	accepted_res = {
		"ColdWater",
	},
	show_grouped_stacks = false,
	distribute_allowed_res = false,
	placement_spots = {
		"Resourceleft",
		"Resourceright",
	},
}

