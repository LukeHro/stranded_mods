local function ShowError()
    if not table.find(ModsLoaded, "id", "LH_Water") then
        CreateRealTimeThread(CreateMessageBox, nil, T("Missing dependency!"),
                T("Animals Drink mod now requires Water mod.\nPlease subscribe to that it before continuing playing!"), T("OK"))
    end
end

OnMsg.GameTimeStart = ShowError
OnMsg.PostLoadGame = ShowError

function OnMsg:ModsReloaded()
    table.insert_unique(Techs.AnimalTaming, PlaceObj('RemoveLockedState',
            { Class = "BuildingCompositeDef", Group = "Water", LockState = "hidden", PresetId = "AnimalTrough_Metal" }))
    table.insert_unique(Techs.AnimalTaming, PlaceObj('RemoveLockedState',
            { Class = "BuildingCompositeDef", Group = "Water", LockState = "hidden", PresetId = "AnimalTrough_Stone" }))
    table.insert_unique(Techs.AnimalTaming, PlaceObj('RemoveLockedState',
            { Class = "BuildingCompositeDef", Group = "Water", LockState = "hidden", PresetId = "AnimalTrough_Wood" }))
end

DefineClass.TroughBuilding = {
    __parents = { "AnimalFeeder" },
    water_plane = false,
}

function TroughBuilding:Init()
    self.water_plane = self.smaller_right or PlaceObject("TroughWaterPlane", nil, const.cofComponentAttach)
    self:Attach(self.water_plane)
end

function TroughBuilding:Done()
    if IsValid(self.water_plane) then
        DoneObject(self.water_plane)
    end
end

function TroughBuilding:GetFirstAnimalFoodOutput()
    return "ColdWater"
end