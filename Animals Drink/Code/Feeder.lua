DefineClass.LH_SmallerFeederAttach = {
    __parents = { "ComponentAttach" },
    flags = { efWalkable = false, efApplyToGrids = false, efCollision = false }
}

local function InitSmallAttaches(feeder, entity)
    feeder.smaller_right = feeder.smaller_right or PlaceObject("LH_SmallerFeederAttach", nil, const.cofComponentAttach)
    feeder.smaller_right:ChangeEntity(entity)
    feeder.smaller_right:SetClipPlane(PlaneFromPoints(0, -800, 0, 0, -800, 1, -1, -800, 0, true))
    feeder:Attach(feeder.smaller_right)
    feeder.smaller_right:SetAttachOffset(0, 250, 0)
    feeder.smaller_right:ClearEnumFlags(const.efCollision)
    feeder.smaller_left = feeder.smaller_left or PlaceObject("LH_SmallerFeederAttach", nil, const.cofComponentAttach)
    feeder.smaller_left:ChangeEntity(entity)
    feeder.smaller_left:SetClipPlane(PlaneFromPoints(0, -300, 0, 0, -300, 1, 1, -300, 0, true))
    feeder:Attach(feeder.smaller_left)
    feeder.smaller_left:SetAttachOffset(0, -250, 0)
    feeder.smaller_left:ClearEnumFlags(const.efCollision)
end

local function DoneSmallAttaches(feeder)
    if IsValid(feeder.smaller_right) then
        DoneObject(feeder.smaller_right)
    end
    if IsValid(feeder.smaller_left) then
        DoneObject(feeder.smaller_left)
    end
end

function AnimalFeeder_Metal:Init()
    InitSmallAttaches(self, "AnimalFeeder_Metal")
end

function AnimalFeeder_Stone:Done()
    DoneSmallAttaches(self)
end

function AnimalFeeder_Stone:Init()
    InitSmallAttaches(self, "AnimalFeeder_Stone")
end

function AnimalFeeder_Stone:Done()
    DoneSmallAttaches(self)
end

function AnimalFeeder_Wood:Init()
    InitSmallAttaches(self, "AnimalFeeder_Wood")
end

function AnimalFeeder_Wood:Done()
    DoneSmallAttaches(self)
end

function SavegameFixups.FixAnimalFeederSizes()
    MapForEach("map", "AnimalFeeder_Metal", function(feeder)
        InitSmallAttaches(feeder, "AnimalFeeder_Metal")
    end)
    MapForEach("map", "AnimalFeeder_Stone", function(feeder)
        InitSmallAttaches(feeder, "AnimalFeeder_Stone")
    end)
    MapForEach("map", "AnimalFeeder_Wood", function(feeder)
        InitSmallAttaches(feeder, "AnimalFeeder_Wood")
    end)
end