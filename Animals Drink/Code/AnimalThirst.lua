function UnitAnimal:IsThirsty()
    self.thirsty = self.thirsty or 0
    return self:IsTamed() and self.thirsty < 10000
end

local OnObjUpdate_UnitAnimal = UnitAnimal.OnObjUpdate
function UnitAnimal:OnObjUpdate(game_time, update_interval)
    self.thirsty = self.thirsty or 0
    if GameTime() > (self.next_drink_reset or 0) then
        if self:IsTamed() and self.thirsty == 0 then
            self:AddHappinessFactor("AnimalIsThirsty")
        end
        self.next_drink_reset = GameTime() + const.DayDuration
        self.thirsty = 0
    end
    return OnObjUpdate_UnitAnimal(self, game_time, update_interval)
end

local GetRamainingDailyFoodNeeded_UnitAnimal = UnitAnimal.GetRamainingDailyFoodNeeded
function UnitAnimal:GetRamainingDailyFoodNeeded(...)
    return self:IsThirsty() and (10000 - self.thirsty) or GetRamainingDailyFoodNeeded_UnitAnimal(self, ...)
end

function UnitAnimal:IsHungry()
    return GetRamainingDailyFoodNeeded_UnitAnimal(self) > 0
end

function UnitAnimal:ShouldStartStarvation()
    return self:IsTamed() and self:IsHungry()
end

local FindFoodSource_UnitAnimal = UnitAnimal.FindFoodSource
function UnitAnimal:FindFoodSource(range, ignore_danger, food_set)
    if self:IsThirsty() then
        local source = FindFoodSource_UnitAnimal(self, range, ignore_danger, {"ColdWater"})
        if source and source:IsKindOf("TroughBuilding") then
            return source
        elseif not self:IsHungry() then
            return
        end
    end
    return FindFoodSource_UnitAnimal(self, range, ignore_danger, food_set)
end

local CheckFoodSource_UnitAnimal = UnitAnimal.CheckFoodSource
function UnitAnimal:CheckFoodSource(source, ...)
    local result, reason = CheckFoodSource_UnitAnimal(self, source, ...)
    if not result then
        return result, reason
    end
    if source:IsKindOf("TroughBuilding") and not self:IsThirsty() then
        return false, "not thirsty"
    elseif not source:IsKindOf("TroughBuilding") and not self:IsHungry() then
        return false, "not hungry"
    end
    return result, reason
end

local OnFoodEaten_UnitAnimal = UnitAnimal.OnFoodEaten
function UnitAnimal:OnFoodEaten(target, res, amount)
    if res == "ColdWater" then
        return
    else
        return OnFoodEaten_UnitAnimal(self, target, res, amount)
    end
end

local IsLookingForFood_UnitAnimalAutoResolve = UnitAnimalAutoResolve.IsLookingForFood
function UnitAnimalAutoResolve:IsLookingForFood()
    local looking = IsLookingForFood_UnitAnimalAutoResolve(self)
    self.was_last_eating = nil
    return looking
end

local OnAnimalFoodEaten_AnimalFoodSource = AnimalFoodSource.OnAnimalFoodEaten
function AnimalFoodSource:OnAnimalFoodEaten(res, amount, animal)
    if res == "ColdWater" then
        if amount > 0 then
            animal:RemoveHappinessFactor("AnimalIsThirsty")
        end
        animal.thirsty = (animal.thirsty or 0) + amount
        if animal.thirsty >= 10000 then
            animal.next_drink_reset = GameTime() + const.DayDuration
        end
    end
    return OnAnimalFoodEaten_AnimalFoodSource(self, res, amount, animal)
end