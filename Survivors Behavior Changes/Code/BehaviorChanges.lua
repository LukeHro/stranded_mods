ProductionDeviceComponent.IsSingleWorkAreaObject = empty_func

FloorSlabTunnel.tunnel_cost = 10000
FloorSlabTunnel.pf_additional_cost_per_elem = 50000

function SavegameFixups.LH_RemoveIsSingleWorkAreaObject()
    MapForEach(true, "ProductionDeviceComponent", function(building)
        building:InitWorkAreas()
    end)
end

function WorkAreaObject:GetChosenWorkArea()
    -- this does not use ForEachWorkArea to reduce allocations
    if self.command_center then
        return self.command_center
    end
    if self.assigned_unit then
        return self.assigned_unit:GetChosenWorkArea()
    end
    for _, area in ipairs(self.work_areas) do
        if area:IsForcefullyAdded(self) then
            return area
        end
    end
    return self.player
end

function ProductionDeviceComponent:GetDeviceAreaWorkers()
    if self:IsSingleWorkAreaObject() then
        local area = self:GetChosenWorkArea()
        return area and area.area_workers
    end
    local workers = {}
    for _, area in pairs(self.command_centers) do
        for _, worker in pairs(area.area_workers) do
            table.insert_unique(workers, worker)
        end
    end
    return workers
end

function ProductionActivityObject:CanUnitPerformActivity(unit, skill_level)
    local building = self.building
    if not building or not building:CanUnitProduce(unit) then
        return false
    end
    local recipe_def = self.recipe_id and Recipes[self.recipe_id] or self.recipe_def
    if not recipe_def then
        return false
    end
    if building:IsSingleWorkAreaObject() and unit.command_center ~= building:GetChosenWorkArea() then
        return false
    end
    return self:CanUnitBringRequiredResources(unit, building, recipe_def)
end

local GetSupplyRequestList_WorkArea = WorkArea.GetSupplyRequestList
function WorkArea:GetSupplyRequestList(res, amount, unit, demand_bld, ignore_flags, test_only, first_supply_bld, max_dist,
                                       exclude_demand_blds, multiple_res, filter_func, ...)
    local list, found_amount
    if unit and demand_bld and not filter_func then
        local same_level_filter = function(res, req_bld) return demand_bld:GetPos():z() == req_bld:GetPos():z() end
        list, found_amount = GetSupplyRequestList_WorkArea(self, res, amount, unit, demand_bld, ignore_flags, test_only, first_supply_bld, max_dist,
                exclude_demand_blds, multiple_res, same_level_filter, ...)
        if list and found_amount and found_amount >= amount then
            return list, found_amount
        end
        local same_outside_filter = function(res, req_bld) return unit:IsOutside() == req_bld:IsOutside() end
        list, found_amount = GetSupplyRequestList_WorkArea(self, res, amount, unit, demand_bld, ignore_flags, test_only, first_supply_bld, max_dist,
                exclude_demand_blds, multiple_res, same_outside_filter, ...)
        if list and found_amount and found_amount >= amount then
            return list, found_amount
        end
    end
    return GetSupplyRequestList_WorkArea(self, res, amount, unit, demand_bld, ignore_flags, test_only, first_supply_bld, max_dist,
            exclude_demand_blds, multiple_res, filter_func, ...)
end

function IsSameFloor(unit, obj)
    local room = obj.room
    local my_room = unit.room
    return IsValid(room) and IsValid(my_room) and room:GetPos():z() == my_room:GetPos():z()
            or obj:HasMember("IsOutside") and unit:IsOutside() and obj:IsOutside()
end

local IsInHomeRoom_Human = Human.IsInHomeRoom
function Human:IsInHomeRoom(obj)
    return IsInHomeRoom_Human(self, obj) or IsSameFloor(self, obj)
end

local function ChangeOpacity(survivor, level, hide_walls)
    local floor = hide_walls and GetFloorAt(survivor)
    if floor and floor.container and GetMaxRoomVisibilityLevelForObj(floor.container, 0) > level then
        survivor:SetOpacity(20)
    else
        survivor:SetOpacity(100)
    end
end

local function ChangeSurvivorsOpacity(level, overlay)
    local hide_walls = overlay and overlay == "show" or not overlay and GetOverlay("RoomsOverlay")
    for _, survivor in ipairs(AllSurvivors) do
        ChangeOpacity(survivor, level, hide_walls)
    end
end

OnMsg.OnRoomVisibilityLevelChanged = ChangeSurvivorsOpacity

local SetRoomsOverlayEnabled_orig = SetRoomsOverlayEnabled
function SetRoomsOverlayEnabled(s, ...)
    local result = SetRoomsOverlayEnabled_orig(s, ...)
    ChangeSurvivorsOpacity(RoomVisibilityLevel, s and "show" or "hide")
    return result
end

function HumanAutoResolve:UpdateRoom(...)
    ChangeOpacity(self, RoomVisibilityLevel, GetOverlay("RoomsOverlay"))
end