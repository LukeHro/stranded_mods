local function TorchCreateHeatSource(torch)
    local index = torch:GetSpotBeginIndex("Light")
    local heatSrc = PlaceObjectAt(g_Classes.HeatSource, nil, torch:GetSpotPos(index))
    heatSrc:MakeSync()
    heatSrc:SetProperty("PowerTemperature", torch.PowerTemperature)
    heatSrc:SetProperty("RadiationTemperature", torch.RadiationTemperature)
    heatSrc:SetProperty("RadiationRange", torch.RadiationRange)
    torch:Attach(heatSrc, index)
    torch.heat_source = heatSrc
    return true
end

function OnMsg.ClassesGenerate()
    GroundTorch_Soil.CreateHeatSource = TorchCreateHeatSource
    GroundTorch_Fat.CreateHeatSource = TorchCreateHeatSource
    Torch_Metal.CreateHeatSource = TorchCreateHeatSource
    Torch_Wood.CreateHeatSource = TorchCreateHeatSource
end