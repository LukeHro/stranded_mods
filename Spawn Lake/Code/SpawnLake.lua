local function MoveObjToDeepest2D(obj)
    local step = const.HeightTileSize / 2
    local x0, y0, z0 = obj:GetVisualPosXYZ()
    local GetHeight = terrain.GetHeight
    local bbox = GetPlayBox()
    local Point2DInside = bbox.Point2DInside
    local x, y = x0, y0
    local zt = GetHeight(x, y)
    local steps = 0
    while true do
        local xt, yt
        for dy = -step,step,step do
            local yi = y + dy
            for dx = -step,step,step do
                local xi = x + dx
                local zi = Point2DInside(bbox, xi, yi) and GetHeight(xi, yi) or zt
                if zi < zt then
                    xt, yt, zt = xi, yi, zi
                end
            end
        end
        if not xt then
            break
        end
        x, y = xt, yt
        steps = steps + 1
    end
    if steps > 0 then
        obj:SetPos(x, y, z0)
        --print("Deepest location found in", steps, "steps")
    end
end

local function MoveWaterfillMarkersToDeepest2D(objs)
    for _, obj in ipairs(objs) do
        if IsKindOf(obj, "TerrainWaterObject") then
            MoveObjToDeepest2D(obj)
        end
    end
end

local function SpawnWaterAtStartupLocation()
    if table.find(ModsLoaded, "id", "LH_Water") then
        local nearestWater = FindNearestWater(GameStartPos)
        WaterMask = false --in order to be recomputed after placing new lake
        if IsCloser2D(GameStartPos, nearestWater, 100 * guim) then
            print("[SpawnLake] Water already exists near starting location on map", GetMapName())
            return
        end
    end
    TerrainRestoreSuspended = true
    local poiType = Region.id == "Desertum" and "Env_Oasis" or Region.id == "Sobrius" and "Env_Lakes" or "Env_Lacus" --Nebula?
    local prefabs = {}
    for _, prefab in ipairs(PrefabMarkers) do
        if(prefab.poi_type == poiType) then
            prefabs[#prefabs+1] = PrefabComposeName(prefab)
        end
    end
    local prefabName = table.rand(prefabs)
    local min_radius = PrefabMarkers[prefabName].min_radius * const.TypeTileSize
    local max_radius = PrefabMarkers[prefabName].max_radius * const.TypeTileSize
    local marker = MapFindNearest(GameStartPos, GameStartPos, 100 * guim + max_radius, "FallingDebrisMarker", function(obj)
        return obj.MaxPrefabRadius >= max_radius and not IsCloser2D(obj:GetPos(), GameStartPos, 20 * guim + max_radius)
    end)
    marker = marker or MapFindNearest(GameStartPos, GameStartPos, 100 * guim + max_radius, "FallingDebrisMarker", function(obj)
        return obj.MaxPrefabRadius >= min_radius and not IsCloser2D(obj:GetPos(), GameStartPos, 20 * guim + max_radius)
    end)
    marker = marker or MapFindNearest(GameStartPos, GameStartPos, 100 * guim + max_radius, "FallingDebrisMarker", function(obj)
        return not IsCloser2D(obj:GetPos(), GameStartPos, 20 * guim + max_radius)
    end)
    if not marker then
        print("[SpawnLake] Failed to find location for starting lake on map", GetMapName())
        return
    end
    local err, objects, pos, _, _, inv_bbox = marker:PlacePrefab(nil, { name = prefabName })
    if err then
        print("[SpawnLake] Failed to place starting lake on map", GetMapName(), err)
        return
    end
    MoveWaterfillMarkersToDeepest2D(objects)
    print("[SpawnLake] Placed stating lake", prefabName, "on map", GetMapName(), "at", marker:GetPos(),
            "radius:", max_radius, "/", min_radius, "@", marker.MaxPrefabRadius)
    marker:ReserveLocation(pos, max_radius)
    MapForEach(pos, max_radius, "attached", false, "FallingDebrisTarget", function(obj, excludes)
        if not table.find(excludes, obj) then
            obj:OnFallingDebrisHit()
        end
    end, objects)
    MapForEach(pos, min_radius, "Rock", function(obj)
        DoneObject(obj)
    end)
    ApplyAllWaterObjects(inv_bbox, true)
    TerrainRestoreSuspended = false
end

OnMsg.StartupPosFound = SpawnWaterAtStartupLocation

CheatSpawnWater = SpawnWaterAtStartupLocation