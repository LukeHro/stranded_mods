DefineClass.WeaponsStorageComponent = {
    __parents = { "VisualStorageBase", "BuildingComponent" },
}

function WeaponsStorageComponent:UpdateStackVisual(stack_idx, ...)
    local result = VisualStorageBase.UpdateStackVisual(self, stack_idx, ...)
    if stack_idx > self.visual_stack_count then return end
    local info = self.res_stacks_info[stack_idx]
    local amount = info and info.amount or 0
    if amount <= 0 then
        return result
    end
    local stacks = self:GetVisualStacks()
    local stack = stacks[stack_idx]
    if not stack then
        return result
    end
    stack:SetAngle(0)
    stack:SetScale(100)
    PlayFX("ApplyBodyPart", "start", stack, info.fx_color and info.fx_color[1] or "red")
    return result
end