return {
PlaceObj('ModItemBuildMenuCategory', {
	BuildMenuCategory = "Storages",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/weapons_racks.dds",
	BuildMenuPos = 10,
	SortKey = 10,
	display_name = T(250639028723, --[[ModItemBuildMenuCategory sub_WeaponsRacks display_name]] "Weapon racks"),
	id = "sub_WeaponsRacks",
	menu_description = T(249644839949, --[[ModItemBuildMenuCategory sub_WeaponsRacks menu_description]] "Store and display your weapons collection."),
}),
PlaceObj('ModItemBuildingCompositeDef', {
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	BuildMenuCategory = "Storages",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/wall_shelf_metal.dds",
	BuildMenuPos = 5,
	CustomMaterial = "Metal",
	EntityHeight = 610,
	Health = 180000,
	MaxHealth = 180000,
	ResourceDismantlingComponent = true,
	SkirtSize = 0,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	accepted_res = {
		"FoodRaw",
		"FoodProcessed",
		"FabricsAllPlusRaw",
		"ElectronicComponents",
		"CraftingResourcesBulk",
		"Apparel",
		"Weapons",
		"Tools",
		"Medicine",
		"Other",
	},
	attached_to_wall = true,
	attack_attraction = 40,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 3000,
	}),
	description = T(873109293339, --[[ModItemBuildingCompositeDef MetalWallShelf description]] "Provides storage space for resources. Cannot fit large construction resources."),
	display_name = T(620466433798, --[[ModItemBuildingCompositeDef MetalWallShelf display_name]] "Wall shelf"),
	display_name_pl = T(777087943348, --[[ModItemBuildingCompositeDef MetalWallShelf display_name_pl]] "Wall shelf"),
	display_name_short = T(670923909482, --[[ModItemBuildingCompositeDef MetalWallShelf display_name_short]] "Metal wall shelves"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "MetalWallShelf",
	group = "Storages",
	id = "MetalWallShelf",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(0, -600, 1050, 600, 600, 1750),
	menu_display_name = T(859928542797, --[[ModItemBuildingCompositeDef MetalWallShelf menu_display_name]] "Wall shelf"),
	object_class = "Building",
	placement_spots = {
		"Resourceleft",
		"Resourceright",
	},
	stack_count = 2,
	unload_anim_hands = "standing_DropDown_Hands",
	update_interval = 20000,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	BuildMenuCategory = "sub_WeaponsRacks",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/spears_rack.dds",
	BuildMenuPos = 10,
	CustomMaterial = "Metal",
	EntityHeight = 610,
	Health = 180000,
	LockState = "hidden",
	MaxHealth = 180000,
	ResourceDismantlingComponent = true,
	SkirtSize = 0,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	accepted_res = {
		"Melee_Spear",
		"Melee_LaserPike",
	},
	attack_attraction = 40,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 5000,
	}),
	description = T(550308551969, --[[ModItemBuildingCompositeDef SpearsRack description]] "Stores and display spears."),
	display_name = T(395661018486, --[[ModItemBuildingCompositeDef SpearsRack display_name]] "Spears rack"),
	display_name_pl = T(280476781978, --[[ModItemBuildingCompositeDef SpearsRack display_name_pl]] "Spears rack"),
	display_name_short = T(142464232766, --[[ModItemBuildingCompositeDef SpearsRack display_name_short]] "Spears racks"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "SpearsRack",
	group = "Storages",
	id = "SpearsRack",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	menu_display_name = T(271972520601, --[[ModItemBuildingCompositeDef SpearsRack menu_display_name]] "Spears"),
	object_class = "Building",
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
	stack_count = 6,
	unload_anim_hands = "standing_DropDown_Hands",
	unlocked_by_tech = "WeaponRacks",
	update_interval = 20000,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	BuildMenuCategory = "sub_WeaponsRacks",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/rifles_rack.dds",
	BuildMenuPos = 60,
	CustomMaterial = "Metal",
	EntityHeight = 610,
	Health = 180000,
	LockState = "hidden",
	MaxHealth = 180000,
	ResourceDismantlingComponent = true,
	SkirtSize = 0,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	accepted_res = {
		"Ranged_PulseRifle",
		"Ranged_PulseRifle_Improved",
		"Ranged_RailgunSniper",
		"Ranged_RailgunSniper_Improved",
	},
	attack_attraction = 40,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 5000,
	}),
	description = T(285493031352, --[[ModItemBuildingCompositeDef RiflesRack description]] "Stores and display rifles."),
	display_name = T(471607584101, --[[ModItemBuildingCompositeDef RiflesRack display_name]] "Rifles rack"),
	display_name_pl = T(217643673522, --[[ModItemBuildingCompositeDef RiflesRack display_name_pl]] "Rifles rack"),
	display_name_short = T(114421287144, --[[ModItemBuildingCompositeDef RiflesRack display_name_short]] "Rifles racks"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "RiflesRack",
	group = "Storages",
	id = "RiflesRack",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	menu_display_name = T(332886133618, --[[ModItemBuildingCompositeDef RiflesRack menu_display_name]] "Rifles"),
	object_class = "Building",
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
	stack_count = 6,
	unload_anim_hands = "standing_DropDown_Hands",
	unlocked_by_tech = "WeaponRacks",
	update_interval = 20000,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	BuildMenuCategory = "sub_WeaponsRacks",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/crossbows_rack.dds",
	BuildMenuPos = 40,
	CustomMaterial = "Metal",
	EntityHeight = 610,
	Health = 180000,
	LockState = "hidden",
	MaxHealth = 180000,
	ResourceDismantlingComponent = true,
	SkirtSize = 0,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	accepted_res = {
		"Ranged_Crossbow",
		"Ranged_Pacifier",
		"Ranged_CarbonCrossbow",
	},
	attack_attraction = 40,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 5000,
	}),
	description = T(139126839186, --[[ModItemBuildingCompositeDef CrossbowsRack description]] "Stores and display rifles."),
	display_name = T(171823943793, --[[ModItemBuildingCompositeDef CrossbowsRack display_name]] "Crossbows rack"),
	display_name_pl = T(971934196513, --[[ModItemBuildingCompositeDef CrossbowsRack display_name_pl]] "Crossbows rack"),
	display_name_short = T(246960494815, --[[ModItemBuildingCompositeDef CrossbowsRack display_name_short]] "Crossbows racks"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "CrossbowsRack",
	group = "Storages",
	id = "CrossbowsRack",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	menu_display_name = T(509349539957, --[[ModItemBuildingCompositeDef CrossbowsRack menu_display_name]] "Crossbows"),
	object_class = "Building",
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
	stack_count = 6,
	unload_anim_hands = "standing_DropDown_Hands",
	unlocked_by_tech = "WeaponRacks",
	update_interval = 20000,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	BuildMenuCategory = "sub_WeaponsRacks",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/bows_rack.dds",
	BuildMenuPos = 30,
	CustomMaterial = "Metal",
	EntityHeight = 610,
	Health = 180000,
	LockState = "hidden",
	MaxHealth = 180000,
	ResourceDismantlingComponent = true,
	SkirtSize = 0,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	accepted_res = {
		"Ranged_ShortBow",
	},
	attack_attraction = 40,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 3000,
	}),
	description = T(975282531386, --[[ModItemBuildingCompositeDef BowsRack description]] "Stores and display rifles."),
	display_name = T(434658342707, --[[ModItemBuildingCompositeDef BowsRack display_name]] "Bows rack"),
	display_name_pl = T(158804256164, --[[ModItemBuildingCompositeDef BowsRack display_name_pl]] "Bows rack"),
	display_name_short = T(733522632723, --[[ModItemBuildingCompositeDef BowsRack display_name_short]] "Bows racks"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "BowsRack",
	group = "Storages",
	id = "BowsRack",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	menu_display_name = T(709492292113, --[[ModItemBuildingCompositeDef BowsRack menu_display_name]] "Bows"),
	object_class = "Building",
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
	stack_count = 6,
	unload_anim_hands = "standing_DropDown_Hands",
	unlocked_by_tech = "WeaponRacks",
	update_interval = 20000,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	BuildMenuCategory = "sub_WeaponsRacks",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/swords_rack.dds",
	BuildMenuPos = 20,
	CustomMaterial = "Metal",
	EntityHeight = 610,
	Health = 180000,
	LockState = "hidden",
	MaxHealth = 180000,
	ResourceDismantlingComponent = true,
	SkirtSize = 0,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	accepted_res = {
		"Melee_LaserSword",
	},
	attack_attraction = 40,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 3000,
	}),
	description = T(377007832307, --[[ModItemBuildingCompositeDef SwordsRack description]] "Stores and display rifles."),
	display_name = T(895574034012, --[[ModItemBuildingCompositeDef SwordsRack display_name]] "Swords rack"),
	display_name_pl = T(738789415766, --[[ModItemBuildingCompositeDef SwordsRack display_name_pl]] "Swords rack"),
	display_name_short = T(224639686857, --[[ModItemBuildingCompositeDef SwordsRack display_name_short]] "Swords racks"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "SwordsRack",
	group = "Storages",
	id = "SwordsRack",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	menu_display_name = T(404220492537, --[[ModItemBuildingCompositeDef SwordsRack menu_display_name]] "Swords"),
	object_class = "Building",
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
	stack_count = 6,
	unload_anim_hands = "standing_DropDown_Hands",
	unlocked_by_tech = "WeaponRacks",
	update_interval = 20000,
}),
PlaceObj('ModItemBuildingCompositeDef', {
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	BuildMenuCategory = "sub_WeaponsRacks",
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/pistols_rack.dds",
	BuildMenuPos = 50,
	CustomMaterial = "Metal",
	EntityHeight = 610,
	Health = 180000,
	LockState = "hidden",
	MaxHealth = 180000,
	ResourceDismantlingComponent = true,
	SkirtSize = 0,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	accepted_res = {
		"Ranged_LaserBlaster",
		"Ranged_LaserBlaster_Improved",
	},
	attack_attraction = 40,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 3000,
	}),
	description = T(559673521215, --[[ModItemBuildingCompositeDef PistolsRack description]] "Stores and display rifles."),
	display_name = T(806580134564, --[[ModItemBuildingCompositeDef PistolsRack display_name]] "Pistols rack"),
	display_name_pl = T(680925752849, --[[ModItemBuildingCompositeDef PistolsRack display_name_pl]] "Pistols rack"),
	display_name_short = T(223586213739, --[[ModItemBuildingCompositeDef PistolsRack display_name_short]] "Pistols racks"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "PistolsRack",
	group = "Storages",
	id = "PistolsRack",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	menu_display_name = T(319532073213, --[[ModItemBuildingCompositeDef PistolsRack menu_display_name]] "Pistols"),
	object_class = "Building",
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
	stack_count = 6,
	unload_anim_hands = "standing_DropDown_Hands",
	unlocked_by_tech = "WeaponRacks",
	update_interval = 20000,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Melee_LaserPike",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"LaserPike",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Melee_Spear",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Spear",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_PulseRifle",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Pulse_Rifle",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_PulseRifle_Improved",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Pulse_Rifle_2",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_RailgunSniper",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"RailgunSniper",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_RailgunSniper_Improved",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"RailgunSniper_2",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_Crossbow",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Crossbow",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_Pacifier",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Crossbow",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_CarbonCrossbow",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Crossbow_Carbon",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_ShortBow",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"ShortBow",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Melee_LaserSword",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"EnergySword",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_LaserBlaster",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Blaster_Pistol",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Resource",
	'TargetId', "Ranged_LaserBlaster_Improved",
	'TargetProp', "resource_entities",
	'TargetValue', {
		"Blaster_Pistol_2",
	},
}),
PlaceObj('ModItemCode', {
	'name', "WeaponsRackComponent",
	'CodeFileName', "Code/WeaponsRackComponent.lua",
}),
PlaceObj('ModItemEntity', {
	'name', "BowsRack",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "BowsRack",
}),
PlaceObj('ModItemEntity', {
	'name', "CrossbowsRack",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "CrossbowsRack",
}),
PlaceObj('ModItemEntity', {
	'name', "MetalWallShelf",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "MetalWallShelf",
}),
PlaceObj('ModItemEntity', {
	'name', "PistolsRack",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "PistolsRack",
}),
PlaceObj('ModItemEntity', {
	'name', "RiflesRack",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "RiflesRack",
}),
PlaceObj('ModItemEntity', {
	'name', "SpearsRack",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "SpearsRack",
}),
PlaceObj('ModItemEntity', {
	'name', "SwordsRack",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "SwordsRack",
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Storages",
	},
	Description = T(654666196741, --[[ModItemTech WeaponRacks Description]] "Display your weapon collection on custom racks for each type.\n\n<style TechSubtitleBlue>Unlocks</style>\n   <color TextEmphasis>Weapon racks</color>: 5<image 'UI/Icons/Resources/res_metal_ingot' 1100>"),
	DisplayName = T(945812855936, --[[ModItemTech WeaponRacks DisplayName]] "Weapon racks"),
	Icon = "Mod/LH_Storages/UI/Icons/Research/weapons_racks.dds",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "Metal",
		}),
		PlaceObj('CheckTech', {
			Tech = "BasicArchitecture",
		}),
	},
	ResearchPoints = 24000,
	SortKey = 6,
	group = "Construction",
	id = "WeaponRacks",
	money_value = 20000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Storages",
		LockState = "hidden",
		PresetId = "SpearsRack",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Storages",
		LockState = "hidden",
		PresetId = "SwordsRack",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Storages",
		LockState = "hidden",
		PresetId = "BowsRack",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Storages",
		LockState = "hidden",
		PresetId = "CrossbowsRack",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Storages",
		LockState = "hidden",
		PresetId = "PistolsRack",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Storages",
		LockState = "hidden",
		PresetId = "RiflesRack",
	}),
}),
}
