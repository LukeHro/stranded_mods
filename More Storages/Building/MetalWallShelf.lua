UndefineClass('MetalWallShelf')
DefineClass.MetalWallShelf = {
	__parents = { "Building", "ResourceDismantlingComponent", "StorageDepotComponent", "VisualStorageComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "Storages",
	display_name = T(620466433798, --[[ModItemBuildingCompositeDef MetalWallShelf display_name]] "Wall shelf"),
	description = T(873109293339, --[[ModItemBuildingCompositeDef MetalWallShelf description]] "Provides storage space for resources. Cannot fit large construction resources."),
	menu_display_name = T(859928542797, --[[ModItemBuildingCompositeDef MetalWallShelf menu_display_name]] "Wall shelf"),
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/wall_shelf_metal.dds",
	BuildMenuPos = 5,
	display_name_pl = T(777087943348, --[[ModItemBuildingCompositeDef MetalWallShelf display_name_pl]] "Wall shelf"),
	display_name_short = T(670923909482, --[[ModItemBuildingCompositeDef MetalWallShelf display_name_short]] "Metal wall shelves"),
	entity = "MetalWallShelf",
	labels = {
		"BerserkTargets",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 3000,
	}),
	Health = 180000,
	MaxHealth = 180000,
	attached_to_wall = true,
	lock_block_box = box(0, -600, 1050, 600, 600, 1750),
	SkirtSize = 0,
	EntityHeight = 610,
	CustomMaterial = "Metal",
	attack_attraction = 40,
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	ResourceDismantlingComponent = true,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	stack_count = 2,
	accepted_res = {
		"FoodRaw",
		"FoodProcessed",
		"FabricsAllPlusRaw",
		"ElectronicComponents",
		"CraftingResourcesBulk",
		"Apparel",
		"Weapons",
		"Tools",
		"Medicine",
		"Other",
	},
	placement_spots = {
		"Resourceleft",
		"Resourceright",
	},
}

