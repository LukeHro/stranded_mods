UndefineClass('SpearsRack')
DefineClass.SpearsRack = {
	__parents = { "Building", "ResourceDismantlingComponent", "StorageDepotComponent", "VisualStorageComponent", "WeaponsStorageComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	LockState = "hidden",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "sub_WeaponsRacks",
	display_name = T(395661018486, --[[ModItemBuildingCompositeDef SpearsRack display_name]] "Spears rack"),
	description = T(550308551969, --[[ModItemBuildingCompositeDef SpearsRack description]] "Stores and display spears."),
	menu_display_name = T(271972520601, --[[ModItemBuildingCompositeDef SpearsRack menu_display_name]] "Spears"),
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/spears_rack.dds",
	BuildMenuPos = 10,
	display_name_pl = T(280476781978, --[[ModItemBuildingCompositeDef SpearsRack display_name_pl]] "Spears rack"),
	display_name_short = T(142464232766, --[[ModItemBuildingCompositeDef SpearsRack display_name_short]] "Spears racks"),
	entity = "SpearsRack",
	labels = {
		"BerserkTargets",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 5000,
	}),
	Health = 180000,
	MaxHealth = 180000,
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	SkirtSize = 0,
	EntityHeight = 610,
	CustomMaterial = "Metal",
	attack_attraction = 40,
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	unlocked_by_tech = "WeaponRacks",
	ResourceDismantlingComponent = true,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	stack_count = 6,
	accepted_res = {
		"Melee_Spear",
		"Melee_LaserPike",
	},
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
}

