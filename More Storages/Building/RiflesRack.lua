UndefineClass('RiflesRack')
DefineClass.RiflesRack = {
	__parents = { "Building", "ResourceDismantlingComponent", "StorageDepotComponent", "VisualStorageComponent", "WeaponsStorageComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	LockState = "hidden",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "sub_WeaponsRacks",
	display_name = T(471607584101, --[[ModItemBuildingCompositeDef RiflesRack display_name]] "Rifles rack"),
	description = T(285493031352, --[[ModItemBuildingCompositeDef RiflesRack description]] "Stores and display rifles."),
	menu_display_name = T(332886133618, --[[ModItemBuildingCompositeDef RiflesRack menu_display_name]] "Rifles"),
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/rifles_rack.dds",
	BuildMenuPos = 60,
	display_name_pl = T(217643673522, --[[ModItemBuildingCompositeDef RiflesRack display_name_pl]] "Rifles rack"),
	display_name_short = T(114421287144, --[[ModItemBuildingCompositeDef RiflesRack display_name_short]] "Rifles racks"),
	entity = "RiflesRack",
	labels = {
		"BerserkTargets",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 5000,
	}),
	Health = 180000,
	MaxHealth = 180000,
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	SkirtSize = 0,
	EntityHeight = 610,
	CustomMaterial = "Metal",
	attack_attraction = 40,
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	unlocked_by_tech = "WeaponRacks",
	ResourceDismantlingComponent = true,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	stack_count = 6,
	accepted_res = {
		"Ranged_PulseRifle",
		"Ranged_PulseRifle_Improved",
		"Ranged_RailgunSniper",
		"Ranged_RailgunSniper_Improved",
	},
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
}

