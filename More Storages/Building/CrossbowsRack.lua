UndefineClass('CrossbowsRack')
DefineClass.CrossbowsRack = {
	__parents = { "Building", "ResourceDismantlingComponent", "StorageDepotComponent", "VisualStorageComponent", "WeaponsStorageComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	LockState = "hidden",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "sub_WeaponsRacks",
	display_name = T(171823943793, --[[ModItemBuildingCompositeDef CrossbowsRack display_name]] "Crossbows rack"),
	description = T(139126839186, --[[ModItemBuildingCompositeDef CrossbowsRack description]] "Stores and display rifles."),
	menu_display_name = T(509349539957, --[[ModItemBuildingCompositeDef CrossbowsRack menu_display_name]] "Crossbows"),
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/crossbows_rack.dds",
	BuildMenuPos = 40,
	display_name_pl = T(971934196513, --[[ModItemBuildingCompositeDef CrossbowsRack display_name_pl]] "Crossbows rack"),
	display_name_short = T(246960494815, --[[ModItemBuildingCompositeDef CrossbowsRack display_name_short]] "Crossbows racks"),
	entity = "CrossbowsRack",
	labels = {
		"BerserkTargets",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 5000,
	}),
	Health = 180000,
	MaxHealth = 180000,
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	SkirtSize = 0,
	EntityHeight = 610,
	CustomMaterial = "Metal",
	attack_attraction = 40,
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	unlocked_by_tech = "WeaponRacks",
	ResourceDismantlingComponent = true,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	stack_count = 6,
	accepted_res = {
		"Ranged_Crossbow",
		"Ranged_Pacifier",
		"Ranged_CarbonCrossbow",
	},
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
}

