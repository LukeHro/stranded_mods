UndefineClass('PistolsRack')
DefineClass.PistolsRack = {
	__parents = { "Building", "ResourceDismantlingComponent", "StorageDepotComponent", "VisualStorageComponent", "WeaponsStorageComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	LockState = "hidden",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "sub_WeaponsRacks",
	display_name = T(806580134564, --[[ModItemBuildingCompositeDef PistolsRack display_name]] "Pistols rack"),
	description = T(559673521215, --[[ModItemBuildingCompositeDef PistolsRack description]] "Stores and display rifles."),
	menu_display_name = T(319532073213, --[[ModItemBuildingCompositeDef PistolsRack menu_display_name]] "Pistols"),
	BuildMenuIcon = "Mod/LH_Storages/UI/Icons/Build Menu/pistols_rack.dds",
	BuildMenuPos = 50,
	display_name_pl = T(680925752849, --[[ModItemBuildingCompositeDef PistolsRack display_name_pl]] "Pistols rack"),
	display_name_short = T(223586213739, --[[ModItemBuildingCompositeDef PistolsRack display_name_short]] "Pistols racks"),
	entity = "PistolsRack",
	labels = {
		"BerserkTargets",
	},
	update_interval = 20000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 3000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 3000,
	}),
	Health = 180000,
	MaxHealth = 180000,
	lock_block_box = box(-300, -300, 0, 300, 300, 1050),
	SkirtSize = 0,
	EntityHeight = 610,
	CustomMaterial = "Metal",
	attack_attraction = 40,
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	unlocked_by_tech = "WeaponRacks",
	ResourceDismantlingComponent = true,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	WeaponsStorageComponent = true,
	stack_count = 6,
	accepted_res = {
		"Ranged_LaserBlaster",
		"Ranged_LaserBlaster_Improved",
	},
	placement_spots = {
		"Place1",
		"Place2",
		"Place3",
		"Place4",
		"Place5",
		"Place6",
	},
	res_entity_for_stack = true,
}

