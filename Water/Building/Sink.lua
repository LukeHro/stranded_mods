UndefineClass('Sink')
DefineClass.Sink = {
	__parents = { "SinkBuilding", "WaterSourceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "SinkBuilding",
	LockState = "hidden",
	unload_anim_handsclose = "standing_DropDown_HandsClose",
	load_anim_handsclose = "standing_PickUp_HandsClose",
	BuildMenuCategory = "Water",
	display_name = T(862435135289, --[[ModItemBuildingCompositeDef Sink display_name]] "Sink"),
	description = T(148037866099, --[[ModItemBuildingCompositeDef Sink description]] "Indoor running water."),
	BuildMenuIcon = "Mod/LH_Water/UI/Icons/Build Menu/sink.dds",
	BuildMenuPos = 110,
	display_name_pl = T(117540551382, --[[ModItemBuildingCompositeDef Sink display_name_pl]] "Sinks"),
	display_name_short = T(225958311116, --[[ModItemBuildingCompositeDef Sink display_name_short]] "Sink"),
	entity = "Sink",
	labels = {
		"Storages",
	},
	update_interval = 4000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
		PlaceObj('ConstructionCheckRoomPlacement', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	Health = 60000,
	MaxHealth = 60000,
	attached_to_wall = true,
	RoomPlacement = "indoors",
	CanPlaceInShelter = false,
	lock_block_box = box(0, -300, 350, 600, 300, 1050),
	SkirtSize = 0,
	EntityHeight = 950,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	WaterSourceComponent = true,
	PowerTemperature = 30000,
	RadiationTemperature = 30000,
	RadiationRange = 500,
	ownership_class = "Sink",
	ChangeOwnerIcon = "Mod/LH_Toilets/UI/Icons/Infopanels/toilet_change_owner.dds",
	Filter = function (obj)
		return not obj:HasUnitTag("Android")
	end,
}

