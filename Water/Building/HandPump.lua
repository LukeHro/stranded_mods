UndefineClass('HandPump')
DefineClass.HandPump = {
	__parents = { "HandPumpBuilding", "WaterSourceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "HandPumpBuilding",
	unload_anim_handsclose = "standing_DropDown_HandsClose",
	load_anim_handsclose = "standing_PickUp_HandsClose",
	BuildMenuCategory = "Water",
	display_name = T(283726947262, --[[ModItemBuildingCompositeDef HandPump display_name]] "Hand pump"),
	description = T(562733203895, --[[ModItemBuildingCompositeDef HandPump description]] "Pumps phreatic water to the surface for immediate use."),
	BuildMenuIcon = "Mod/LH_Water/UI/Icons/Build Menu/hand_pump.dds",
	BuildMenuPos = 100,
	display_name_pl = T(946968045947, --[[ModItemBuildingCompositeDef HandPump display_name_pl]] "Hand pumps"),
	display_name_short = T(544135807901, --[[ModItemBuildingCompositeDef HandPump display_name_short]] "Hand pump"),
	entity = "HandPump",
	labels = {
		"Storages",
	},
	construction_cost = PlaceObj('ConstructionCost', {
		ScrapMetal = 30000,
	}),
	construction_points = 30000,
	custom_constr_rules = {
		PlaceObj('HandPumpConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 10000,
	}),
	Health = 60000,
	MaxHealth = 60000,
	can_be_placed_on_floor = false,
	RoomPlacement = "outdoors",
	lock_block_box = box(-600, -300, 0, 600, 300, 1400),
	SkirtSize = 0,
	EntityHeight = 1286,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	WaterSourceComponent = true,
	can_change_ownership = false,
	ownership_class = "Chair",
	IsPowerConsumer = true,
	PowerConsumption = 20000000,
}

