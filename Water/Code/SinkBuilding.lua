DefineClass.WaterSourceComponent = {
    __parents = { "BuildingComponent" },
}

function WaterSourceComponent:Init()
    self.res_amounts = self.res_amounts or ResList:new()
end

function OnMsg.ConstructionComplete(_, obj)
    if IsKindOf(obj, "WaterSourceComponent") then
        obj:OnObjUpdate()
    end
end

function WaterSourceComponent:OnObjUpdate(...)
    if self:IsDeconstructing() or not IsConstructionFinished(self) then
        return
    end
    if self.res_amounts and self.res_amounts["PotableWater"] then
        local amountPotable = 500 * const.ResourceScale - self.res_amounts["PotableWater"]
        if amountPotable > 0 then
            self:AddRes("PotableWater", amountPotable)
        end
        self:UpdateResPolicies()
    end
    if self.res_amounts and self.res_amounts["ColdWater"] then
        local amountCold = 500 * const.ResourceScale - self.res_amounts["ColdWater"]
        if amountCold > 0 then
            self:AddRes("ColdWater", amountCold)
        end
        self:UpdateResPolicies()
    end
end

function WaterSourceComponent:SetupResPolicies(processed_list)
    self:SetResourcePolicy("PotableWater", 0, 1000 * const.ResourceScale, "allow_distribution")
    processed_list["PotableWater"] = processed_list["PotableWater"] or 0
    self:SetResourcePolicy("ColdWater", 0, 1000 * const.ResourceScale, "allow_distribution")
    processed_list["ColdWater"] = processed_list["ColdWater"] or 0
end

function WaterSourceComponent:OnStartDeconstruct(...)
    Building.OnStartDeconstruct(self, ...)
    ExtendedResStorage.SubtractRes(self, "PotableWater", self.res_amounts["PotableWater"])
    ExtendedResStorage.SubtractRes(self, "ColdWater", self.res_amounts["ColdWater"])
end

DefineClass.SinkBuilding = {
    __parents = { "CompositeBuilding" },
    water_flow = false,
}

function SinkBuilding:Init()
    self.water_flow = self.water_flow or PlaceObject("SinkWater", nil, const.cofComponentAttach)
    self:Attach(self.water_flow, self:GetRandomSpot("Water"))
    self.water_flow:SetVisible(false)
end

function SinkBuilding:Done()
    if IsValid(self.water_flow) then
        DoneObject(self.water_flow)
    end
end

function SinkBuilding:SubtractRes(res, amount, worker, ...)
    if res == "PotableWater" or res == "ColdWater" then
        if worker and not IsKindOf(worker, "QuadcopterBase") then
            worker:PushDestructor(function()
                self.water_flow:SetVisible(false)
            end)
            self.water_flow:SetVisible(true)
            worker:PlayMomentTrackedAnim("standing_Build_Hammer_Idle", 1, nil, 200, 1000)
            worker:PopAndCallDestructor()
        else
            CreateGameTimeThread(function()
                if (IsValid(self)) then
                    self.water_flow:SetVisible(true)
                    Sleep(1000)
                    self.water_flow:SetVisible(false)
                end
            end)
            if worker then
                Sleep(1000)
            end
        end
    end
    return ExtendedResStorage.SubtractRes(self, res, amount, worker, ...)
end

function SavegameFixups.AddWaterSourceComponentsToStoragesLabels()
    local labels = UIPlayer.labels
    for _, building in pairs(labels.HandPump) do
        building:UpdateResPolicies()
        table.insert_unique(labels.Storages, building)
    end
    for _, building in pairs(labels.Sink) do
        building:UpdateResPolicies()
        table.insert_unique(labels.Storages, building)
    end
end

