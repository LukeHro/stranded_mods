function AddWaterToRecipe(recipe, amount, resource)
    resource = resource or "PotableWater"
    amount = amount or Recipes[recipe].OutputResources[1].amount
    table.insert_unique(Recipes[recipe].InputResources, PlaceObj('ResAmount', { 'resource', resource, 'amount', amount}))
end

function OnMsg:ModsReloaded()
    AddWaterToRecipe("AlcoholBeer")
    AddWaterToRecipe("Coffee_Grain")
    AddWaterToRecipe("Tea_Buzzshroom")
    AddWaterToRecipe("Tea_Dandelion")
    AddWaterToRecipe("Tea_Smokeleaf")
    AddWaterToRecipe("Tea_Teaballs")
    AddWaterToRecipe("SoupVegetable")
    AddWaterToRecipe("SoupMeat")
end

function CheckHygieneBuildingAccess(unit, building)
    return IsValid(building) and not unit:HasUnitTag("Android") and not building:IsVirtual()
            and (not IsKindOf(building, "OwnedObject") or building:CanBeOwnedBy(unit))
            and not unit:IsTargetUnreachable(building)
end

MapVar("WaterMask", false)
MapVar("WaterMissing", false)

function CalcWaterMask()
    local hm = terrain.GetHeightGrid()
    local wm = terrain.GetWaterGrid()
    -- convert to floating point
    hm = GridRepack(hm, "f")
    wm = GridRepack(wm, "f")
    -- make the same size
    local w, h = wm:size()
    hm = GridResample(hm, w, h)
    -- find where the water is above the terrain
    GridAddMulDiv(wm, hm, -1)
    GridMask(wm, 1, max_int)
    WaterMask = GridRepack(wm, "u", 8)
    WaterMissing = GridEquals(WaterMask, 0)
    return WaterMask
end

function FindNearestWater(pt)
    WaterMask = WaterMask or CalcWaterMask()
    if WaterMissing then
        return
    end
    local x, y = pt:xy()
    local w, h = WaterMask:size()
    local mw, mh = terrain.GetMapSize()
    x = x * w / mw
    y = y * h / mh
    local minx, miny = GridMinMaxDist(WaterMask, x, y)
    minx = (minx * mw + mw / 2) / w
    miny = (miny * mh + mh / 2) / h
    return point(minx, miny)
end

---TODO make a standalone mod for this (can also be used for milk) and also to revive the "basket" carry type.

function Human:PickDoorAnim()
    local carry_type = self:GetCarryType()
    if carry_type == "Wood" then
        return "standing_Walk_OpenDoor_HoldingWood"
    elseif carry_type == "Hands" then
        return "standing_OpenDoor_Hands"
    elseif carry_type == "HandsClose" then
        return "standing_Walk_OpenDoor_HandsClose"
    elseif carry_type == "Basket" then
        return "standing_Walk_OpenDoor_Basket"
    elseif IsValid(self.carry_body) then
        return "standing_Walk_OpenDoor_CarryBody", self.carry_body, "carriedBody_OpenDoor"
    elseif self:IsMovementImpaired() then
        return "standing_Walk_OpenDoor_Disabled"
    end
    local weapon_anim = self:GetWeaponAnim("AnimDoor")
    if weapon_anim then
        return weapon_anim
    end
    return "standing_OpenDoor_Free"
end

GetCarryType_Human = Human.GetCarryType
function Human:GetCarryType()
    local carried_res = self:GetCarriedRes()
    local preset = carried_res and Resources[carried_res]
    if preset and preset.carry_spot == "Basket" then
        return "Basket"
    end
    return GetCarryType_Human(self)
end

local AttachCarryRes_Worker = Worker.AttachCarryRes
function Worker:AttachCarryRes(res, amount, ...)
    local result = AttachCarryRes_Worker(self, res, amount, ...)
    local preset = res and Resources[res]
    if preset.carry_spot == "Basket" and preset.carry_type ~= "Basket" then
        local carry_part = self:GetPart("Carry")
        if carry_part then
            carry_part:SetAttachOffset(-80, 20, 0)
        end
    end
    return result
end

AutoResolveMethods.IsDesperatelyDoingSomething = "or"

function UnitEnergy:IsDesperatelyDoingSomething()
    return self.command == "CmdDesperateEat"
end
function UnitHealth:IsDesperatelyDoingSomething()
    return self.command == "CmdPatient"
end
function UnitMedicalBed:IsDesperatelyDoingSomething()
    return self.command == "CmdMedicalBedPatient"
end

local IsAlmostStarving_UnitEnergy = UnitEnergy.IsAlmostStarving
function UnitEnergy:IsAlmostStarving(...)
    return IsAlmostStarving_UnitEnergy(self, ...) and not self:IsDesperatelyDoingSomething()
end

local NeedsToEat_UnitEnergy = UnitEnergy.NeedsToEat
function UnitEnergy:NeedsToEat(...)
    return NeedsToEat_UnitEnergy(self, ...) and not self:IsDesperatelyDoingSomething()
end