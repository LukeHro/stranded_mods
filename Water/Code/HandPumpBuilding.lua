function OnMsg:ModsReloaded()
    PlaceObj('ConstructionRuleResult', {
        id = "HandPumpTooClose",
        infopanel_text = T("Too close to another Pump."),
        inview_text = T("Too close to others"),
    })
end

DefineClass.HandPumpConstructionRule = {
    __parents = { "ConstructionObjectSnapToVoxel" },
}

function HandPumpConstructionRule:UpdateConstructionRule(controller)
    local cursor_pos = controller.cursor:GetPos()
    for _, d in ipairs(controller.player.labels.HandPump) do
        if d ~= controller.moving_building and IsCloser2D(d, cursor_pos, 20 * guim) then
            return "HandPumpTooClose", d
        end
    end
end

DefineClass.HandPumpBuilding = {
    __parents = { "CompositeBuilding", "CommandObject" },
    pump_handle = false,
    water_flow = false,
    water_surface = false,
    interrupted = false,
}

function HandPumpBuilding:Init()
    self.pump_handle = self.pump_handle or PlaceObject("PumpHandle", nil, const.cofComponentAttach)
    self:Attach(self.pump_handle, self:GetRandomSpot("Handle"))
    self.pump_handle:SetAxis(axis_x)
    self.water_flow = self.water_flow or PlaceObject("PumpFlow", nil, const.cofComponentAttach)
    self:Attach(self.water_flow, self:GetRandomSpot("Waterflow"))
    self.water_flow:SetVisible(false)
    self.water_surface = self.water_surface or PlaceObject("PumpWater", nil, const.cofComponentAttach)
    self:Attach(self.water_surface, self:GetRandomSpot("Watersurface"))
    self.water_surface:SetVisible(false)
    self.interrupted = nil
end

function HandPumpBuilding:Done()
    self:SubtractRes("PotableWater", self.res_amounts["PotableWater"])
    self:SubtractRes("ColdWater", self.res_amounts["ColdWater"])
    self:UpdateResPolicies()
    if IsValid(self.pump_handle) then
        DoneObject(self.pump_handle)
    end
    if IsValid(self.water_flow) then
        DoneObject(self.water_flow)
    end
    if IsValid(self.water_surface) then
        DoneObject(self.water_surface)
    end
end

function HandPumpBuilding:CmdPumpWater(pumps)
    self.interrupted = false
    self:PushDestructor(function()
        self.water_flow:SetVisible(false)
        self.pump_handle:SetAngle(0)
        if self.interrupted then
            self.water_surface:SetVisible(false)
            self.interrupted = nil
        end
    end)
    for i = 2, Max(0, 3 - (pumps or 3)), -1 do
        self.pump_handle:SetAngle(1600, 650)
        self.water_flow:SetVisible(false)
        Sleep(700)
        self.pump_handle:SetAngle(0, 700)
        self.water_flow:SetVisible(true)
        Sleep(800)
        self.water_surface:SetVisible(true)
        self.water_surface:SetScale(100 - 10 * i)
        self.water_surface:SetAttachOffset(0, 0, -50 * i)
    end
    self:PopAndCallDestructor()
end

function HandPumpBuilding:RemoveWater()
    self.water_surface:SetVisible(false)
    if self.command == "CmdPumpWater" then
        self.interrupted = true
    end
end

function HandPumpBuilding:SubtractRes(res, amount, worker, ...)
    if res == "PotableWater" or res == "ColdWater" then
        if worker and not IsKindOf(worker, "QuadcopterBase") then
            self:PumpWater(worker, true)
        end
    end
    return ExtendedResStorage.SubtractRes(self, res, amount, worker, ...)
end

function HandPumpBuilding:PumpWater(unit, stop)
    unit:PushDestructor(function()
        if stop then
            self:RemoveWater()
        end
    end)
    unit:Face(self:GetSpotPos(self:GetSpotBeginIndex("Pump")))
    self:SetCommand("CmdPumpWater", 2)
    unit:PlayMomentTrackedAnim("bend_Cut_Tool", 1, nil, 200, 3000)
    unit:PopAndCallDestructor()
    return true
end