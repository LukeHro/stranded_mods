return {
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(319764683128, --[[ModItemActivityType Irrigate ActivityStatusText |gender-variants]] "Irrigating"),
	ActivityText = T(351497962756, --[[ModItemActivityType Irrigate ActivityText |gender-variants]] "Irrigate <target.DisplayName>"),
	BatchCount = 10,
	CheckSelectionBorderPassed = function (self, obj, prev_obj)
		if not prev_obj then return end
		-- do not allow batching of irrigating activities for multiple fields
		local target1 = obj:GetActivityTarget()
		local target2 = prev_obj:GetActivityTarget()
		return IsKindOf(target1, "FieldBase") or IsKindOf(target2, "FieldBase")
	end,
	Description = T(711461973020, --[[ModItemActivityType Irrigate Description]] "Irrigate a field."),
	DisplayName = T(594535618017, --[[ModItemActivityType Irrigate DisplayName]] "Irrigate"),
	EfficiencyFromManipulation = true,
	EfficiencyFromSkill = 10,
	GetDisplayedStatusText = function (self, unit, short)
		local activity_target = unit and unit.current_act_target
		local target = IsValid(activity_target) and activity_target:GetActivityTarget()
		local def = Plants[IsValid(target) and target.Crop or false]
		local status_text = GetTByGender(self.ActivityStatusText, unit)
		if def then
			if def.StatusName then
				return status_text .. T{379841959613, " <plant_name>", plant_name = def.StatusName}
			elseif def.StatusIcon then
				return status_text and status_text .. " <image " .. def.StatusIcon .. " 1000>"
			elseif def.HarvestResources then
				return ResAmountsIconList(status_text, def.HarvestResources)
			end
		end
		return status_text
	end,
	PickClosest = true,
	PriorityDisplayName = T(788839173842, --[[ModItemActivityType Irrigate PriorityDisplayName]] "Irrigating"),
	QueueStatusText = T(242827011070, --[[ModItemActivityType Irrigate QueueStatusText |gender-variants]] "Irrigate"),
	Skill = "Farming",
	SortKey = 2960,
	StatusIcon = "UI/Hud/st_plant",
	id = "Irrigate",
}),
PlaceObj('ModItemChangeProp', {
	'name', "ReduceDesertRainGrowth",
	'comment', "total modifier must be 8bit signed int",
	'TargetClass', "RegionDef",
	'TargetId', "Desertum",
	'TargetProp', "PlantsGrowthRain",
	'TargetValue', 50,
}),
PlaceObj('ModItemChangeProp', {
	'name', "IncreaseSaltuRainGrowth",
	'comment', "total modifier must be 8bit signed int",
	'TargetClass', "RegionDef",
	'TargetId', "Saltu",
	'TargetProp', "PlantsGrowthRain",
	'TargetValue', 10,
}),
PlaceObj('ModItemChangeProp', {
	'name', "AddSaltuRainGrowthTime",
	'comment', "total modifier must be 8bit signed int",
	'TargetClass', "RegionDef",
	'TargetId', "Saltu",
	'TargetProp', "PlantsGrowthRainTime",
	'TargetValue', 960000,
}),
PlaceObj('ModItemCode', {
	'name', "Irrigation",
	'CodeFileName', "Code/Irrigation.lua",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, ...)
		if not target.irrigating_allowed then
			target:rfnToggleAllowedIrrigating()
		end
		local activity_obj = target:GetIrrigateFieldActivityObject() or target:CreateIrrigateFieldActivityObject(UIPlayer)
		if IsValid(unit) and activity_obj then
			local activity_id = self.ActivityId
			local skill_level, efficiency =  unit:GetSkillAndEfficiency(activity_id)
			unit:EnqueueAndTryExecuteActivity(activity_id, activity_obj, skill_level, efficiency, clear_queue)
		end
	end,
	ActivityId = "Irrigate",
	CanCancel = function (self, unit, target, param)
		return unit and target:GetIrrigateFieldActivityObject()
	end,
	CanCollapse = true,
	Cancel = function (self, unit, target, param)
		target:CancelIrrigateFieldActivity()
	end,
	GetAssignedUnit = function (self, target, param)
		if IsKindOf(target, "ActivityObject") then
			return target.activity_unit or target.enqueued
		end
		local activity_obj = target.GetIrrigateFieldActivityObject and target:GetIrrigateFieldActivityObject()
		return activity_obj and (activity_obj.activity_unit or activity_obj.enqueued)
	end,
	GetErrorStatus = function (self, unit, target, param)
		if IsValid(target) and target:CanBeIrrigated() then
			if target:ShouldBeIrrigated() then
				local amount = target:GetNeededIrrigateResourceAmount(true, unit and unit.player)
				if amount <= 0 then
					return T(798126454695, "Not enough resources.")
				end
			else
				return T(370622164382, "pending planting or harvesting")
			end
		end
		return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	HasCollapseCountText = false,
	RequireUnit = true,
	ShouldCollapseButtons = function (self, button1, button2)
		return button1.target == button2.target
	end,
	UseOnlyFirstCollapsed = true,
	id = "Irrigate",
}),
}
