AppendClass.PlantContainer = {
    irrigate_activity_object_class = "IrrigateFieldActivityObject",
    irrigating_allowed = false
}

function SavegameFixups.IrrigationInitialize()
    PlantsGrowthUpdate()
    MapForEach(true, "PlantContainer", function(container)
        container:ForEachPlant(function(plant)
            plant:UpdateGrowthModifier(nil, nil, true)
        end)
    end)
end

function OnMsg.ModsReloaded()
    Presets.XTemplate["Infopanel Sections"]["tabOverview_100_PlantBars"][1][3].__condition = nil
    Presets.XTemplate["Infopanel Sections"]["tabOverview_Field"][1][1][8].__condition = nil

    Presets.ActivitySet.Default.Work.Activities.Irrigate = true
    table.insert_unique(Presets.ActivityGroup.Default.Farm.Activities, "Irrigate")
    ForEachPreset("PlantDef", function(preset, group)
        preset.IrrigateAmount = Max(preset.FertilizeAmount or const.ResourceScale)
        if preset.FertilizerGrowth and preset.FertilizerGrowth > 75 then
            preset.FertilizerGrowth = 75
        end
    end)
    PlantsReloadDefs(Plants)
end

DefineClass.IrrigateFieldActivityObject = {
    __parents = { "ActivityObject", "ComplexResStorage" },
    activity_id = "Irrigate",

    desired_res = false,
    desired_amount = 0,
    used_amount = 0,
}

function IrrigateFieldActivityObject:CalcDemandRes(target)
    self.desired_res = target:GetIrrigateResource()
    self.desired_amount = target:GetNeededIrrigateResourceAmount(nil, self.player)
    self:UpdateResPolicies()
end

function IrrigateFieldActivityObject:SetupResPolicies(processed_list)
    local desired_res, desired_amount = self.desired_res, self.desired_amount
    self:SetResourcePolicy(desired_res, 0, desired_amount)
    processed_list[desired_res] = desired_amount
end

function IrrigateFieldActivityObject:CanBeSuppliedByUnit(unit)
    -- do not allow units irrigating neighbouring fields to bring the required resources
    return self.activity_unit == unit or (unit.finding_activity and unit.testing_irrigate == self) or (unit.current_act_target == self)
end

function IrrigateFieldActivityObject:ApproachUnitToTarget(unit)
    -- custom approaches in DoActivity
    unit:ExitHolder()
    return true
end

function IrrigateFieldActivityObject:CanUnitPerformActivity(unit, ...)
    local field = self:GetActivityTarget()
    if not IsValid(field) or not field:ShouldBeIrrigated() then return end

    if self.desired_amount <= 0 then
        -- allow the unit to take this activity so that it gets deleted
        return true
    end

    local amount = field:GetNeededIrrigateResourceAmount(true, unit.player)
    if amount <= 0 then return end

    unit.testing_irrigate = self
    local result = unit:FindDeliveryTask(unit.command_center, self, nil, nil, nil, "test_only")
    unit.testing_irrigate = nil
    return result and ActivityObject.CanUnitPerformActivity(self, unit, ...)
end

function IrrigateFieldActivityObject:OnActivityStarted(unit, ...)
    ActivityObject.OnActivityStarted(self, unit, ...)
    local target = self:GetActivityTarget()
    if not IsValid(target) or not target:ShouldBeIrrigated() or target:GetNeededIrrigateResourceAmount(true, self.player) <= 0 then return end

    local supply_list, demand_list = unit:FindDeliveryTask(unit.command_center, self)
    if supply_list then
        unit:StartDeliveryTask(supply_list, demand_list)--, "skip_anim")
    end
end

function IrrigateFieldActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local field = self:GetActivityTarget()
    local activity_range = self:GetActivityRange()
    local work_time_chunk = unit:GetWorkTimeChunk(field)
    local t_chunk = 0 -- time since last 'work time chunk' check
    local step_start_time = GameTime()
    local watering_can = PlaceObject("WateringCan", nil, const.cofComponentAttach)
    local speed = unit:GetAnimSpeedModifier()
    unit:Attach(watering_can, unit:GetRandomSpot("Spatula"))
    unit:PushDestructor(function(unit)
        unit:SetAnimSpeedModifier(speed)
        if IsValid(watering_can) then
            DoneObject(watering_can)
        end
    end)
    local amount = field:GetPlantDef().IrrigateAmount
    local res = field:GetIrrigateResource()
    field:ForEachPlant(function(plant, self, unit, field)
        if self:GetResAvailable(res) == 0 then
            return "break"
        end
        if plant:CanBeIrrigated() and unit:Approach(plant) then
            unit:SetAnimSpeedModifier(speed / 2)
            unit:PlayMomentTrackedAnim("standing_Plant_Start")
            unit:PlayMomentTrackedAnim("standing_Plant_End")
            unit:SetAnimSpeedModifier(speed)
            plant:Irrigate()
            ObjModified(field)
            self:SubtractRes(res, amount)
        end
        local step_duration = GameTime() - step_start_time
        t_chunk = t_chunk + step_duration
        if t_chunk >= work_time_chunk then
            -- check for something with a higher priority
            unit:SetActivityCommand("queue_is_empty", unit.current_activity)
            t_chunk = 0
        end
        step_start_time = GameTime()
    end, self, unit, field)
    unit:PopAndCallDestructor()
    return true
end

function IrrigateFieldActivityObject:OnActivityStopped(unit, activity, interrupted)
    ActivityObject.OnActivityStopped(self, unit, activity, interrupted)
    if interrupted then
        local res = self.desired_res
        local amount = self:GetResAvailable(res)
        if amount > 0 then
            self:YieldStoredResources(unit)
            self:SubtractRes(res, amount)
        end
    end
    local target = self:GetActivityTarget()
    if IsValid(target) then
        if target:GetNeededIrrigateResourceAmount(nil, self.player) == 0 then
            if not IsBeingDestructed(self) then
                self:delete()
            end
        else
            self:CalcDemandRes(target)
        end
    end
end

function IrrigateFieldActivityObject:Done()
    local target = self:GetActivityTarget()
    if IsValid(target) then
        ActionsOverlay:RemoveOverlayReason(target, "Irrigate")
    end
end

local GatherDirectOrders_FieldBase = FieldBase.GatherDirectOrders
function FieldBase:GatherDirectOrders(unit, orders)
    GatherDirectOrders_FieldBase(self, unit, orders)
    if self:CanBeIrrigated() and (not unit or unit:IsInSameArea(self)) then
        for _, plant in ipairs(self.plants) do
            if IsValid(plant) and plant:CanBeIrrigated() then
                CreateDirectOrderButtons(orders, DirectOrders.Irrigate, self)
                break
            end
        end
    end
end

---

--called from OnObjUpdate
local UpdateRequests_PlantContainer = PlantContainer.UpdateRequests
function PlantContainer:UpdateRequests(...)
    self:ForEachPlant(function(plant, game_time, plant_def)
        plant:UpdateGrowthModifier(game_time, plant_def)
    end, GameTime(), self:GetPlantDef())
    self:UpdateIrrigationRequests()
    return UpdateRequests_PlantContainer(self, ...)
end

function PlantContainer:UpdateIrrigationRequests()
    if not self.irrigating_allowed then return end
    local player = self.player
    local scheduled_time = self.next_irrigate_schedule or 0
    if scheduled_time <= GameTime() and self:ShouldBeIrrigated()
            and not self:GetIrrigateFieldActivityObject() and self:GetNeededIrrigateResourceAmount(true, player) > 0 then
        self:CreateIrrigateFieldActivityObject(player)
        self.next_irrigate_schedule = GameTime() + const.Gameplay.FieldFertilizeScheduleInterval
    end
end

function PlantContainer:CreateIrrigateFieldActivityObject(player)
    if not self:CanBeIrrigated() then return end
    local activity_obj = CreateActivityObject(self.irrigate_activity_object_class, { player = player }, self)
    activity_obj:CalcDemandRes(self)
    activity_obj:SetActivityAvailable(true)
    ActionsOverlay:AddOverlayReason(self, "Irrigate")
    ObjModified(self)
    return activity_obj
end

function PlantContainer:GetIrrigateFieldActivityObject()
    return self:GetAttach(self.irrigate_activity_object_class)
end

function PlantContainer:CancelIrrigateFieldActivity()
    local activity = self:GetIrrigateFieldActivityObject()
    if not activity then return end
    DoneObject(activity)
    ObjModified(self)
end

function PlantContainer:GetNeededIrrigateResourceAmount(supplied_only, player)
    local total = 0
    self:ForEachPlant(function(plant, supplied_only, player)
        if not plant:CanBeIrrigated() then return end
        local new_amount = total + plant:GetPlantDef().IrrigateAmount
        if not supplied_only or player:GetSuppliedResAmount(self:GetIrrigateResource()) >= new_amount then
            total = new_amount
        end
    end, supplied_only, player or self.player)
    return total
end

function PlantContainer:GetIrrigateResource()
    return "ColdWater"
end

function PlantContainer:CanBeIrrigated()
    return true
end

function PlantContainer:ShouldBeIrrigated()
    return self:CanBeIrrigated() and not self:HasPendingActionRequests()
end

function PlantContainer:GetIrrigatedPlantsCount()
    local count = 0
    self:ForEachPlant(function(plant)
        if plant:GetHealth() > 0 and plant:IsIrrigated() then
            count = count + 1
        end
    end)
    return count
end

function PlantContainer:GetWaterInfo()
    if GetPlantRainGrowthEffect() ~= 0 then
        return GameState.Rain and T{"<positive>Raining</positive><right><em><diff(PlantsGrowthRain)>% growth",
                                    PlantsGrowthRain = Region.PlantsGrowthRain}
                or T{"Rain<right><em><diff(PlantsGrowthRain)>% growth for <Irrigated> hours",
                     PlantsGrowthRain = Region.PlantsGrowthRain,
                     Irrigated = DivCeil(Region.PlantsGrowthRainTime - (GameTime() - LastRain), const.HourDuration)}
    end
    local irrigated_plants = self:GetIrrigatedPlantsCount()
    if irrigated_plants > 0 then
        return T{"Irrigated<right><em><diff(PlantsGrowthRain)>% growth for <IrrigatedPlants>/<PlantsCount></em> plants",
                 PlantsGrowthRain = Region.PlantsGrowthRain,
                 IrrigatedPlants = irrigated_plants, PlantsCount = self:GetIPAllPlantsCount()}
    end
    return T{"<negative>No water"}
end

---

function Plant:UpdateGrowthModifier(game_time, plant_def, forced)
    if not forced and not self.fertilization_expire and not self.irrigation_expire then
        return
    end
    game_time = game_time or GameTime()
    plant_def = plant_def or self:GetPlantDef()
    if not plant_def then return end
    local modifier = (self:IsFertilized(game_time) and plant_def.FertilizerGrowth or 0)
            + (self:IsIrrigated(game_time) and GetPlantRainGrowthEffect() == 0 and Region.PlantsGrowthRain or 0)
    local duration
    if self.fertilization_expire and (game_time < self.fertilization_expire) then
        duration = self.fertilization_expire - game_time
    else
        self.fertilization_expire = false
    end
    if self.irrigation_expire and (game_time < self.irrigation_expire) then
        duration = duration and Min(duration, self.irrigation_expire - game_time) or (self.irrigation_expire - game_time)
    else
        self.irrigation_expire = false
    end
    if duration then
        self:SetGrowthModifier(duration, modifier)
    else
        self:SetGrowthModifier(const.HourDuration, 0)
    end
    self:TryReopenInfopanel()
    return modifier --for printing
end

function Plant:CanBeIrrigated()
    if self:IsIrrigated() then return end
    local def = self:GetPlantDef()
    if not def or def.IrrigateAmount <= 0 then
        return
    end
    return self:GetGrowth() < 100 or def.RemainAfterHarvest
end

function Plant:Irrigate()
    local def = self:GetPlantDef()
    if not def then return end
    local game_time = GameTime()
    self.irrigation_expire = game_time + Region.PlantsGrowthRainTime
    self:UpdateGrowthModifier(game_time, def)
end

function Plant:IsIrrigated(game_time)
    return GetPlantRainGrowthEffect() ~= 0 or self.irrigation_expire and (game_time or GameTime()) < self.irrigation_expire
end

function Plant:GetWaterInfo()
    if GetPlantRainGrowthEffect() ~= 0 then
        return GameState.Rain and T{"<positive>Raining</positive><right><em><diff(PlantsGrowthRain)>% growth",
                                    PlantsGrowthRain = Region.PlantsGrowthRain}
                or T{"Rain<right><em><diff(PlantsGrowthRain)>% growth for <Irrigated> hours",
                     PlantsGrowthRain = Region.PlantsGrowthRain,
                     Irrigated = DivCeil(Region.PlantsGrowthRainTime - (GameTime() - LastRain), const.HourDuration)}
    end
    if self:IsIrrigated() then
        return T{"Irrigated<right><em><diff(PlantsGrowthRain)>% growth for <Irrigated> hours",
                 PlantsGrowthRain = Region.PlantsGrowthRain,
                 Irrigated = DivCeil(self.irrigation_expire- GameTime(), const.HourDuration)}
    end
    return T{"<negative>No water"}
end

--brutally overwritten
function Plant:Fertilize()
    local def = self:GetPlantDef()
    if not def then return end
    local game_time = GameTime()
    self.fertilization_expire = game_time + def.FertilizerDuration
    self:UpdateGrowthModifier(game_time, def)
end

--brutally overwritten
function Plant:IsFertilized(game_time)
    return self.fertilization_expire and (game_time or GameTime()) < self.fertilization_expire
end

--brutally overwritten, used in UI
function Plant:GetGrowthModifier()
    local def = self:GetPlantDef()
    return def and def.FertilizerGrowth or 0
end

--brutally overwritten, used in UI
function TFormat.PlantsRainGrowth(context_obj)
    return table.fget(context_obj, "GetWaterInfo", "()")
end

function PlantContainer:rfnToggleAllowedIrrigating()
    local allowed = not self.irrigating_allowed
    self.irrigating_allowed = allowed
    if allowed then
        self.next_fertilize_schedule = nil
        self:UpdateIrrigationRequests()
    else
        self:CancelIrrigateFieldActivity()
    end
    ObjModified(self)
end

function FieldBase:GetToggleAllowedIrrigatingText()
    return T("Irrigate")
end

function FieldBase:GetToggleAllowedIrrigatingTextRollover()
    local def = self:GetPlantDef()
    if def and def.IrrigateAmount <= 0 then
        return T("This field will not benefit from irrigation.")
    end
    return self.irrigating_allowed
            and T("Forbid survivors to irrigate plants on the field.")
            or T("Allow survivors to irrigate plants on the field.")
end

local CreateBinActions_FieldBaseAutoResolve = FieldBaseAutoResolve.CreateBinActions
function FieldBaseAutoResolve.CreateBinActions(bin, host)
    XAction:new({
        ActionId = "idAllowIrrigating",
        ActionName = bin[1]:GetToggleAllowedIrrigatingText(),
        ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/irrigate_forbid",
        ActionToolbar = "ip_actions",
        RolloverTemplate = "Rollover",
        RolloverText = bin[1]:GetToggleAllowedIrrigatingTextRollover(),
        ActionSortKey = "41",
        ActionState = function(self)
            local disabled = true
            for _, field in ipairs(bin) do
                if field:CanBeIrrigated() then
                    if not field.irrigating_allowed then
                        self.RolloverText = field:GetToggleAllowedIrrigatingTextRollover()
                        return "enabled"
                    end
                    disabled = false
                end
            end
            return disabled and "disabled" or "hidden"
        end,
        OnAction = function(...)
            for _, field in ipairs(bin) do
                if field:CanBeIrrigated() and not field.irrigating_allowed then
                    NetSyncEvent("ObjFunc", field, "rfnToggleAllowedIrrigating")
                end
            end
        end,
    }, host)
    XAction:new({
        ActionId = "idForbidIrrigating",
        ActionName = bin[1]:GetToggleAllowedIrrigatingText(),
        ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/irrigate_allow",
        ActionToolbar = "ip_actions",
        RolloverTemplate = "Rollover",
        RolloverText = bin[1]:GetToggleAllowedIrrigatingTextRollover(),
        ActionSortKey = "41",
        ActionState = function(self)
            for _, field in ipairs(bin) do
                if field:CanBeIrrigated() and field.irrigating_allowed then
                    self.RolloverText = field:GetToggleAllowedIrrigatingTextRollover()
                    return "enabled"
                end
            end
            return "hidden"
        end,
        OnAction = function(...)
            for _, field in ipairs(bin) do
                if field.irrigating_allowed then
                    NetSyncEvent("ObjFunc", field, "rfnToggleAllowedIrrigating")
                end
            end
        end,
    }, host)
    return CreateBinActions_FieldBaseAutoResolve(bin, host)
end

function OnMsg.ClassesBuilt()
    local parent = table.find_value(Presets.XTemplate["Infopanel Actions"].IPActionsFields, "__context_of_kind", "FieldBase")
    table.remove_entry(parent, "ActionId", "idToggleAllowedIrrigating")
    parent[#parent + 1] = PlaceObj('XTemplateAction', {
        'RolloverTemplate', "Rollover",
        'RolloverText', T("<ToggleAllowedIrrigatingTextRollover>"),
        'ActionId', "idToggleAllowedIrrigating",
        'ActionSortKey', "16",
        'ActionName', T("<ToggleAllowedIrrigatingText>"),
        'ActionIcon', CurrentModPath .. "UI/Icons/Infopanels/irrigate_allow",
        'ActionToolbar', "ip_actions",
        'ActionState', function (self, host)
            local obj = ResolvePropObj(host.context)
            if not obj:CanBeIrrigated() then
                return "disabled"
            end
            self.ActionIcon = CurrentModPath ..
                    (obj.irrigating_allowed and "UI/Icons/Infopanels/irrigate_allow" or "UI/Icons/Infopanels/irrigate_forbid")
        end,
        'OnAction', function (self, host, source, ...)
            local obj = ResolvePropObj(host.context)
            NetSyncEvent("ObjFunc", obj, "rfnToggleAllowedIrrigating")
        end,
    })
end

local UpdateSkillInclinations_UnitSkill = UnitSkill.UpdateSkillInclinations
function UnitSkill:UpdateSkillInclinations(...)
    if self.activity_forbidden_reasons then
        self.activity_forbidden_reasons["Irrigate"] = self.activity_forbidden_reasons["Fertilizing"]
    end
    return UpdateSkillInclinations_UnitSkill(self, ...)
end
UnitSkill.SetActivityAllowedReason = UnitSkill.UpdateSkillInclinations

function SavegameFixups.FixHopeIrrigate()
    if Hope then
        Hope:UpdateSkillInclinations()
    end
end