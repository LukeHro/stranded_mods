function OnMsg.ModsReloaded()
    table.insert_unique(Presets.AnimMetadata.HumanMale.standing_Heal_Idle.Moments,
            PlaceObj('AnimMoment', { 'Type', "attack", 'AnimRevision', 27816, 'Time', 1800})
    )
end

function AddAnimMetadata(toon)
    for _, anim in pairs(toon:GetStates()) do
        if not Presets.AnimMetadata.HumanMale[anim] then
            Presets.AnimMetadata.HumanMale[anim] = PlaceObj('AnimMetadata', {
                Moments = {
                    PlaceObj('AnimMoment', {
                        'Type', "start",
                        'AnimRevision', 20726,
                    }),
                },
                group = "HumanMale",
                id = anim,
            })
        end
    end
end

local PlayMusicalInstrument_UnitMusical = UnitMusical.PlayMusicalInstrument
function UnitMusical:PlayMusicalInstrument(building, duration)
    if IsKindOf(building, "MandolinBaseBuilding") then
        self:Approach(building)
        self:SetAngle(self:GetVisualAngle() + 10800, 1000)
        local end_time = GameTime() + (duration or 20000) --relaxation routine duration
        building.mandolin:SetVisible(false)
        self.mandolin = PlaceObject("Mandolin", nil, const.cofComponentAttach)
        self:Attach(self.mandolin, self:GetRandomSpot("Weaponl"))
        self:PushDestructor(function(unit)
            if IsValid(building.mandolin) then
                building.mandolin:SetVisible(true)
            end
            if IsValid(unit.mandolin) then
                DoneObject(unit.mandolin)
                unit.mandolin = nil
            end
        end)
        local weight = 66.66
        while GameTime() < end_time do
            self:SetAnim(1, "standing_Heal_Idle", 0, 200)
            self:SetAnimPhase(1, self:GetAnimMoment("standing_Heal_Idle", "attack"))
            self:SetAnimMask(2, "UpperBodyMask")
            self:SetAnim(2, "standing_Idle_Crossbow", 0, 200)
            self:SetAnimWeight(1, 100 - weight)
            self:SetAnimWeight(2, weight)
            Sleep(500)
        end
        self:PopAndCallDestructor()
    else
        return PlayMusicalInstrument_UnitMusical(self, building, duration)
    end
end

function UnitMusical:CmdPlayMandolin(weight, anim)
    weight = weight or 66.66
    self.mandolin = PlaceObject("Mandolin", nil, const.cofComponentAttach)
    self:Attach(self.mandolin, self:GetRandomSpot("Weaponl"))
    self:PushDestructor(function(unit)
        DoneObject(unit.mandolin)
    end)
    while not self:IsManuallyControlled() do
        self:SetAnim(1, "standing_Heal_Idle", 0, 200)
        self:SetAnimPhase(1, self:GetAnimMoment("standing_Heal_Idle", "attack"))
        self:SetAnimMask(2, "UpperBodyMask")
        self:SetAnim(2, anim or "standing_Idle_Crossbow", 0, 200)
        self:SetAnimWeight(1, 100 - weight)
        self:SetAnimWeight(2, weight)
        --self:SetAnimMask(3, "LowerBodyMask")
        --self:SetAnim(3, anim or "social_DancingAlone_Gracefull", 0, 200)
        --self:SetAnimWeight(3, weight)
        Sleep(500)
    end
    self:PopAndCallDestructor()
end