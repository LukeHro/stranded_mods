DefineClass.MandolinBaseBuilding = {
    __parents = { "MusicalInstrumentStandBase" },
    water_plane = false,
}

function MandolinBaseBuilding:Init()
    self.mandolin = self.mandolin or PlaceObject("MandolinOnStand", nil, const.cofComponentAttach)
    self:Attach(self.mandolin, self:GetRandomSpot("Mandolin"))
end

function MandolinBaseBuilding:Done()
    if IsValid(self.mandolin) then
        DoneObject(self.mandolin)
    end
end