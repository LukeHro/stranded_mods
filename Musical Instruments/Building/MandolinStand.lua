UndefineClass('MandolinStand')
DefineClass.MandolinStand = {
	__parents = { "MandolinBaseBuilding", "OwnedComponent", "RelaxationDeviceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "MandolinBaseBuilding",
	LockState = "hidden",
	BuildMenuCategory = "Leisure",
	display_name = T(423555869752, --[[ModItemBuildingCompositeDef MandolinStand display_name]] "Mandolin"),
	description = T(473552163756, --[[ModItemBuildingCompositeDef MandolinStand description]] "A musical instrument used for relaxation. Grants Intellect experience on use."),
	BuildMenuIcon = "Mod/LH_Music/UI/Icons/Build Menu/mandolin.dds",
	BuildMenuPos = 1010,
	display_name_pl = T(666040825987, --[[ModItemBuildingCompositeDef MandolinStand display_name_pl]] "Mandolins"),
	entity = "MandolinStand",
	update_interval = 1000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 5000,
		Wood = 5000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 5000,
		Wood = 3000,
	}),
	Health = 120000,
	MaxHealth = 120000,
	lock_block_box = box(-300, 0, 0, 300, 0, 700),
	SkirtSize = 174,
	EntityHeight = 1383,
	CustomMaterial = "Planks",
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	InstrumentBodyPart = "",
	PlayStartAnim = "standing_Play_Didgeridoo_Start",
	PlayIdleAnim = "standing_Play_Didgeridoo_Idle",
	PlayEndAnim = "standing_Play_Didgeridoo_End",
	OwnedComponent = true,
	RelaxationDeviceComponent = true,
	PowerOutput = 5000,
	PowerTemperature = 90000,
	RadiationTemperature = 20000,
	RadiationRange = 6000,
	can_change_ownership = false,
	ownership_class = "Mandolin",
}

