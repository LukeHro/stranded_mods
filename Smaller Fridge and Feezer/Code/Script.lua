DefineClass.LH_SmallerFrAttach = {
    __parents = { "ComponentAttach" },
    flags = { efWalkable = false, efApplyToGrids = false, efCollision = false }
}

DefineClass.SmallerFreezerBuilding = {
    __parents = { "CompositeBuilding" },
    smaller_right = false,
    smaller_left = false,
}

function SmallerFreezerBuilding:Init()
    self.smaller_right = self.smaller_right or PlaceObject("LH_SmallerFrAttach", nil, const.cofComponentAttach)
    self.smaller_right:ChangeEntity("Interior_Freezer")
    self.smaller_right:SetClipPlane(PlaneFromPoints(0, -480, 0, 0, -480, 1, -1, -480, 0, true))
    self:Attach(self.smaller_right, self:GetRandomSpot("Smaller_right"))
    self.smaller_right:ClearEnumFlags(const.efCollision)
    self.smaller_left = self.smaller_left or PlaceObject("LH_SmallerFrAttach", nil, const.cofComponentAttach)
    self.smaller_left:ChangeEntity("Interior_Freezer")
    self.smaller_left:SetClipPlane(PlaneFromPoints(0, 520, 0, 0, 520, 1, 1, 520, 0, true))
    self:Attach(self.smaller_left, self:GetRandomSpot("Smaller_left"))
    self.smaller_left:ClearEnumFlags(const.efCollision)
end

function SmallerFreezerBuilding:Done()
    if IsValid(self.smaller_right) then
        DoneObject(self.smaller_right)
    end
    if IsValid(self.smaller_left) then
        DoneObject(self.smaller_left)
    end
end

DefineClass.SmallerRefrigeratorBuilding = {
    __parents = { "CompositeBuilding" },
    smaller_right = false,
    smaller_left = false,
}

function SmallerRefrigeratorBuilding:Init()
    self.smaller_right = self.smaller_right or PlaceObject("LH_SmallerFrAttach", nil, const.cofComponentAttach)
    self.smaller_right:ChangeEntity("Interior_Refrigerator")
    self.smaller_right:SetClipPlane(PlaneFromPoints(0, -480, 0, 0, -480, 1, -1, -480, 0, true))
    self:Attach(self.smaller_right, self:GetRandomSpot("Smaller_right"))
    self.smaller_right:ClearEnumFlags(const.efCollision)
    self.smaller_left = self.smaller_left or PlaceObject("LH_SmallerFrAttach", nil, const.cofComponentAttach)
    self.smaller_left:ChangeEntity("Interior_Refrigerator")
    self.smaller_left:SetClipPlane(PlaneFromPoints(0, 520, 0, 0, 520, 1, 1, 520, 0, true))
    self:Attach(self.smaller_left, self:GetRandomSpot("Smaller_left"))
    self.smaller_left:ClearEnumFlags(const.efCollision)
end

function SmallerRefrigeratorBuilding:Done()
    if IsValid(self.smaller_right) then
        DoneObject(self.smaller_right)
    end
    if IsValid(self.smaller_left) then
        DoneObject(self.smaller_left)
    end
end

function OnMsg.ResearchCompleted(tech, player)
    if tech and (tech.id == "Refrigeration" or tech.id == "Freezers")then
        CheckLockPrerequisites(UIPlayer)
    end
    --this doesn't seem to work if hidden by prerequisites
    --if tech and tech.id == "Refrigeration" then
    --    RemovePresetLockStateReason("BuildingCompositeDef", "Storages", "SmallerFridge", "hidden", false, player)
    --end
    --if tech and tech.id == "Freezers" then
    --    RemovePresetLockStateReason("BuildingCompositeDef", "Storages", "SmallerFreezer", "hidden", false, player)
    --end
end

function SavegameFixups.FixSmallerFrCollision()
    MapForEach("map", "SmallerFreezerBuilding", function(fr)
        fr.smaller_left:ClearEnumFlags(const.efCollision)
        fr.smaller_right:ClearEnumFlags(const.efCollision)
    end)
    MapForEach("map", "SmallerRefrigeratorBuilding", function(fr)
        fr.smaller_left:ClearEnumFlags(const.efCollision)
        fr.smaller_right:ClearEnumFlags(const.efCollision)
    end)
end