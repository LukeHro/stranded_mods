return PlaceObj('ModDef', {
	'title', "Smaller Fridge and Feezer",
	'description', 'Adds smaller versions for the fridges and freezers: two squares wide, less power consumption, with a storage capacity of 12.\nNote that the vanilla ones are prefixed with "Large". Maybe will add even smaller if you like it...\n\n[url=paypal.me/LukeHro][b]Paypal Support[/b][/url]',
	'image', "Mod/LH_SmallerFridgeFreezer/Images/SmallerFridgeFreezer.png",
	'last_changes', "More placement fixes",
	'id', "LH_SmallerFridgeFreezer",
	'author', "lukeh_ro",
	'version_major', 1,
	'version_minor', 1,
	'version', 8,
	'lua_revision', 233360,
	'saved_with_revision', 347716,
	'entities', {
		"SmallerFreezer",
		"SmallerRefrigerator",
	},
	'code', {
		"Building/SmallerFreezer.lua",
		"Building/SmallerFridge.lua",
		"Code/Script.lua",
		"Entities/SmallerFreezer.lua",
		"Entities/SmallerRefrigerator.lua",
	},
	'has_data', true,
	'saved', 1704216743,
	'code_hash', -9177462785760608304,
	'steam_id', "3118815343",
	'TagBuildings', true,
	'TagOther', true,
})