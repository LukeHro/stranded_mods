UndefineClass('SmallerFridge')
DefineClass.SmallerFridge = {
	__parents = { "SmallerRefrigeratorBuilding", "MalfunctionOverTimeComponent", "PowerComponent", "ResourceDismantlingComponent", "StorageDepotComponent", "VisualStorageComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "SmallerRefrigeratorBuilding",
	Prerequisites = {
		PlaceObj('CheckTech', {
			Tech = "Refrigeration",
		}),
	},
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	StorageTargetTemperature = 4000,
	TempChangeRateOn = 4000,
	TempChangeRateOff = 3000,
	ClosedStorage = true,
	StorageDemandPriority = 2,
	show_amount_UI = true,
	BuildMenuCategory = "Storages",
	display_name = T(811544559289, --[[ModItemBuildingCompositeDef SmallerFridge display_name]] "Fridge"),
	description = T(277450487659, --[[ModItemBuildingCompositeDef SmallerFridge description]] "Stores <color TextEmphasis>Raw food</color> <image 'UI/Icons/Resources/res_raw_food' 1100> and <color TextEmphasis>Cooked meals</color> <image 'UI/Icons/Resources/res_food' 1100> at low temperatures. Slows down resource decay."),
	BuildMenuIcon = "Mod/LH_SmallerFridgeFreezer/UI/Icons/Build Menu/fridge_smaller.dds",
	BuildMenuPos = 30,
	display_name_pl = T(891233100470, --[[ModItemBuildingCompositeDef SmallerFridge display_name_pl]] "Fridges"),
	entity = "SmallerRefrigerator",
	labels = {
		"BerserkTargets",
	},
	update_interval = 20000,
	can_turn_off = true,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	construction_points = 30000,
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 10000,
	}),
	Health = 300000,
	MaxHealth = 300000,
	lock_block_box = box(-300, -300, 0, 600, 1500, 1750),
	lock_pass_box = box(-300, -300, 0, 600, 1500, 1750),
	SkirtSize = 1,
	EntityHeight = 1991,
	BuildStartAnimation = "standing_Repair_Weld_Start",
	BuildIdleAnimations = {
		"standing_Repair_Weld_Idle",
	},
	BuildEndAnimation = "standing_Repair_Weld_End",
	BuildActivityTool = "WeldingTool",
	attack_attraction = 40,
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	MalfunctionOverTimeComponent = true,
	PowerComponent = true,
	ResourceDismantlingComponent = true,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	MinMalfunctionDamage = 10,
	MaxMalfunctionDamage = 25,
	IsPowerConsumer = true,
	PowerConsumption = 6000,
	HasSmartConnection = true,
	stack_count = 12,
	accepted_res = {
		"FoodRaw",
		"FoodProcessed",
		"Drink",
		"Other_Refrigerated",
	},
	placement_spots = {
		"Resourceup",
		"Resourcemiddle1",
		"Resourcemiddle2",
		"Resourcedown",
	},
}

