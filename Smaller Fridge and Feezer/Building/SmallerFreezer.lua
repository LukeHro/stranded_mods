UndefineClass('SmallerFreezer')
DefineClass.SmallerFreezer = {
	__parents = { "SmallerFreezerBuilding", "MalfunctionOverTimeComponent", "PowerComponent", "ResourceDismantlingComponent", "StorageDepotComponent", "VisualStorageComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "SmallerFreezerBuilding",
	Prerequisites = {
		PlaceObj('CheckTech', {
			Tech = "Freezers",
		}),
	},
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	StorageTargetTemperature = -18000,
	TempChangeRateOn = 4000,
	TempChangeRateOff = 3000,
	ClosedStorage = true,
	StorageDemandPriority = 2,
	show_amount_UI = true,
	BuildMenuCategory = "Storages",
	display_name = T(911573229976, --[[ModItemBuildingCompositeDef SmallerFreezer display_name]] "Freezer"),
	description = T(962537785366, --[[ModItemBuildingCompositeDef SmallerFreezer description]] "Stores <color TextEmphasis>Raw food</color> <image 'UI/Icons/Resources/res_raw_food' 1100> and <color TextEmphasis>Cooked meals</color> <image 'UI/Icons/Resources/res_food' 1100> at frozen state. Stops resource decay."),
	BuildMenuIcon = "Mod/LH_SmallerFridgeFreezer/UI/Icons/Build Menu/freezer_smaller.dds",
	BuildMenuPos = 31,
	display_name_pl = T(981925682531, --[[ModItemBuildingCompositeDef SmallerFreezer display_name_pl]] "Freezers"),
	entity = "SmallerFreezer",
	labels = {
		"BerserkTargets",
	},
	update_interval = 20000,
	can_turn_off = true,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
		ScrapElectronics = 1000,
	}),
	construction_points = 30000,
	repair_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
		ScrapElectronics = 1000,
	}),
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapElectronics = 1000,
		ScrapMetal = 20000,
	}),
	Health = 300000,
	MaxHealth = 300000,
	lock_block_box = box(-300, -300, 0, 600, 1500, 1750),
	lock_pass_box = box(-300, -300, 0, 600, 1500, 1750),
	SkirtSize = 39,
	EntityHeight = 2088,
	BuildStartAnimation = "standing_Repair_Weld_Start",
	BuildIdleAnimations = {
		"standing_Repair_Weld_Idle",
	},
	BuildEndAnimation = "standing_Repair_Weld_End",
	BuildActivityTool = "WeldingTool",
	attack_attraction = 40,
	AttackAttractionGetter = function (self)
		if not self.res_amounts:IsEmpty() then
			return self.attack_attraction * 3
		else
			return self.attack_attraction
		end
	end,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	MalfunctionOverTimeComponent = true,
	PowerComponent = true,
	ResourceDismantlingComponent = true,
	StorageDepotComponent = true,
	VisualStorageComponent = true,
	MinMalfunctionDamage = 10,
	MaxMalfunctionDamage = 25,
	IsPowerConsumer = true,
	PowerConsumption = 8000,
	HasSmartConnection = true,
	stack_count = 12,
	accepted_res = {
		"FoodRaw",
		"FoodProcessed",
		"Drink",
		"Other_Refrigerated",
	},
	placement_spots = {
		"Resourceup",
		"Resourcemiddle1",
		"Resourcemiddle2",
		"Resourcedown",
	},
}

