UndefineClass('Treadmill')
DefineClass.Treadmill = {
	__parents = { "Building", "OwnedComponent", "PowerComponent", "RelaxationDeviceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	LockState = "hidden",
	BuildMenuCategory = "Leisure",
	display_name = T(647117418634, --[[ModItemBuildingCompositeDef Treadmill display_name]] "Treadmill"),
	description = T(956361608570, --[[ModItemBuildingCompositeDef Treadmill description]] "Exercise equipment used for relaxation. Grants Physical experience on use."),
	BuildMenuIcon = "Mod/LH_Treadmill/UI/Icons/Build Menu/treadmill.dds",
	BuildMenuPos = 5030,
	display_name_pl = T(144087973297, --[[ModItemBuildingCompositeDef Treadmill display_name_pl]] "Treadmills"),
	entity = "Treadmill",
	update_interval = 5000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 30000,
		ScrapElectronics = 1000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	Health = 160000,
	MaxHealth = 160000,
	lock_block_box = box(-600, -300, 0, 1500, 300, 1400),
	SkirtSize = 198,
	EntityHeight = 1444,
	orient_to_terrain = true,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	OwnedComponent = true,
	PowerComponent = true,
	RelaxationDeviceComponent = true,
	can_change_ownership = false,
	ownership_class = "BenchPress",
	IsPowerConsumer = true,
	PowerConsumption = 6000,
	HasSmartConnection = true,
	ManageActiveState = true,
}

