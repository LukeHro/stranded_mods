return {
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Leisure",
	BuildMenuIcon = "Mod/LH_Treadmill/UI/Icons/Build Menu/treadmill.dds",
	BuildMenuPos = 5030,
	EntityHeight = 1444,
	HasSmartConnection = true,
	Health = 160000,
	IsPowerConsumer = true,
	LockState = "hidden",
	ManageActiveState = true,
	MaxHealth = 160000,
	OwnedComponent = true,
	PowerComponent = true,
	PowerConsumption = 6000,
	RelaxationDeviceComponent = true,
	SkirtSize = 198,
	attack_attraction = 5,
	can_change_ownership = false,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 30000,
		ScrapElectronics = 1000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('AlignToWallConstructionRule', nil),
	},
	description = T(956361608570, --[[ModItemBuildingCompositeDef Treadmill description]] "Exercise equipment used for relaxation. Grants Physical experience on use."),
	display_name = T(647117418634, --[[ModItemBuildingCompositeDef Treadmill display_name]] "Treadmill"),
	display_name_pl = T(144087973297, --[[ModItemBuildingCompositeDef Treadmill display_name_pl]] "Treadmills"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Treadmill",
	group = "Relaxatation",
	id = "Treadmill",
	lock_block_box = box(-600, -300, 0, 1500, 300, 1400),
	object_class = "Building",
	orient_to_terrain = true,
	ownership_class = "BenchPress",
	update_interval = 5000,
}),
PlaceObj('ModItemCode', {
	'name', "TreadmillScript",
	'CodeFileName', "Code/TreadmillScript.lua",
}),
PlaceObj('ModItemDeviceInterface', {
	id = "TreadmillInterface",
	prg_name = "Treadmill",
	spot_name = "Treadmillinterface",
}),
PlaceObj('ModItemEntity', {
	'name', "Treadmill",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "Treadmill",
	'material_type', "Metal",
}),
PlaceObj('ModItemHappinessFactor', {
	Description = T(376350202938, --[[ModItemHappinessFactor UsedTreadmill Description]] '"I feel better already!"'),
	DisplayName = T(318250172709, --[[ModItemHappinessFactor UsedTreadmill DisplayName]] "Used treadmill"),
	Expiration = true,
	ExpirationTime = 960000,
	Happiness = 18,
	StackLimit = 1,
	group = "Relaxation routines",
	id = "UsedTreadmill",
}),
PlaceObj('ModItemRelaxationRoutine', {
	Description = T(976750289247, --[[ModItemRelaxationRoutine TreadmillRunning Description]] "Another great way to relax while keeping in good shape."),
	DeviceInterface = "TreadmillInterface",
	DirectOrderText = T(108803485751, --[[ModItemRelaxationRoutine TreadmillRunning DirectOrderText |gender-variants]] "Use treadmill"),
	DisplayName = T(371117965104, --[[ModItemRelaxationRoutine TreadmillRunning DisplayName]] "Running"),
	Execute = function (self, unit, target, end_time)
		local interface = DeviceInterfaces[self.DeviceInterface]
		unit:PlayPrg(PrgAmbientLife[interface.prg_name], end_time - GameTime(), target)
		return true
	end,
	HappinessFactor = "UsedTreadmill",
	IsValidTarget = function (self, target, unit, direct_order)
		if not target.working or not target:CanBeOwnedBy(unit, direct_order) then return end
		return RelaxationRoutine.IsValidTarget(self, target, unit, direct_order)
	end,
	OnEnd = function (self, unit, target, success)
		if success then
			unit:AddSkillExperience("Physical", 200)
		end
	end,
	OnStart = function (self, unit, target)  end,
	Preference = 10,
	RequiredLabel = "Treadmill",
	ReserveTarget = true,
	Satisfaction = 25000,
	SortKey = 500,
	StatusText = T(356518662717, --[[ModItemRelaxationRoutine TreadmillRunning StatusText |gender-variants]] "Running"),
	id = "TreadmillRunning",
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Leisure",
	},
	Description = T(505330745519, --[[ModItemTech Treadmill Description]] "Using a treadmill is an excellent way to burn some extra calories and fat.\n\n<style TechSubtitleBlue>Unlocks</style>\n   <color TextEmphasis>Treadmill</color>: 1<image 'UI/Icons/Resources/res_electronic_components' 1100> 30<image 'UI/Icons/Resources/res_metal_ingot' 1100>"),
	DisplayName = T(203723851659, --[[ModItemTech Treadmill DisplayName]] "Treadmills"),
	Icon = "Mod/LH_Treadmill/UI/Icons/Research/treadmills.dds",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "Metal",
		}),
		PlaceObj('CheckTech', {
			Tech = "BasicElectricity",
		}),
	},
	ResearchPoints = 48000,
	SortKey = 29,
	group = "Construction",
	id = "Treadmill",
	money_value = 50000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Relaxatation",
		LockState = "hidden",
		PresetId = "Treadmill",
	}),
}),
}
