local _slots = {
    {
        adjust_z = false,
        flags_missing = 1,
        goto_spot = "LeadToSpot",
        groups = {
            ["A"] = true,
        },
        spots = {
            "Treadmillinterface",
        },
    },
}
PrgAmbientLife["Treadmill"] = function(unit, bld)
    local _spot, _obj, _slot_desc, _slot, _slotname
    _spot, _obj, _slot_desc, _slot, _slotname = PrgGetObjRandomSpotFromGroup(bld, nil, "A", _slots, unit)
    if _spot then
        PrgVisitSlot(unit, bld, _obj, _spot, _slot_desc, _slot, _slotname, nil, nil)
        if unit.visit_restart then return end
    else
        PrgVisitHolder(unit, bld)
        if unit.visit_restart then return end
    end
end

PrgAmbientLife["Treadmillinterface"] = function(unit, bld, obj, spot, slot_data, slot, slotname)
    unit:PushDestructor(function(unit)
        Sleep(unit:TimeToAnimEnd())
    end)
    unit:SetAngle(unit:GetAngle() + 10800, 0)
    unit:PlayMomentTrackedAnim("standing_Run_Free", 1, nil, 200, nil)
    while unit:VisitTimeLeft() > 0 do
        unit:PlayMomentTrackedAnim("standing_Run_Free", 1, nil, 200, nil)
        if unit.visit_restart then return end
    end
    unit:PopAndCallDestructor()
end