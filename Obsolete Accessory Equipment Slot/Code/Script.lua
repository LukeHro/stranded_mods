LH_NotAccessories = {}

--like this as an example how other mods can use this to keep they tools in the tool slot if needed
function OnMsg.ClassesBuilt()
    table.insert_unique(LH_NotAccessories, "EMUmbrella")
    table.insert_unique(LH_NotAccessories, "HarvestingTools")
end

function OnMsg.ClassesBuilt()
    if (XTemplates.tabHumanEquipment_Slots[1][2][1][1][5][2].Id == "idAccessory") then
        return
    end
    XTemplates.tabHumanEquipment_Slots[1][2][1][1][5][3] = XTemplates.tabHumanEquipment_Slots[1][2][1][1][5][2]
    XTemplates.tabHumanEquipment_Slots[1][2][1][1][5][2] = PlaceObj("XTemplateTemplate", {
        "__template", "IPHumanEquipSlot",
        "Id", "idAccessory",
        "Margins", box(184, 0, 0, 160),
        "HAlign", "left",
        "VAlign", "bottom",
        "GridX", 3,
        "RelativeFocusOrder", "next-in-line",
        "OnPressParam", "Accessory",
        "BindTo", "Accessory"
    })
    table.insert_unique(ToolSlots, "Accessory")
    table.insert_unique(EquipSlots, "Accessory")
    EquipSlotsNames.Accessory = T("Accessory")
    EquipSlotsPrompts.Accessory = T("Select an accessory to equip")
    local OnPropUpdate_template = XTemplates.IPHumanEquipSlot[1][10][1][1]
    if string.find(OnPropUpdate_template.name, "OnPropUpdate") and not OnPropUpdate_template.lh_changed then
        local OnPropUpdate_func = OnPropUpdate_template.func
        OnPropUpdate_template.lh_changed = true
        OnPropUpdate_template.func = function(self, context, prop_meta, value)
            if value or self:GetBindTo() ~= "Accessory" then
                return OnPropUpdate_func(self, context, prop_meta, value)
            end
            local icon = self:ResolveId("idItem")
            local SetImage_func = icon.SetImage
            icon.SetImage = function(self, path)
                SetImage_func(self, CurrentModPath .. path)
            end
            local result = OnPropUpdate_func(self, context, prop_meta, value)
            icon.SetImage = SetImage_func
            return result
        end
    end
end

function OnMsg.ModsReloaded()
    ForEachPreset("ToolResource", function(preset)
        if preset.EquipSlot == "Tool" and preset.Uses == 0 then
            preset.EquipSlot = "Accessory"
        end
        if preset.EquipSlot == "Accessory" and table.find(LH_NotAccessories, preset.id) then
            preset.EquipSlot = "Tool"
        end
    end)
end

--handle RespiratorMask and others that may use those functions bellow

local HasValidSurvivalTool_Human = Human.HasValidSurvivalTool
function Human:HasValidSurvivalTool(tool)
    local equipment_def = self:GetValidEquipment("Accessory")
    return equipment_def and equipment_def.id == tool or HasValidSurvivalTool_Human(self, tool)
end

local HasSurvivalTool_Human = Human.HasSurvivalTool
function Human:HasSurvivalTool(tool)
    local equipment_def = self:GetEquipment("Accessory")
    return equipment_def and equipment_def.id == tool or HasSurvivalTool_Human(self, tool)
end