return {
PlaceObj('ModItemBuildMenuCategory', {
	BuildMenuCategory = "Devices",
	BuildMenuIcon = "Mod/LH_Chemistry/UI/Icons/Build Menu/chemistry_metal.dds",
	BuildMenuPos = 7,
	SortKey = 7,
	description = "",
	display_name = T(530083619662, --[[ModItemBuildMenuCategory sub_Chemistry_Tables display_name]] "Chemistry table"),
	id = "sub_Chemistry_Tables",
	menu_description = T(203623314024, --[[ModItemBuildMenuCategory sub_Chemistry_Tables menu_description]] "Used for crafting of various chemical products - <color TextEmphasis>Medicines</color> <image 'UI/Icons/Resources/res_medicines' 1100>, <color TextEmphasis>Fuel</color> <image 'UI/Icons/Resources/res_oil' 1100> and other."),
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "sub_Chemistry_Tables",
	BuildMenuIcon = "Mod/LH_Chemistry/UI/Icons/Build Menu/chemistry_wood.dds",
	BuildMenuPos = 10,
	EntityHeight = 1220,
	Health = 120000,
	MaxHealth = 120000,
	ProductionDeviceComponent = true,
	ProductionDeviceSkipsStateChange = true,
	SkirtSize = 56,
	attack_attraction = 5,
	construction_cost = PlaceObj('ConstructionCost', {
		Wood = 10000,
	}),
	construction_points = 15000,
	description = T(170099655560, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood description]] "Used for crafting of various chemical products - <color TextEmphasis>Medicines</color> <image 'UI/Icons/Resources/res_medicines' 1100>, <color TextEmphasis>Fuel</color> <image 'UI/Icons/Resources/res_oil' 1100> and other."),
	display_name = T(182990601513, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood display_name]] "Wooden chemistry table"),
	display_name_pl = T(579950452404, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood display_name_pl]] "Wooden workbenches"),
	display_name_short = T(571735638801, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood display_name_short]] "Workbench"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "ChemistryTableWood",
	group = "Devices",
	id = "Chemistry_Table_Wood",
	interfaces = {
		"BasicMixInterface",
	},
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-600, -1800, 0, 600, 300, 1050),
	menu_display_name = T(208011555743, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood menu_display_name]] "Wooden"),
	object_class = "Building",
	prefer_in_buildmenu = true,
	unload_anim_hands = "standing_DropDown_Hands",
	update_interval = 5000,
	upgrade_label = "Workbench",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "sub_Chemistry_Tables",
	BuildMenuIcon = "Mod/LH_Chemistry/UI/Icons/Build Menu/chemistry_metal.dds",
	BuildMenuPos = 20,
	CustomMaterial = "Metal-Construction",
	EntityHeight = 1328,
	Health = 180000,
	MaxHealth = 180000,
	ProductionDeviceComponent = true,
	ProductionDeviceSkipsStateChange = true,
	SkirtSize = 116,
	attack_attraction = 5,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	construction_points = 20000,
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 10000,
	}),
	description = T(901307536483, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal description]] "Used for crafting of various chemical products - <color TextEmphasis>Medicines</color> <image 'UI/Icons/Resources/res_medicines' 1100>, <color TextEmphasis>Fuel</color> <image 'UI/Icons/Resources/res_oil' 1100> and other."),
	display_name = T(573300933200, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal display_name]] "Metal chemistry table"),
	display_name_pl = T(257546727256, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal display_name_pl]] "Metal workbenches"),
	display_name_short = T(785084073602, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal display_name_short]] "Workbench"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "ChemistryTableMetal",
	group = "Devices",
	id = "Chemistry_Table_Metal",
	interfaces = {
		"BasicMixInterface",
	},
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-600, -1500, 0, 600, 600, 1050),
	menu_display_name = T(168912777205, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal menu_display_name]] "Metal"),
	object_class = "Building",
	unload_anim_hands = "standing_DropDown_Hands",
	update_interval = 5000,
	upgrade_label = "Workbench",
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "BuildingCompositeDef",
	'TargetId', "Workbench_Wood",
	'TargetProp', "interfaces",
	'EditType', "Code",
	'TargetFunc', function (self, value, default)
		table.remove_value(value, "BasicMixInterface")
		return value
	end,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "BuildingCompositeDef",
	'TargetId', "Workbench_Metal_2",
	'TargetProp', "interfaces",
	'EditType', "Code",
	'TargetFunc', function (self, value, default)
		table.remove_value(value, "BasicMixInterface")
		return value
	end,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "Slop",
	'TargetProp', "RequiredDeviceInterfaces",
	'TargetValue', {
		"CauldronInterface",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "MedicineMedKit",
	'TargetProp', "RequiredDeviceInterfaces",
	'TargetValue', {
		"BasicCraftingInterface",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "RepairParts",
	'TargetProp', "RequiredDeviceInterfaces",
	'TargetValue', {
		"BasicCraftingInterface",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "LiquidFuel_FuelManure",
	'TargetProp', "RequiredDeviceInterfaces",
	'TargetValue', {
		"BasicFermentationInterface",
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "LiquidFuel_FuelManure",
	'TargetProp', "DeviceWorkTime",
	'TargetValue', 3840000,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "LiquidFuel_FuelManure",
	'TargetProp', "ManualWork",
	'EditType', "Code",
	'TargetFunc', function (self, value, default)
		return false
	end,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "LiquidFuel_FuelManure",
	'TargetProp', "Activity",
	'EditType', "Code",
	'TargetFunc', function (self, value, default)
		return nil
	end,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Recipe",
	'TargetId', "LiquidFuel_FromInsectMeat",
	'TargetProp', "BuildCategory",
	'TargetValue', "CraftResources",
}),
PlaceObj('ModItemEntity', {
	'name', "ChemistryTableMetal",
	'fade_category', "Never",
	'entity_name', "ChemistryTableMetal",
}),
PlaceObj('ModItemEntity', {
	'name', "ChemistryTableWood",
	'fade_category', "Never",
	'entity_name', "ChemistryTableWood",
}),
}
