UndefineClass('Chemistry_Table_Wood')
DefineClass.Chemistry_Table_Wood = {
	__parents = { "Building", "ProductionDeviceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "sub_Chemistry_Tables",
	display_name = T(182990601513, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood display_name]] "Wooden chemistry table"),
	description = T(170099655560, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood description]] "Used for crafting of various chemical products - <color TextEmphasis>Medicines</color> <image 'UI/Icons/Resources/res_medicines' 1100>, <color TextEmphasis>Fuel</color> <image 'UI/Icons/Resources/res_oil' 1100> and other."),
	menu_display_name = T(208011555743, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood menu_display_name]] "Wooden"),
	BuildMenuIcon = "Mod/LH_Chemistry/UI/Icons/Build Menu/chemistry_wood.dds",
	BuildMenuPos = 10,
	display_name_pl = T(579950452404, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood display_name_pl]] "Wooden workbenches"),
	display_name_short = T(571735638801, --[[ModItemBuildingCompositeDef Chemistry_Table_Wood display_name_short]] "Workbench"),
	entity = "ChemistryTableWood",
	labels = {
		"BerserkTargets",
	},
	update_interval = 5000,
	construction_cost = PlaceObj('ConstructionCost', {
		Wood = 10000,
	}),
	construction_points = 15000,
	Health = 120000,
	MaxHealth = 120000,
	upgrade_label = "Workbench",
	lock_block_box = box(-600, -1800, 0, 600, 300, 1050),
	SkirtSize = 56,
	EntityHeight = 1220,
	attack_attraction = 5,
	prefer_in_buildmenu = true,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	ProductionDeviceComponent = true,
	interfaces = {
		"BasicMixInterface",
	},
	ProductionDeviceSkipsStateChange = true,
}

