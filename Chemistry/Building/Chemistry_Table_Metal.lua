UndefineClass('Chemistry_Table_Metal')
DefineClass.Chemistry_Table_Metal = {
	__parents = { "Building", "ProductionDeviceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "sub_Chemistry_Tables",
	display_name = T(573300933200, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal display_name]] "Metal chemistry table"),
	description = T(901307536483, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal description]] "Used for crafting of various chemical products - <color TextEmphasis>Medicines</color> <image 'UI/Icons/Resources/res_medicines' 1100>, <color TextEmphasis>Fuel</color> <image 'UI/Icons/Resources/res_oil' 1100> and other."),
	menu_display_name = T(168912777205, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal menu_display_name]] "Metal"),
	BuildMenuIcon = "Mod/LH_Chemistry/UI/Icons/Build Menu/chemistry_metal.dds",
	BuildMenuPos = 20,
	display_name_pl = T(257546727256, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal display_name_pl]] "Metal workbenches"),
	display_name_short = T(785084073602, --[[ModItemBuildingCompositeDef Chemistry_Table_Metal display_name_short]] "Workbench"),
	entity = "ChemistryTableMetal",
	labels = {
		"BerserkTargets",
	},
	update_interval = 5000,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	construction_points = 20000,
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 10000,
	}),
	Health = 180000,
	MaxHealth = 180000,
	upgrade_label = "Workbench",
	lock_block_box = box(-600, -1500, 0, 600, 600, 1050),
	SkirtSize = 116,
	EntityHeight = 1328,
	CustomMaterial = "Metal-Construction",
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	ProductionDeviceComponent = true,
	interfaces = {
		"BasicMixInterface",
	},
	ProductionDeviceSkipsStateChange = true,
}

