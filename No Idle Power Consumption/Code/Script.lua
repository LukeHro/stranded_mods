local BeginCrafting_ProductionDeviceComponent = ProductionDeviceComponent.BeginCrafting
function ProductionDeviceComponent:BeginCrafting(...)
    self.power_idle = nil
    if self:IsKindOf("PowerComponent") then
        self.power_idle_disable = true
        PowerComponent.UpdatePowerElement(self)
    end
    return BeginCrafting_ProductionDeviceComponent(self, ...)
end

local InterruptProduction_ProductionDeviceComponent = ProductionDeviceComponent.InterruptProduction
function ProductionDeviceComponent:InterruptProduction(...)
    local result = InterruptProduction_ProductionDeviceComponent(self, ...)
    if self:IsKindOf("PowerComponent") then
        PowerComponent.UpdatePowerElement(self)
    end
    return result
end

local OnEnterHolder_Holder = Holder.OnEnterHolder
function Holder:OnEnterHolder(...)
    local result = OnEnterHolder_Holder(self, ...)
    if self:IsKindOf("PowerComponent") and self:IsKindOfClasses("ResearchTerminalBase", "RelaxationDeviceComponent", "MedicalBed") then
        self.power_idle_disable = true
        PowerComponent.UpdatePowerElement(self)
    end
    return result
end

local OnExitHolder_Holder = Holder.OnExitHolder
function Holder:OnExitHolder(...)
    local result = OnExitHolder_Holder(self, ...)
    if self:IsKindOf("PowerComponent") and self:IsKindOfClasses("ResearchTerminalBase", "RelaxationDeviceComponent", "MedicalBed") then
        PowerComponent.UpdatePowerElement(self)
    end
    return result
end

local CalcPowerConsumption_PowerComponent = PowerComponent.CalcPowerConsumption
function PowerComponent:CalcPowerConsumption()
    if self.power_idle_disable then
        self.power_idle_disable = nil
        self.power_idle = nil
    elseif self:IsKindOf("ProductionDeviceComponent") and not self.working_on_recipe then
        self.power_idle = true
        return 0
    elseif self:IsKindOfClasses("ResearchTerminalBase", "RelaxationDeviceComponent") and not next(self.units) then
        self.power_idle = true
        return 0
    elseif self:IsKindOf("MedicalBed") and not self.healing_unit then
        self.power_idle = true
        return 0
    elseif self:IsKindOfClasses("ProductionDeviceComponent", "ResearchTerminalBase", "RelaxationDeviceComponent", "MedicalBed") then
        self.power_idle = nil
    end
    return CalcPowerConsumption_PowerComponent(self)
end

function OnMsg.PowerGridPowered()
    MapForEach("map", "PowerComponent", function(comp)
        if comp.power_idle and (not comp.working or not comp.active) and comp:CanBePowered() then
            comp:UpdateWorking()
        end
    end)
end

local CanWork_PowerComponent = PowerComponent.CanWork
function PowerComponent:CanWork(...)
    if self.power_idle and self:CanBePowered() then
        return true
    end
    return CanWork_PowerComponent(self, ...)
end

local HasNoPower_PowerComponent = PowerComponent.HasNoPower
function PowerComponent:HasNoPower(...)
    if self.power_idle and not self:CanBePowered() then
        return false
    end
    return HasNoPower_PowerComponent(self, ...)
end

function PowerComponent:CanBePowered()
    local grid = self:GetPowerGrid()
    if not grid then
        return false
    end
    if grid.total_production - grid.total_consumption >= self.PowerConsumption then
        --production can sustain new consumption
        return true
    end
    --need better way to determine if batteries can sustain new consumption
    if grid:GetIPCurrentStorageWh() > 0 then
        return true
    end
    return false
end