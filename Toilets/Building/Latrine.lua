UndefineClass('Latrine')
DefineClass.Latrine = {
	__parents = { "OutdoorToiletBuilding" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "OutdoorToiletBuilding",
	BuildMenuCategory = "Water",
	display_name = T(657193361311, --[[ModItemBuildingCompositeDef Latrine display_name]] "Latrine"),
	description = T(383603256713, --[[ModItemBuildingCompositeDef Latrine description]] "Basic retreat for private needs."),
	menu_display_name = T(850149857028, --[[ModItemBuildingCompositeDef Latrine menu_display_name]] "Latrine"),
	BuildMenuIcon = "Mod/LH_Toilets/UI/Icons/Build Menu/latrine.dds",
	BuildMenuPos = 1,
	display_name_pl = T(677992961325, --[[ModItemBuildingCompositeDef Latrine display_name_pl]] "Latrines"),
	display_name_short = T(682916629853, --[[ModItemBuildingCompositeDef Latrine display_name_short]] "Latrine"),
	entity = "Latrine",
	construction_cost = PlaceObj('ConstructionCost', {
		ScrapMetal = 50000,
	}),
	construction_points = 40000,
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 20000,
	}),
	Health = 60000,
	MaxHealth = 60000,
	can_be_placed_on_floor = false,
	RoomPlacement = "outdoors",
	CanPlaceInShelter = false,
	lock_block_box = box(-600, -600, 0, 600, 1500, 2100),
	SkirtSize = 0,
	EntityHeight = 1286,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
}

