UndefineClass('Toilet')
DefineClass.Toilet = {
	__parents = { "IndoorToiletBuilding", "OwnedComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "IndoorToiletBuilding",
	LockState = "hidden",
	BuildMenuCategory = "Water",
	display_name = T(516961879347, --[[ModItemBuildingCompositeDef Toilet display_name]] "Toilet"),
	description = T(256874679443, --[[ModItemBuildingCompositeDef Toilet description]] "All the possible comfort private needs."),
	menu_display_name = T(779941379129, --[[ModItemBuildingCompositeDef Toilet menu_display_name]] "Toilet"),
	BuildMenuIcon = "Mod/LH_Toilets/UI/Icons/Build Menu/toilet.dds",
	BuildMenuPos = 2,
	display_name_pl = T(920544763132, --[[ModItemBuildingCompositeDef Toilet display_name_pl]] "Toilets"),
	display_name_short = T(324039149076, --[[ModItemBuildingCompositeDef Toilet display_name_short]] "Toilet"),
	entity = "Toilet",
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
		PlaceObj('ConstructionCheckRoomPlacement', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	Health = 60000,
	MaxHealth = 60000,
	attached_to_wall = true,
	RoomPlacement = "indoors",
	CanPlaceInShelter = false,
	lock_block_box = box(0, 0, 350, 900, 300, 2100),
	SkirtSize = 0,
	EntityHeight = 2400,
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	OwnedComponent = true,
	PowerTemperature = 30000,
	RadiationTemperature = 30000,
	RadiationRange = 500,
	ownership_class = "Toilet",
	ChangeOwnerIcon = "Mod/LH_Toilets/UI/Icons/Infopanels/toilet_change_owner.dds",
	Filter = function (obj)
		return not obj:HasUnitTag("Android")
	end,
}

