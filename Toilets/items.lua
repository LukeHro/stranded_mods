return {
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(591835666615, --[[ModItemActivityType UseToilet ActivityStatusText |gender-variants]] "Use toilet"),
	ActivityText = T(944583656278, --[[ModItemActivityType UseToilet ActivityText |gender-variants]] "Use toilet"),
	AllowAutoRun = false,
	DefaultPriority = 1,
	Description = T(132675748625, --[[ModItemActivityType UseToilet Description]] "Takes care of private needs."),
	DestlockImportance = 1,
	DisplayName = T(328357467884, --[[ModItemActivityType UseToilet DisplayName]] "Use toilet"),
	GetDisplayedStatusText = function (self, unit, short)
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PriorityDisplayName = T(294793345556, --[[ModItemActivityType UseToilet PriorityDisplayName]] "Using toilet"),
	QueueStatusText = T(894421831047, --[[ModItemActivityType UseToilet QueueStatusText |gender-variants]] "Use toilet"),
	SortKey = 1460,
	StatusIcon = "Mod/LH_Toilets/UI/Icons/Hud/st_toilet.dds",
	id = "UseToilet",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(304446083802, --[[ModItemActivityType NeedToilet ActivityStatusText |gender-variants]] "Need toilet"),
	ActivityText = T(169910919081, --[[ModItemActivityType NeedToilet ActivityText |gender-variants]] "Need toilet"),
	AllowAutoRun = false,
	Cmd = "CmdNeedToilet",
	DefaultPriority = 1,
	Description = T(191012251756, --[[ModItemActivityType NeedToilet Description]] "Takes care of private needs."),
	DestlockImportance = 1,
	DisplayName = T(480780566709, --[[ModItemActivityType NeedToilet DisplayName]] "Need toilet"),
	FindTarget = function (self, unit, range, prev_target)
		return FindToilet(unit, range)
	end,
	GetDisplayedStatusText = function (self, unit, short)
		return GetTByGender(self.ActivityStatusText, unit)
	end,
	PriorityDisplayName = T(182523507427, --[[ModItemActivityType NeedToilet PriorityDisplayName]] "Needing toilet"),
	QueueStatusText = T(405013316612, --[[ModItemActivityType NeedToilet QueueStatusText |gender-variants]] "Need toilet"),
	SortKey = 1460,
	StatusIcon = "Mod/LH_Toilets/UI/Icons/Hud/st_toilet.dds",
	id = "NeedToilet",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Water",
	BuildMenuIcon = "Mod/LH_Toilets/UI/Icons/Build Menu/toilet.dds",
	BuildMenuPos = 2,
	CanPlaceInShelter = false,
	ChangeOwnerIcon = "Mod/LH_Toilets/UI/Icons/Infopanels/toilet_change_owner.dds",
	EntityHeight = 2400,
	Filter = function (obj)
		return not obj:HasUnitTag("Android")
	end,
	Health = 60000,
	LockState = "hidden",
	MaxHealth = 60000,
	OwnedComponent = true,
	PowerTemperature = 30000,
	RadiationRange = 500,
	RadiationTemperature = 30000,
	RoomPlacement = "indoors",
	SkirtSize = 0,
	attached_to_wall = true,
	attack_attraction = 5,
	construction_cost = PlaceObj('ConstructionCost', {
		Metal = 20000,
	}),
	construction_points = 20000,
	custom_constr_rules = {
		PlaceObj('ConstructionObjectSnapToWall', nil),
		PlaceObj('ConstructionCheckRoomPlacement', nil),
	},
	deconstruction_output = PlaceObj('ConstructionCost', {
		Metal = 10000,
	}),
	description = T(256874679443, --[[ModItemBuildingCompositeDef Toilet description]] "All the possible comfort private needs."),
	display_name = T(516961879347, --[[ModItemBuildingCompositeDef Toilet display_name]] "Toilet"),
	display_name_pl = T(920544763132, --[[ModItemBuildingCompositeDef Toilet display_name_pl]] "Toilets"),
	display_name_short = T(324039149076, --[[ModItemBuildingCompositeDef Toilet display_name_short]] "Toilet"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Toilet",
	group = "Water",
	id = "Toilet",
	lock_block_box = box(0, 0, 350, 900, 300, 2100),
	menu_display_name = T(779941379129, --[[ModItemBuildingCompositeDef Toilet menu_display_name]] "Toilet"),
	object_class = "IndoorToiletBuilding",
	ownership_class = "Toilet",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Water",
	BuildMenuIcon = "Mod/LH_Toilets/UI/Icons/Build Menu/latrine.dds",
	BuildMenuPos = 1,
	CanPlaceInShelter = false,
	EntityHeight = 1286,
	Health = 60000,
	MaxHealth = 60000,
	RoomPlacement = "outdoors",
	SkirtSize = 0,
	attack_attraction = 5,
	can_be_placed_on_floor = false,
	construction_cost = PlaceObj('ConstructionCost', {
		ScrapMetal = 50000,
	}),
	construction_points = 40000,
	deconstruction_output = PlaceObj('ConstructionCost', {
		ScrapMetal = 20000,
	}),
	description = T(383603256713, --[[ModItemBuildingCompositeDef Latrine description]] "Basic retreat for private needs."),
	display_name = T(657193361311, --[[ModItemBuildingCompositeDef Latrine display_name]] "Latrine"),
	display_name_pl = T(677992961325, --[[ModItemBuildingCompositeDef Latrine display_name_pl]] "Latrines"),
	display_name_short = T(682916629853, --[[ModItemBuildingCompositeDef Latrine display_name_short]] "Latrine"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Latrine",
	group = "Water",
	id = "Latrine",
	lock_block_box = box(-600, -600, 0, 600, 1500, 2100),
	menu_display_name = T(850149857028, --[[ModItemBuildingCompositeDef Latrine menu_display_name]] "Latrine"),
	object_class = "OutdoorToiletBuilding",
}),
PlaceObj('ModItemCode', {
	'name', "ToiletsScript",
	'CodeFileName', "Code/ToiletsScript.lua",
}),
PlaceObj('ModItemCode', {
	'name', "ToiletBuilding",
	'CodeFileName', "Code/ToiletBuilding.lua",
}),
PlaceObj('ModItemCode', {
	'name', "UnitToilet",
	'CodeFileName', "Code/UnitToilet.lua",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, from_transfer, ...)
		        local activity_id = self.ActivityId
		        local activity = ActivityTypes[activity_id]
		        local skill_level, efficiency = unit:GetSkillAndEfficiency(activity)
		        local activity_target = target:CreateToiletActivityObject()
		        unit:EnqueueAndTryExecuteActivity(activity_id, activity_target, skill_level, efficiency, clear_queue)
	end,
	ActivityId = "UseToilet",
	CanTransfer = function (self, old_unit, new_unit, target, param, queue_cleared)
		        return self:GetAssignedUnit(target, param)
	end,
	EnableCanceling = false,
	GetAssignedUnit = function (self, target, param)
		        return target:GetAssignedSurvivor()
	end,
	GetButtonText = function (self, unit, target, param, status_text)
		        return GetTByGender(self.Text, unit)
	end,
	GetErrorStatus = function (self, unit, target, param)
		local survivor = target:GetAssignedSurvivor()
		if IsValid(survivor) then
			return T{"used by <unit_name>", unit_name = survivor:GetDisplayNameNoStatus()}
		end
		        return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	RequireUnit = true,
	Transfer = function (self, old_unit, new_unit, target, param, queue_cleared)
		        local activity_target = target:GetAttach("ToiletActivityObject")
		        local activity_unit = activity_target.activity_unit or activity_target.enqueued
		        if activity_unit == target.assigned_unit then
		            activity_target.transferring = true
		        end
		        self:Activate(new_unit, target, param, queue_cleared)
	end,
	id = "UseToilet",
}),
PlaceObj('ModItemEntity', {
	'name', "Latrine",
	'class_parent', "AnimatedTextureObject",
	'fade_category', "Never",
	'entity_name', "Latrine",
	'material_type', "Metal",
}),
PlaceObj('ModItemEntity', {
	'name', "Toilet",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "Toilet",
	'material_type', "Metal",
}),
PlaceObj('ModItemEntity', {
	'name', "Toilet_Lid",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "Toilet_Lid",
	'material_type', "Metal",
}),
}
