local LH_Hygiene_Enabled

AppendClass.Human = {
    __parents = {"UnitToilet"}
}

function OnMsg.ModsReloaded()
    LH_Hygiene_Enabled = table.find(ModsLoaded, "id", "LH_Hygiene")
    local parent = XTemplates.tabHumanStatus_Health[2]
    if parent.toilet_added then
        return
    end
    parent[#parent + 1] = PlaceObj('XTemplateTemplate', {
        '__context', function (parent, context) return SubContext(context,
                { stat_name = T("Toilet need"),
                  stat_description = T("The urgency to find a toilet. Movement and manipulation may be affected if one is unable to take care of private needs."),
                  stat_prop_id = "ToiletNeed" })
        end,
        '__condition', function (parent, context) return not context:HasUnitTag("Android") end,
        '__template', "IPUnitStatusText",
        'RolloverAnchor', "right",
        'Id', "idToilet",
        'RolloverOnFocus', true,
        'RelativeFocusOrder', "new-line",
        'Text', T("<stat_name><right><em><IPToilet></em>"),
    })
    parent.toilet_added = true
end

DefineClass.UnitToilet = {
    __parents = { "UnitComponent" },
    properties = {
        { category = "Toilet", id = "ToiletNeed", editor = "number", scale = 1000, default = 0, min = 0, max = 100000 },
        { category = "Toilet", id = "ToiletGainPerDay", name = T("Daily gain"), editor = "number", modifiable = true, scale = 1000, default = 200000 },
        { category = "Toilet", id = "NeedToiletThreshold", name = T("Need Toilet Threshold"), editor = "number", modifiable = true, scale = 1000, default = 75000, min = 0, max = 100000 },
        use_toilet_timeout = false
    },
}

function UnitToilet:IsHygieneEnabled()
    return LH_Hygiene_Enabled
end

function UnitToilet:IsDesperatelyDoingSomething()
    return self.command == "CmdNeedToilet" or self.current_activity == "UseToilet" or self.current_activity == "NeedToilet"
end

AutoResolveMethods.ChangeToilet = "call"
function UnitToilet:ChangeToilet(toilet_need)
    if self:HasUnitTag("Android") then
        self.ToiletNeed = self.ToiletNeed
        return
    end
    local old_toilet = self.ToiletNeed
    toilet_need = Clamp(old_toilet + (toilet_need or 0), 0, 100000)
    if toilet_need ~= old_toilet then
        self.ToiletNeed = toilet_need
    end

    if self.ToiletNeed < self.NeedToiletThreshold or self:IsActivityEnqueued("UseToilet") or self:IsActivityEnqueued("NeedToilet")
            or self:IsManuallyControlled() or self:IsSleeping() or self:IsAwaitingRescue() or self:IsOnExpedition()
            or self:IsDesperatelyDoingSomething() then
        return
    end

    local current_time = GameTime()
    if self.use_toilet_timeout and (self.use_toilet_timeout > current_time) then
        return
    end
    self.use_toilet_timeout = current_time + DivRound(const.HourDuration, 4)
    if self.ToiletNeed >= 100000 then
        self:RememberCurrentActivity()
        self:ForceSetActivity("NeedToilet", FindToilet(self), nil, nil, const.CommandImportance.DirectOrder)
    elseif self.ToiletNeed > self.NeedToiletThreshold then
        self:EnqueueActivityInFront("NeedToilet", DelayedActivitySearchId)
    end
end

function UnitToilet:CmdNeedToilet(activity, toilet)
    toilet = toilet and toilet:CreateToiletActivityObject():IsActivityFree() and toilet or FindToilet(self)
    local toilet_dist = toilet and pf.GetLinearDist(self, toilet)
    local fallback = (IsPosOutside(self) or not toilet_dist or toilet_dist > 200 * guim) and FindToiletFallback(self)
    if toilet and (not fallback or pf.GetLinearDist(self, fallback) > toilet_dist) then
        self:ForceSetActivity("UseToilet", toilet:CreateToiletActivityObject(), nil, nil, const.CommandImportance.DirectOrder)
    elseif fallback then
        self:SafeGoto(fallback)
        local pants_item, pants_res
        self:PushDestructor(function()
            self:PlayMomentTrackedAnim("sit_To_Standing", 1, nil, 200)
            if pants_item and pants_res then
                self:Equip(pants_item.id, "Pants", pants_res)
            end
        end)
        pants_item, pants_res = self:Unequip("Pants")
        if self:IsHygieneEnabled() then
            self:ChangeHygiene(-10000)
        end
        self:PlayMomentTrackedAnim("standing_To_Sit", 1, nil, 200)
        Sleep(const.HourDuration / 8)
        self:ChangeToilet(-100000)
        self:PopAndCallDestructor()
        return true
    end
end

function UnitToilet:CheatToggleGodMode()
    if not self.god_mode then
        self.ChangeToilet = nil
        return
    end
    self:ChangeToilet(0)
    self.ChangeToilet = empty_func
end

function UnitToilet:CalcToilet(interval)
    if not self:IsSleeping() then
        return MulDivRound(self.ToiletGainPerDay, interval, const.DayDuration)
    end
end

function UnitToilet:OnObjUpdate(time, update_interval)
    if self.Health <= 0 then return end
    self:ChangeToilet(self:CalcToilet(update_interval))
end

function UnitToilet:GetIPToilet()
    local text = T("<percent(ToiletNeed,100000)>")
    if self.ToiletNeed >= 100000 then
        text = T("<negative>")..text..T("</negative>")
    elseif self.ToiletNeed < self.NeedToiletThreshold then
        text = T("<positive>")..text..T("</positive>")
    end
    return text
end