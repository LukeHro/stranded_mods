DefineClass.ToiletBuilding = {
    __parents = { "CompositeBuilding" },
    lid = false,
}

function ToiletBuilding:LidUp(time)
    if IsValid(self.lid) then
        self.lid:SetAngle(-5500, time or 0)
    end
end

function ToiletBuilding:LidDown(time)
    if IsValid(self.lid) then
        self.lid:SetAngle(0, time or 0)
    end
end

function ToiletBuilding:CreateToiletActivityObject()
    if not IsValid(self) or IsBeingDestructed(self) then
        return
    end
    local obj = self:GetAttach("ToiletActivityObject")
    if IsValid(obj) then
        return obj
    end
    obj = CreateActivityObject("ToiletActivityObject", {
        player = self.player
    }, self)
    obj:SetActivityAvailable(true)
    return obj
end

function ToiletBuilding:RemoveToiletActivityObject()
    local obj = self:GetAttach("ToiletActivityObject")
    if IsValid(obj) then
        obj:delete()
    end
end

function ToiletBuilding:GatherDirectOrders(unit, orders)
    if unit and not unit:HasUnitTag("Android") and self:CanWork() then
        CreateDirectOrderButtons(orders, DirectOrders.UseToilet, self)
    end
end

function ToiletBuilding:GetAssignedSurvivor()
    local activity = self:GetAttach("ToiletActivityObject")
    return activity and activity.activity_unit
end

DefineClass.ToiletActivityObject = {
    __parents = {
        "ActivityObject"
    },
    activity_id = "UseToilet",
    retry_time = false,
}

function ToiletActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

local function AvoidNoClothesStatus(unit, id)
    if id == "ClothesMissing" then
        return true
    end
    return StatusEffectsObject.FirstEffectById(unit, id)
end

function ToiletActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local toilet = self:GetActivityTarget()
    local unit_orig_scale = unit:GetScale()
    local pants_item, pants_res
    if not toilet then
        return
    end
    unit:PushDestructor(function()
        unit:SetScale(unit_orig_scale, unit:GetAnimDuration("sit_To_Standing"))
        unit:PlayMomentTrackedAnim("sit_To_Standing", 1, nil, 200)
        if pants_item and pants_res then
            unit:Equip(pants_item.id, "Pants", pants_res)
        end
        unit.FirstEffectById = nil
        if IsValid(toilet) then
            toilet:LidDown(100)
        end
    end)
    unit.FirstEffectById = AvoidNoClothesStatus
    toilet:LidUp(100)
    pants_item, pants_res = unit:Unequip("Pants")
    if unit:IsHygieneEnabled() then
        unit:ChangeHygiene(-5000)
    end
    local anim_time = unit:GetAnimDuration("standing_To_Sit")
    unit:SetScale(100, anim_time)
    unit:PlayMomentTrackedAnim("standing_To_Sit", 1, nil, 200)

    unit:SetAngle(unit:GetVisualAngle() + 10800, 0)
    unit:SetPos(toilet:GetSpotLocPos(toilet:GetRandomSpot("Sit")))

    anim_time = unit:GetAnimDuration("sit_Idle")
    local duration = const.HourDuration / 8
    while duration > 0 do
        unit:PlayMomentTrackedAnim("sit_Idle", 1, nil, 0, Min(duration, anim_time))
        duration = duration - anim_time
        if unit.visit_restart then unit:PopAndCallDestructor() return end
    end
    unit:ChangeToilet(-100000)
    if unit:IsHygieneEnabled() then
        local sink = MapFindNearest(unit, unit, 100 * guim, "SinkBuilding", nil, nil, function(obj)
            return obj.working and obj:CreateWashSinkActivityObject():IsActivityFree() and CheckHygieneBuildingAccess(unit, obj)
        end)
        local pos = unit:GetPos()
        local dist = sink and unit:GetDist2D(sink)
        if unit:IsOutside() then
            local pump = MapFindNearest(unit, unit, 100 * guim, "HandPumpBuilding", nil, nil, function(obj)
                return obj.working and (not sink or IsCloser2D(obj:GetPos(), pos, dist)) and not obj:GetAssignedSurvivor()
                        and obj:CreateWashSinkActivityObject():IsActivityFree() and CheckHygieneBuildingAccess(unit, obj)
            end)
            sink = pump or sink
        end
        if sink then
            unit:EnqueueActivityInFront("WashSink", sink:CreateWashSinkActivityObject())
        end
    end
    unit:PopAndCallDestructor()
    self:delete()
    return true
end

function ToiletActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    if unit:HasUnitTag("Android") then
        return false
    end
    if direct_order then
        return true
    end
    if (self.retry_time or 0) > GameTime() then
        return
    end
    local target = self:GetActivityTarget()
    if not CheckHygieneBuildingAccess(unit, target) then
        return
    end
    local area_is_locked = not target:IsSafeForApproaching(unit)
    if area_is_locked then
        self.retry_time = GameTime() + const.HourDuration
        return
    end
    return unit.ToiletNeed >= unit.NeedToiletThreshold
end

function ToiletActivityObject:Done()
end

DefineClass.OutdoorToiletBuilding = {
    __parents = { "ToiletBuilding" },
    lid = false,
}

DefineClass.IndoorToiletBuilding = {
    __parents = { "ToiletBuilding" },
    lid = false,
}

function IndoorToiletBuilding:Init()
    self.lid = self.lid or PlaceObject("Toilet_Lid", nil, const.cofComponentAttach)
    local lid_spot = self:GetRandomSpot("Lid")
    if lid_spot then
        self:Attach(self.lid, lid_spot)
        self.lid:SetAxis(axis_y)
    end
end

function IndoorToiletBuilding:Done()
    if IsValid(self.lid) then
        DoneObject(self.lid)
    end
end
