local function ShowError()
    if not table.find(ModsLoaded, "id", "LH_Water") then
        CreateRealTimeThread(CreateMessageBox, nil, T("Missing dependency!"),
                T("Toilets mod now requires Water mod.\nPlease subscribe to that it before continuing playing!"), T("OK"))
    end
end

OnMsg.GameTimeStart = ShowError
OnMsg.PostLoadGame = ShowError

PlaceObj('RoomRole', {
    Prerequisites = {
        PlaceObj('CheckFurniture', {Furniture = {"Toilet",},}),
        PlaceObj('CheckFurniture', {Furniture = {"Shower",},Negate = true,}),
        PlaceObj('CheckFurniture', {Furniture = {"Table",},Negate = true,}),
        PlaceObj('CheckFurniture', {Furniture = {"Bed",},Negate = true,}),
    },
    SortKey = 3200,
    display_name = T("Toilet"),
    group = "Default",
    id = "Toilet",
})

function FindToilet(unit, range)
    local toilet = unit:GetOwnedObject("Toilet")
    toilet = toilet and toilet.working and toilet:CreateToiletActivityObject():IsActivityFree() and CheckHygieneBuildingAccess(unit, toilet)
    if not toilet and unit.room then
        local rooms = {}
        unit.room:GatherAllNeighbourhoodRooms(rooms)
        if next(rooms) then
            toilet = MapFindNearest(unit, unit, range or 500 *guim, "IndoorToiletBuilding", nil, nil, function(obj)
                return obj.working and obj.room and table.find(rooms, obj.room)
                        and obj:CreateToiletActivityObject():IsActivityFree() and CheckHygieneBuildingAccess(unit, obj)
            end)
        end
    end
    return toilet or MapFindNearest(unit, unit, range or 500 *guim, "ToiletBuilding", nil, nil, function(obj)
        return obj.working and obj:CreateToiletActivityObject():IsActivityFree() and CheckHygieneBuildingAccess(unit, obj)
    end)
end

function FindToiletFallback(unit)
    local pos = terrain.FindReachable(unit, const.tfrRandom, unit:RandSeed("Toilet"), const.tfrLimitDist, 20 * guim, guim) or unit:GetPos()
    local loc = MapFindNearest(pos, pos, 20 * guim, "Bush") or MapFindNearest(pos, pos, 20 * guim, "Tree")
            or MapFindNearest(unit, unit, 40 * guim, "Bush") or MapFindNearest(unit, unit, 40 * guim, "Tree")
    local fallback = loc and terrain.FindReachable(loc, const.tfrRandom, unit:RandSeed("Toilet"), const.tfrLimitDist, guim, guim / 2)
    return fallback and IsPosOutside(fallback) and fallback
end