DefineClass.CheckTool = {
    __parents = {"Condition"},
    __generated_by_class = "ConditionDef",
    properties = {
        {
            id = "Negate",
            name = "Negate",
            editor = "bool",
            default = false
        },
        {
            id = "Tool",
            name = "Tool",
            editor = "preset_id",
            default = false,
            preset_class = "ToolResource"
        },
        {--"Positive" cooldown check very nasty invention
            id = "SkipCooldownCheck",
            name = "SkipCooldownCheck",
            editor = "bool",
            default = false
        }
    },
    EditorView = Untranslated("Has <Tool>"),
    EditorViewNeg = Untranslated("Hasn't got <Tool>"),
    Documentation = "Checks if the human has a specific tool."
}

function CheckTool:__eval(obj, context)
    return obj and obj:HasValidSurvivalTool(self.Tool)
end

function CheckTool:GetError()
    return --no such tool?
end

function OnMsg.ModsReloaded()
    local story_bit = Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip
    if story_bit.lh_changed then
        return --don't make changes twice on game reload
    end
    story_bit.lh_changed = true
    --add mod follow ups to the first possible outcome of the Site_Crash_CruiseShip expedition
    for _, outcome in ipairs(story_bit) do
        if outcome:IsKindOf("StoryBitOutcome") then
            outcome.StoryBits = outcome.StoryBits or {}
            table.insert_unique(outcome.StoryBits,
                    PlaceObj('StoryBitWithWeight', {
                        'StoryBitId', "Site_Crash_CruiseShip_Survivor_Mask",
                        'NoCooldown', true,
                    })
            )
            table.insert_unique(outcome.StoryBits,
                    PlaceObj('StoryBitWithWeight', {
                        'StoryBitId', "Site_Crash_CruiseShip_NoSurvivor_Mask",
                        'NoCooldown', true,
                    })
            )
            table.insert_unique(outcome.StoryBits,
                    PlaceObj('StoryBitWithWeight', {
                        'StoryBitId', "Site_Crash_CruiseShip_Resources_Mask",
                        'NoCooldown', true,
                    })
            )
            break
        end
    end
    --add condition to vanilla follow ups so they won't be activated if the survivor has RespiratorMask
    LH_AddPrerequisiteToStoryBit(Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip_Injury,
            PlaceObj('CheckTool', { Tool = "RespiratorMask", Negate = true }))
    LH_AddPrerequisiteToStoryBit(Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip_Survivor,
            PlaceObj('CheckTool', { Tool = "RespiratorMask", Negate = true }))
    LH_AddPrerequisiteToStoryBit(Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip_NoSurvivors,
            PlaceObj('CheckTool', { Tool = "RespiratorMask", Negate = true }))
    LH_AddPrerequisiteToStoryBit(Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip_Resources,
            PlaceObj('CheckTool', { Tool = "RespiratorMask", Negate = true }))
    --add condition to mod added follow ups so they will be activated if the survivor has RespiratorMask
    LH_AddPrerequisiteToStoryBit(Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip_Survivor_Mask,
            PlaceObj('CheckTool', { Tool = "RespiratorMask", }))
    LH_AddPrerequisiteToStoryBit(Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip_NoSurvivors_Mask,
            PlaceObj('CheckTool', { Tool = "RespiratorMask", }))
    LH_AddPrerequisiteToStoryBit(Presets.StoryBit.Expedition_FollowUP.Site_Crash_CruiseShip_Resources_Mask,
            PlaceObj('CheckTool', { Tool = "RespiratorMask", }))
end

function LH_AddPrerequisiteToStoryBit(story_bit, condition)
    if not story_bit then
        return
    end
    story_bit.Prerequisites = story_bit.Prerequisites or {}
    table.insert_unique(story_bit.Prerequisites, condition)
end