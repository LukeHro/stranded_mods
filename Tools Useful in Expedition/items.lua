return {
PlaceObj('ModItemCode', {
	'CodeFileName', "Code/Script.lua",
}),
PlaceObj('ModItemStoryBit', {
	Category = "Exploration",
	Enabled = true,
	FxAction = "UINotificationExpedition",
	HasNotification = false,
	OneTime = false,
	PopupFxAction = "MessagePopup",
	Prerequisites = {
		PlaceObj('CheckTool', {
			Tool = "RespiratorMask",
			param_bindings = false,
		}),
	},
	ScriptDone = true,
	SelectObject = false,
	Sets = set( "Negative" ),
	Text = T(878519696726, --[[ModItemStoryBit Site_Crash_CruiseShip_NoSurvivors_Mask Text]] "My respirator mask helped me cope with the smoke as I was making my way around the crash, looking for survivors.\n\nI did the best I could protected like that and kept looking for the longest time, but I didn't find anyone. I'll be heading back now."),
	Title = T(815183296862, --[[ModItemStoryBit Site_Crash_CruiseShip_NoSurvivors_Mask Title]] "At the crash site"),
	UseObjectImage = true,
	group = "Expedition_FollowUP",
	id = "Site_Crash_CruiseShip_NoSurvivors_Mask",
	max_reply_id = 4,
	qa_info = PlaceObj('PresetQAInfo', {
		Log = "Modified by Lina on 2021-Jan-07\nModified by Gaby on 2021-Jan-08\nModified by Lina on 2021-Jan-12\nModified by Ivan on 2021-Feb-11\nModified by Bobby on 2021-May-20\nModified by Lina on 2021-Aug-25\nModified by Gaby on 2021-Sep-03\nModified by Lina on 2021-Sep-03\nModified by Lina on 2021-Sep-08\nModified by Lina on 2021-Sep-20\nModified by Lina on 2021-Sep-27\nModified by Lina on 2021-Oct-20\nModified by Lina on 2021-Dec-17\nModified by Lina on 2022-Feb-04\nModified by Xaerial on 2022-Oct-10",
		param_bindings = false,
	}),
}),
PlaceObj('ModItemStoryBit', {
	ActivationEffects = {
		PlaceObj('GiveExpeditionRewardToSurvivor', {
			Amount = 20000,
			Resource = "MedKit",
			param_bindings = false,
		}),
		PlaceObj('GiveExpeditionRewardToSurvivor', {
			Amount = 5000,
			Resource = "MedBandages",
			param_bindings = false,
		}),
		PlaceObj('GiveExpeditionRewardToSurvivor', {
			Amount = 5000,
			Resource = "MedAntibiotics",
			param_bindings = false,
		}),
	},
	Category = "Exploration",
	Enabled = true,
	FxAction = "UINotificationExpedition",
	HasNotification = false,
	OneTime = false,
	PopupFxAction = "MessagePopup",
	Prerequisites = {
		PlaceObj('CheckTool', {
			Tool = "RespiratorMask",
			param_bindings = false,
		}),
	},
	ScriptDone = true,
	SelectObject = false,
	Text = T(829566883225, --[[ModItemStoryBit Site_Crash_CruiseShip_Resources_Mask Text]] "My respirator mask helped me cope with the smoke as I was making my way around the crash, looking for survivors.\n\nI did the best I could protected like that and kept looking for the longest time, but I didn't find anyone.\n\nI was going back to the balloon when I tripped on something which turned out to be a first aid box."),
	Title = T(319763022510, --[[ModItemStoryBit Site_Crash_CruiseShip_Resources_Mask Title]] "At the crash site"),
	UseObjectImage = true,
	group = "Expedition_FollowUP",
	id = "Site_Crash_CruiseShip_Resources_Mask",
	max_reply_id = 4,
	qa_info = PlaceObj('PresetQAInfo', {
		Log = "Modified by Lina on 2021-Jan-07\nModified by Gaby on 2021-Jan-08\nModified by Lina on 2021-Jan-12\nModified by Ivan on 2021-Feb-11\nModified by Bobby on 2021-May-20\nModified by Lina on 2021-Aug-25\nModified by Gaby on 2021-Sep-03\nModified by Lina on 2021-Sep-03\nModified by Lina on 2021-Sep-08\nModified by Lina on 2021-Sep-20\nModified by Lina on 2021-Sep-27\nModified by Lina on 2021-Oct-20\nModified by Lina on 2021-Dec-17\nModified by Lina on 2022-Feb-04\nModified by Lina on 2022-Apr-05\nModified by Xaerial on 2022-Oct-10",
		param_bindings = false,
	}),
	PlaceObj('StoryBitReply', {
		Text = T(611804236855, --[[ModItemStoryBit Site_Crash_CruiseShip_Resources_Mask Text]] "Get whatever is inside that box and come back."),
		param_bindings = false,
		unique_id = 4,
	}),
}),
PlaceObj('ModItemStoryBit', {
	ActivationEffects = {
		PlaceObj('PickCharacterDef', {
			param_bindings = false,
		}),
	},
	AllowedInScenarios = set( "CrashLanding" ),
	Category = "Exploration",
	Comment = "crash",
	Enabled = true,
	FxAction = "UINotificationExpedition",
	HasNotification = false,
	OneTime = false,
	PopupFxAction = "MessagePopup",
	Prerequisites = {
		PlaceObj('CheckTool', {
			Tool = "RespiratorMask",
			param_bindings = false,
		}),
	},
	ScriptDone = true,
	SelectObject = false,
	Sets = set( "IncreasePop" ),
	Text = T(987784013528, --[[ModItemStoryBit Site_Crash_CruiseShip_Survivor_Mask Text]] "My respirator mask helped me cope with the smoke as I was making my way around the crash, looking for survivors.\n\nI did the best I could protected like that and kept looking for the longest time, but I didn't find anyone.\n\nAs I was heading back, I saw a figure crawling a little ways off the crash!"),
	Title = T(382323615180, --[[ModItemStoryBit Site_Crash_CruiseShip_Survivor_Mask Title]] "At the crash site"),
	UseObjectImage = true,
	group = "Expedition_FollowUP",
	id = "Site_Crash_CruiseShip_Survivor_Mask",
	max_reply_id = 6,
	qa_info = PlaceObj('PresetQAInfo', {
		Log = "Modified by Lina on 2021-Jan-07\nModified by Gaby on 2021-Jan-08\nModified by Lina on 2021-Jan-12\nModified by Ivan on 2021-Feb-11\nModified by Bobby on 2021-May-20\nModified by Lina on 2021-Aug-25\nModified by Gaby on 2021-Sep-03\nModified by Lina on 2021-Sep-03\nModified by Lina on 2021-Sep-08\nModified by Lina on 2021-Sep-20\nModified by Lina on 2021-Sep-27\nModified by Lina on 2021-Oct-20\nModified by Lina on 2021-Dec-17\nModified by Lina on 2022-Feb-04\nModified by Lina on 2022-Mar-23\nModified by Lina on 2022-Apr-05\nModified by Ivan on 2022-Jun-10\nModified by Ivan on 2022-Jul-29\nModified by Xaerial on 2022-Oct-10\nModified by Ivan on 2022-Nov-28",
		param_bindings = false,
	}),
	PlaceObj('StoryBitReply', {
		CustomOutcomeText = T(690514886226, --[[ModItemStoryBit Site_Crash_CruiseShip_Survivor_Mask CustomOutcomeText]] "<character_def.FirstName> joins; +18 Happiness"),
		OutcomeText = "custom",
		Text = T(666032992972, --[[ModItemStoryBit Site_Crash_CruiseShip_Survivor_Mask Text]] "Take that person back with you."),
		param_bindings = false,
		unique_id = 5,
	}),
	PlaceObj('StoryBitOutcome', {
		Effects = {
			PlaceObj('ForEachExecuteEffects', {
				Effects = {
					PlaceObj('AddRemoveHappinessFactor', {
						HappinessFactor = "NewSurvivor_Accepted",
						param_bindings = false,
					}),
				},
				Label = "Survivors",
				param_bindings = false,
			}),
			PlaceObj('SpawnSurvivor', {
				BoardBalloon = true,
				FollowLeader = true,
				param_bindings = false,
			}),
			PlaceObj('ExecuteCode', {
				Code = function (self, obj)
					Msg("NewSurvivorAccepted")
				end,
				param_bindings = false,
			}),
		},
		param_bindings = false,
	}),
	PlaceObj('StoryBitReply', {
		CustomOutcomeText = T(453779956024, --[[ModItemStoryBit Site_Crash_CruiseShip_Survivor_Mask CustomOutcomeText]] "-10 Happiness"),
		OutcomeText = "custom",
		Text = T(653685848349, --[[ModItemStoryBit Site_Crash_CruiseShip_Survivor_Mask Text]] "Leave them behind."),
		param_bindings = false,
		unique_id = 6,
	}),
	PlaceObj('StoryBitOutcome', {
		Effects = {
			PlaceObj('ForEachExecuteEffects', {
				Effects = {
					PlaceObj('AddRemoveHappinessFactor', {
						HappinessFactor = "NewSurvivor_Rejected",
						param_bindings = false,
					}),
				},
				Label = "Survivors",
				param_bindings = false,
			}),
			PlaceObj('ExecuteCode', {
				Code = function (self, obj)
					Msg("NewSurvivorDismissed")
				end,
				param_bindings = false,
			}),
		},
		param_bindings = false,
	}),
}),
}
