function ReplaceGrainOnRecipe(recipe, resource)
    if Recipes[recipe] then
        local grain = table.find_value(Recipes[recipe].InputResources, 'resource', "Grain")
        if grain then
            grain.resource = resource or "Flour"
        end
    end
end

function OnMsg:ModsReloaded()
    ReplaceGrainOnRecipe("Bread")
    ReplaceGrainOnRecipe("Cake")
    ReplaceGrainOnRecipe("MushroomCroquettes")
    ReplaceGrainOnRecipe("PiePumpkin")
    ReplaceGrainOnRecipe("PizzaVegetable")
    ReplaceGrainOnRecipe("PizzaVegetable_Desertum")
    ReplaceGrainOnRecipe("SpaghettiInsect")
    ReplaceGrainOnRecipe("SpaghettiInsect_Desertum")
    ReplaceGrainOnRecipe("MacaroniCheese")

    if table.find(ModsLoaded, "id", "LH_Water") then
        AddWaterToRecipe("AlcoholBeer_Seeds")
        AddWaterToRecipe("Coffee_Seeds")
    end

    if table.find(ModsLoaded, "id", "LH_Water") then
        AddWaterToRecipe("AlcoholBeer_Seeds")
        AddWaterToRecipe("Coffee_Seeds")
    end

    if Plants["FrostTree"] then
        local grain = table.find_value(Plants["FrostTree"].HarvestResources, 'resource', "Grain")
        if grain then
            grain.resource = "DrySeeds"
        end
    end
    PlantsReloadDefs(Plants)
end

function OnMsg.ModsReloaded()
    local index = 0
    index = table.findfirst(Presets.StoryBit.Expedition_FollowUP.Site_Scavenge_ChoiceConsumables, function (_, entry)
        if entry.Effects and entry.Effects[1].Resource == "Grain" then print(entry.Effects)
            return true
        end end)
    if index  and index > 0 then
        Presets.StoryBit.Expedition_FollowUP.Site_Scavenge_ChoiceConsumables[index].Prerequisites =
                {PlaceObj('CheckRegion', { Negate = true, Region = set( "Alba" ) })}
        local seeds_outcome = PlaceObj('StoryBitOutcome', {
            Effects = {PlaceObj('GiveExpeditionRewardToSurvivor', { Amount = 200000, Resource = "DrySeeds" })},
            Prerequisites = {PlaceObj('CheckRegion', { Region = set( "Alba" ) })}
        })
        table.insert(Presets.StoryBit.Expedition_FollowUP.Site_Scavenge_ChoiceConsumables, index + 1, seeds_outcome)
    end
    index = table.findfirst(Presets.StoryBit.Expedition_FollowUP.Site_Crash_Cargo, function (_, entry)
        if entry.Effects and entry.Effects[1].Resource == "Grain" then print(entry.Effects)
            return true
        end end)
    if index  and index > 0 then
        Presets.StoryBit.Expedition_FollowUP.Site_Crash_Cargo[index].Prerequisites =
        {PlaceObj('CheckRegion', { Negate = true, Region = set( "Alba" ) })}
        local seeds_outcome = PlaceObj('StoryBitOutcome', {
            Effects = {PlaceObj('GiveExpeditionRewardToSurvivor', { Amount = 200000, Resource = "DrySeeds" })},
            Prerequisites = {PlaceObj('CheckRegion', { Region = set( "Alba" ) })}
        })
        table.insert(Presets.StoryBit.Expedition_FollowUP.Site_Crash_Cargo, index + 1, seeds_outcome)
    end
end

function OnMsg.GenerateLoot(_, items)
    if items and Game.region == "Alba" then
        for _, entry in pairs(items) do
            table.replace(entry, "Grain", "DrySeeds")
        end
    end
end