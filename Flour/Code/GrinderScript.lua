local _slots = {
    {
        adjust_z = false,
        flags_missing = 1,
        goto_spot = "LeadToSpot",
        groups = {
            ["A"] = true,
        },
        spots = {
            "Grinderinterface",
        },
    },
}
PrgAmbientLife["FlourGrinder"] = function(unit, bld)
    local _spot, _obj, _slot_desc, _slot, _slotname
    _spot, _obj, _slot_desc, _slot, _slotname = PrgGetObjRandomSpotFromGroup(bld, nil, "A", _slots, unit)
    if _spot then
        PrgVisitSlot(unit, bld, _obj, _spot, _slot_desc, _slot, _slotname, nil, nil)
        if unit.visit_restart then return end
    else
        PrgVisitHolder(unit, bld)
        if unit.visit_restart then return end
    end
end

PrgAmbientLife["Grinderinterface"] = function(unit, bld, obj, spot, slot_data, slot, slotname)
    local unit_orig_scale
    bld:SetStateText("start", 0, 200)
    local grains = PlaceObject("ResourceBulk_Wheat", nil, const.cofComponentAttach)
    grains:SetScale(60)
    NetTempObject(grains)
    bld:Attach(grains, bld:GetRandomSpot("Grains"))
    unit:PushDestructor(function(unit)
        bld:SetStateText("end", 0, 200)
        unit:SetScale(unit_orig_scale, 200)
        if IsValid(grains) then
            DoneObject(grains)
        end
    end)
    unit_orig_scale = unit:GetScale()
    unit:SetScale(100, 200)
    unit:Face(bld:GetSpotPos(bld:GetSpotBeginIndex("Face")), 200)
    while unit:VisitTimeLeft() > 0 do
        bld:SetStateText("working", 0, 200)
        local duration = Min(unit:VisitTimeLeft(), 1 * unit:GetAnimDuration("bend_Cut_Tool"))
        unit:PlayMomentTrackedAnim("bend_Cut_Tool", 1, nil, 200, duration)
        if unit.visit_restart then return end
    end
    unit:PopAndCallDestructor()
end