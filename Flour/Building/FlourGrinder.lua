UndefineClass('FlourGrinder')
DefineClass.FlourGrinder = {
	__parents = { "Building", "OwnedComponent", "ProductionDeviceComponent" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "Building",
	LockState = "hidden",
	unload_anim_hands = "standing_DropDown_Hands_High",
	load_anim_hands = "standing_PickUp_Hands_High",
	BuildMenuCategory = "Devices",
	display_name = T(520935748965, --[[ModItemBuildingCompositeDef FlourGrinder display_name]] "Flour grinder"),
	description = T(254659170153, --[[ModItemBuildingCompositeDef FlourGrinder description]] "Used for grinding grains or other dried seeds to obtain flour.\n\nProcesses <color TextEmphasis>Grain</color> <image 'UI/Icons/Resources/res_grains' 1100> into <color TextEmphasis>Flour</color> <image 'Mod/LH_Flour/UI/Icons/Resources/res_flour' 1100>."),
	menu_display_name = T(640471737858, --[[ModItemBuildingCompositeDef FlourGrinder menu_display_name]] "Flour grinder"),
	BuildMenuIcon = "Mod/LH_Flour/UI/Icons/Build Menu/flour_grinder.dds",
	BuildMenuPos = 11,
	display_name_pl = T(238062296837, --[[ModItemBuildingCompositeDef FlourGrinder display_name_pl]] "Flour grinders"),
	entity = "Grinder",
	labels = {
		"BerserkTargets",
	},
	update_interval = 5000,
	construction_cost = PlaceObj('ConstructionCost', {
		Stone = 10000,
	}),
	construction_points = 15000,
	Health = 120000,
	MaxHealth = 120000,
	upgrade_label = "FlourGrinder",
	lock_block_box = box(-300, -300, 0, 300, 300, 700),
	SkirtSize = 238,
	EntityHeight = 2379,
	CustomMaterial = "Wood",
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	OwnedComponent = true,
	ProductionDeviceComponent = true,
	interfaces = {
		"GrinderInterface",
	},
	ProductionDeviceSkipsStateChange = true,
}

