return {
PlaceObj('ModItemDeviceInterface', {
	id = "GrinderInterface",
	prg_name = "FlourGrinder",
	spot_name = "Grinderinterface",
}),
PlaceObj('ModItemBuildCategory', {
	ActionText = T(387279184531, --[[ModItemBuildCategory Default Grind ActionText |gender-variants]] "Grind"),
	Description = T(771868338375, --[[ModItemBuildCategory Default Grind Description]] "Task the survivors to grind raw materials."),
	DisplayName = T(704250228227, --[[ModItemBuildCategory Default Grind DisplayName]] "Grind"),
	Icon = "UI/Icons/Infopanels/device_press",
	SortKey = 1210,
	id = "Grind",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Devices",
	BuildMenuIcon = "Mod/LH_Flour/UI/Icons/Build Menu/flour_grinder.dds",
	BuildMenuPos = 11,
	CustomMaterial = "Wood",
	EntityHeight = 2379,
	Health = 120000,
	LockState = "hidden",
	MaxHealth = 120000,
	OwnedComponent = true,
	ProductionDeviceComponent = true,
	ProductionDeviceSkipsStateChange = true,
	SkirtSize = 238,
	attack_attraction = 5,
	construction_cost = PlaceObj('ConstructionCost', {
		Stone = 10000,
	}),
	construction_points = 15000,
	description = T(254659170153, --[[ModItemBuildingCompositeDef FlourGrinder description]] "Used for grinding grains or other dried seeds to obtain flour.\n\nProcesses <color TextEmphasis>Grain</color> <image 'UI/Icons/Resources/res_grains' 1100> into <color TextEmphasis>Flour</color> <image 'Mod/LH_Flour/UI/Icons/Resources/res_flour' 1100>."),
	display_name = T(520935748965, --[[ModItemBuildingCompositeDef FlourGrinder display_name]] "Flour grinder"),
	display_name_pl = T(238062296837, --[[ModItemBuildingCompositeDef FlourGrinder display_name_pl]] "Flour grinders"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "Grinder",
	group = "Devices",
	id = "FlourGrinder",
	interfaces = {
		"GrinderInterface",
	},
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands_High",
	lock_block_box = box(-300, -300, 0, 300, 300, 700),
	menu_display_name = T(640471737858, --[[ModItemBuildingCompositeDef FlourGrinder menu_display_name]] "Flour grinder"),
	object_class = "Building",
	unload_anim_hands = "standing_DropDown_Hands_High",
	update_interval = 5000,
	upgrade_label = "FlourGrinder",
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Devices",
	},
	Description = T(922952191058, --[[ModItemTech FlourGrinding Description]] "An ancient method for obtaining flour by grinding grains between two rocks. Primitive yet efficient.\n\n<style TechSubtitleBlue>Unlocks</style>\n<tabulator><em>Flour</em>: <recipe_cost('GrindedFlour')>\n<tabulator><em>Flour grinder</em>: <building_cost('FlourGrinder')>"),
	DisplayName = T(487028010732, --[[ModItemTech FlourGrinding DisplayName]] "Grains grinding"),
	Icon = "Mod/LH_Flour/UI/Icons/Research/flour_grinder.dds",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "Grain",
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Negate = true,
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "Grain",
				}),
			},
		}),
	},
	ResearchPoints = 12000,
	SortKey = 10,
	group = "Resources",
	id = "FlourGrinding",
	money_value = 12500000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "FlourGrinder",
	}),
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Devices",
	},
	Description = T(933200265694, --[[ModItemTech FlourGrinding_Seeds Description]] "Not only grains have nutrient properties. By grinding some local dry seeds we can obtain a flour just as good for cooking various dishes.\n\n<style TechSubtitleBlue>Unlocks</style>\n<tabulator><em>Flour</em>: <recipe_cost('GrindedSeeds')>\n<tabulator><em>Flour grinder</em>: <building_cost('FlourGrinder')>"),
	DisplayName = T(585266888412, --[[ModItemTech FlourGrinding_Seeds DisplayName]] "Seeds grinding"),
	Icon = "Mod/LH_Flour/UI/Icons/Research/flour_grinder.dds",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "DrySeeds",
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "DrySeeds",
				}),
			},
		}),
	},
	ResearchPoints = 12000,
	SortKey = 10,
	TradePrerequisites = {
		PlaceObj('CheckRegion', {
			Region = set( "Alba" ),
		}),
	},
	group = "Resources",
	id = "FlourGrinding_Seeds",
	money_value = 12500000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "FlourGrinder",
	}),
}),
PlaceObj('ModItemCode', {
	'name', "FlourScript",
	'CodeFileName', "Code/FlourScript.lua",
}),
PlaceObj('ModItemCode', {
	'name', "GrinderScript",
	'CodeFileName', "Code/GrinderScript.lua",
}),
PlaceObj('ModItemEntity', {
	'name', "Flour",
	'class_parent', "ResourceEntityClass",
	'fade_category', "Never",
	'ClassParents', {
		"ResourceEntityClass",
	},
	'entity_name', "Flour",
}),
PlaceObj('ModItemEntity', {
	'name', "Grinder",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'ClassParents', {
		"AutoAttachObject",
	},
	'entity_name', "Grinder",
}),
PlaceObj('ModItemRecipe', {
	Activity = "Crafting",
	ActivityDuration = 20000,
	ActivityXPGrade = "Crafting_Low",
	BuildCategory = "Grind",
	Description = T(953297554688, --[[ModItemRecipe GrindedFlour Description]] "Grind grain into flour."),
	DisplayName = T(572947742092, --[[ModItemRecipe GrindedFlour DisplayName]] "Flour"),
	FailChance = 10,
	GuaranteedSuccessLevel = 3,
	Icon = "Mod/LH_Flour/UI/Icons/Items/flour.dds",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "Grain",
			'amount', 10000,
		}),
	},
	MakeUntilMultiplier = 10,
	ManualWork = true,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "Flour",
			'amount', 10000,
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Negate = true,
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "Grain",
				}),
			},
		}),
	},
	RequiredDeviceInterfaces = {
		"GrinderInterface",
	},
	SortKey = 10,
	id = "GrindedFlour",
}),
PlaceObj('ModItemRecipe', {
	Activity = "Crafting",
	ActivityDuration = 20000,
	ActivityXPGrade = "Crafting_Low",
	BuildCategory = "Grind",
	Description = T(302366376610, --[[ModItemRecipe GrindedSeeds Description]] "Grind seeds into flour."),
	DisplayName = T(316435454696, --[[ModItemRecipe GrindedSeeds DisplayName]] "Flour"),
	FailChance = 10,
	GuaranteedSuccessLevel = 3,
	Icon = "Mod/LH_Flour/UI/Icons/Items/flour.dds",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "DrySeeds",
			'amount', 10000,
		}),
	},
	MakeUntilMultiplier = 10,
	ManualWork = true,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "Flour",
			'amount', 10000,
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "DrySeeds",
				}),
			},
		}),
	},
	RequiredDeviceInterfaces = {
		"GrinderInterface",
	},
	SortKey = 10,
	id = "GrindedSeeds",
}),
PlaceObj('ModItemResource', {
	ConsumerTags = set( "Human" ),
	DecayRateInside = 87,
	DecayRateOutside = 231,
	DecayRateRefrigerated = 43,
	DecayTimeInside = 46080000,
	DecayTimeOutside = 17280000,
	DecayTimeRefrigerated = 92160000,
	MinStorageCondition = 4,
	ShowDiscoveryNotification = false,
	SortKey = 212,
	alt_icon = "Mod/LH_Flour/UI/Icons/Items/flour.dds",
	anim_pile_load = "standing_PickUp_HandsClose",
	anim_pile_unload = "standing_DropDown_HandsClose_High",
	carry_amount = 200000,
	carry_entity = "Flour",
	carry_spot = "Tool",
	carry_type = "HandsClose",
	decay_fx = true,
	description = T(540068911478, --[[ModItemResource Flour description]] "Flower obtained from grinding grains or other dried seeds."),
	display_name = T(944847988130, --[[ModItemResource Flour display_name]] "Flour"),
	food_entity = "Meal_02",
	group = "FoodRaw",
	happiness_factor = "AteRawFood",
	health_condition = "Poisoning_1_Mild",
	health_condition_chance = 5,
	icon = "Mod/LH_Flour/UI/Icons/Resources/res_flour.dds",
	id = "Flour",
	in_groups = {
		"Food",
		"FoodRaw",
		"FoodRaw_NoGroups",
		"FoodEdible",
	},
	money_value = 20000,
	preference = -25,
	progress = 200,
	serving_amount = 10000,
	serving_nutrition = 1500000,
	stack_entity = "Flour",
	stack_idle_state_count = 3,
	stack_size = 100000,
	trade_amount_multiplier = 10000,
	use_unfinished_item = "UnfinishedFood",
	vegetarian = true,
	visible = false,
}),
PlaceObj('ModItemResource', {
	ConsumerTags = set( "Animal", "Human" ),
	DecayRateInside = 87,
	DecayRateOutside = 231,
	DecayRateRefrigerated = 43,
	DecayTimeInside = 46080000,
	DecayTimeOutside = 17280000,
	DecayTimeRefrigerated = 92160000,
	LockState = "hidden",
	MinStorageCondition = 4,
	ShowDiscoveryNotification = false,
	SortKey = 212,
	alt_icon = "UI/Icons/Items/grains",
	anim_pile_load = "standing_PickUp_HandsClose",
	anim_pile_unload = "standing_DropDown_HandsClose_High",
	carry_amount = 200000,
	carry_entity = "ResourceBulk_Wheat",
	carry_spot = "Tool",
	carry_type = "HandsClose",
	decay_fx = true,
	description = T(625370714818, --[[ModItemResource DrySeeds description]] "Small hard dry seeds used for cooking or in the production of medicines, oils and other items."),
	display_name = T(827164070132, --[[ModItemResource DrySeeds display_name]] "Seeds"),
	food_entity = "Meal_02",
	food_entity_color = 4287708937,
	group = "FoodRaw",
	happiness_factor = "AteRawFood",
	health_condition = "Poisoning_1_Mild",
	health_condition_chance = 5,
	icon = "UI/Icons/Resources/res_grains",
	id = "DrySeeds",
	in_groups = {
		"Food",
		"FoodRaw",
		"FoodRaw_NoGroups",
		"FoodEdible",
		"FoodAnimalHerbivore",
	},
	money_value = 15000,
	preference = -25,
	progress = 200,
	serving_amount = 10000,
	serving_nutrition = 1500000,
	stack_entity = "ResourceBulk_Wheat",
	stack_idle_state_count = 3,
	stack_size = 100000,
	trade_amount_multiplier = 10000,
	use_unfinished_item = "UnfinishedFood",
	vegetarian = true,
	visible = false,
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "VegetableOil",
	'TargetProp', "Prerequisites",
	'TargetValue', {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Negate = true,
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "Grain",
				}),
			},
		}),
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "VegetableOil",
	'TargetProp', "TradePrerequisites",
	'EditType', "Append To Table",
	'TargetValue', {
		PlaceObj('CheckRegion', {
			Negate = true,
			Region = set( "Alba" ),
		}),
	},
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Devices",
	},
	Description = T(325905288827, --[[ModItemTech VegetableOil_Seeds Description]] "For centuries people have been extracting vegetable oil from various seeds by applying brute force to them. We just need to design a primitive oil press and not use it as a torture device.\n\n<style TechSubtitleBlue>Unlocks</style>\n<tabulator><em>Vegetable oil</em>: <recipe_cost('OilVegetable')>\n<tabulator><em>Wooden oil press</em>: <building_cost('OilPress')>\n<tabulator><em>Metal oil press</em>: <building_cost('OilPress_Metal')>"),
	DisplayName = T(466909432261, --[[ModItemTech VegetableOil_Seeds DisplayName]] "Oil extraction"),
	Icon = "UI/Icons/Research/cold_pressing",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "DrySeeds",
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "DrySeeds",
				}),
			},
		}),
	},
	ResearchPoints = 12000,
	SortKey = 10,
	TradePrerequisites = {
		PlaceObj('CheckRegion', {
			Region = set( "Alba" ),
		}),
	},
	group = "Resources",
	id = "VegetableOil_Seeds",
	money_value = 12500000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "OilPress",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "OilPress_Metal",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "Recipe",
		Group = "Default",
		LockState = "hidden",
		PresetId = "OilVegetable_Seeds",
	}),
}),
PlaceObj('ModItemRecipe', {
	Activity = "Crafting",
	ActivityDuration = 20000,
	ActivityXPGrade = "Crafting_Low",
	BuildCategory = "Press",
	Description = T(194737529811, --[[ModItemRecipe OilVegetable_Seeds Description]] "Press seeds and extract oil."),
	DisplayName = T(247504332343, --[[ModItemRecipe OilVegetable_Seeds DisplayName]] "Vegetable oil"),
	FailChance = 10,
	GuaranteedSuccessLevel = 3,
	Icon = "UI/Icons/Items/vegetable_oil",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "DrySeeds",
			'amount', 20000,
		}),
	},
	LockState = "hidden",
	MakeUntilMultiplier = 10,
	ManualWork = true,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "OilVegetable",
			'amount', 10000,
		}),
	},
	RequiredDeviceInterfaces = {
		"PressInterface",
	},
	SortKey = 10,
	id = "OilVegetable_Seeds",
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "Antibiotics",
	'TargetProp', "Prerequisites",
	'TargetValue', {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Negate = true,
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "Grain",
				}),
			},
		}),
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "Antibiotics",
	'TargetProp', "TradePrerequisites",
	'EditType', "Append To Table",
	'TargetValue', {
		PlaceObj('CheckRegion', {
			Negate = true,
			Region = set( "Alba" ),
		}),
	},
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Devices",
	},
	Description = T(117522268153, --[[ModItemTech Antibiotics_Seeds Description]] "In all likelihood, at least one local fungi could be used in the production of antibiotics. Let's mix some moldy dirt and some selected seeds in a fermentation barrel and see what we will get!\n\n<style TechSubtitleBlue>Unlocks</style>\n<tabulator><em>Antibiotics</em>: <recipe_cost('MedicineAntibiotics')>\n<tabulator><em>Wooden fermentation barrel</em>: <building_cost('FermentationBarrel')>\n<tabulator><em>Scrap metal fermentation barrel</em>: <building_cost('FermentationBarrel_Metal')>"),
	DisplayName = T(508041616250, --[[ModItemTech Antibiotics_Seeds DisplayName]] "Antibiotics production"),
	Icon = "UI/Icons/Research/antibiotics",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "DrySeeds",
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "DrySeeds",
				}),
			},
		}),
	},
	ResearchPoints = 24000,
	SortKey = 20,
	TradePrerequisites = {
		PlaceObj('CheckRegion', {
			Region = set( "Alba" ),
		}),
	},
	group = "Resources",
	id = "Antibiotics_Seeds",
	money_value = 25000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "FermentationBarrel",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "FermentationBarrel_Metal",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "Recipe",
		Group = "Workbench",
		LockState = "hidden",
		PresetId = "MedicineAntibiotics_Seeds",
	}),
}),
PlaceObj('ModItemRecipe', {
	BuildCategory = "Brew",
	Description = T(783938913761, --[[ModItemRecipe MedicineAntibiotics_Seeds Description]] "Grow bacteria on seeds and collect antibiotics from it."),
	DeviceWorkTime = 480000,
	DisplayName = T(103466750992, --[[ModItemRecipe MedicineAntibiotics_Seeds DisplayName]] "Antibiotics"),
	FailChance = 10,
	GuaranteedSuccessLevel = 3,
	Icon = "UI/Icons/Items/antibiotics",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "DrySeeds",
			'amount', 50000,
		}),
	},
	LockState = "hidden",
	MakeUntilMultiplier = 5,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "MedAntibiotics",
			'amount', 5000,
		}),
	},
	RequiredDeviceInterfaces = {
		"BasicFermentationInterface",
	},
	group = "Workbench",
	id = "MedicineAntibiotics_Seeds",
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "Beer",
	'TargetProp', "Prerequisites",
	'TargetValue', {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Negate = true,
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "Grain",
				}),
			},
		}),
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "Beer",
	'TargetProp', "TradePrerequisites",
	'EditType', "Append To Table",
	'TargetValue', {
		PlaceObj('CheckRegion', {
			Negate = true,
			Region = set( "Alba" ),
		}),
	},
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Devices",
	},
	Description = T(272118187159, --[[ModItemTech Beer_Seeds Description]] "The ancient Peruvians used to make beer by chewing and spiting corn. There are other ways, of course. Putting seeds in fermentation barrels and leaving it to ferment is one of them.\n\n<style TechSubtitleBlue>Unlocks</style>\n<tabulator><em>Ale</em>: <recipe_cost('AlcoholBeer')>\n<tabulator><em>Wooden fermentation barrel</em>: <building_cost('FermentationBarrel')>\n<tabulator><em>Scrap metal fermentation barrel</em>: <building_cost('FermentationBarrel_Metal')>"),
	DisplayName = T(718017684571, --[[ModItemTech Beer_Seeds DisplayName]] "Brewing"),
	Icon = "UI/Icons/Research/beer",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "DrySeeds",
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "DrySeeds",
				}),
			},
		}),
	},
	ResearchPoints = 24000,
	SortKey = 50,
	TradePrerequisites = {
		PlaceObj('CheckRegion', {
			Region = set( "Alba" ),
		}),
	},
	group = "Resources",
	id = "Beer_Seeds",
	money_value = 25000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "FermentationBarrel",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "FermentationBarrel_Metal",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "Recipe",
		Group = "Drinks",
		LockState = "hidden",
		PresetId = "AlcoholBeer_Seeds",
	}),
}),
PlaceObj('ModItemRecipe', {
	BuildCategory = "Brew",
	Description = T(798585348905, --[[ModItemRecipe AlcoholBeer_Seeds Description]] "Ferment ale from seeds."),
	DeviceWorkTime = 2880000,
	DisplayName = T(249740002051, --[[ModItemRecipe AlcoholBeer_Seeds DisplayName]] "Ale"),
	FailChance = 10,
	GuaranteedSuccessLevel = 3,
	Icon = "UI/Icons/Items/beer",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "DrySeeds",
			'amount', 100000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
	},
	LockState = "hidden",
	MakeUntilMultiplier = 5,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "AlcoholBeer",
			'amount', 5000,
		}),
	},
	RequiredDeviceInterfaces = {
		"BasicFermentationInterface",
	},
	group = "Drinks",
	id = "AlcoholBeer_Seeds",
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "Distillation",
	'TargetProp', "Prerequisites",
	'TargetValue', {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Negate = true,
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "Grain",
				}),
			},
		}),
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "Distillation",
	'TargetProp', "TradePrerequisites",
	'EditType', "Append To Table",
	'TargetValue', {
		PlaceObj('CheckRegion', {
			Negate = true,
			Region = set( "Alba" ),
		}),
	},
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Devices",
	},
	Description = T(682714243142, --[[ModItemTech Distillation_Seeds Description]] "Though we can't expect to replicate perfect lab conditions here, it is entirely possible to build a simple distillation device and use it to produce various refined substances.\n\n<style TechSubtitleBlue>Unlocks</style>\n<tabulator><em>Moonshine</em>: <recipe_cost('AlcoholCorn')>\n<tabulator><em>Sweet syrup</em>: <recipe_cost('SweetSyrup_Corn')>\n<tabulator><em>Distillery</em>: <building_cost('Distillery')>"),
	DisplayName = T(458578676034, --[[ModItemTech Distillation_Seeds DisplayName]] "Distillation"),
	Icon = "UI/Icons/Research/distillation2",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "DrySeeds",
		}),
		PlaceObj('CheckResourceUnlocked', {
			Resource = "Metal",
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "DrySeeds",
				}),
			},
		}),
	},
	ResearchPoints = 24000,
	SortKey = 50,
	TradePrerequisites = {
		PlaceObj('CheckRegion', {
			Region = set( "Alba" ),
		}),
	},
	group = "Resources",
	id = "Distillation_Seeds",
	money_value = 25000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Devices",
		LockState = "hidden",
		PresetId = "Distillery",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "Recipe",
		Group = "Drinks",
		LockState = "hidden",
		PresetId = "AlcoholBeer_Seeds",
	}),
}),
PlaceObj('ModItemRecipe', {
	BuildCategory = "Distill",
	Description = T(599352900949, --[[ModItemRecipe Alcohol_Seeds Description]] "Distill moonshine from seeds."),
	DeviceWorkTime = 960000,
	DisplayName = T(816174622861, --[[ModItemRecipe Alcohol_Seeds DisplayName]] "Moonshine"),
	FailChance = 10,
	GuaranteedSuccessLevel = 3,
	Icon = "UI/Icons/Items/whiskey",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "DrySeeds",
			'amount', 100000,
		}),
	},
	LockState = "hidden",
	MakeUntilMultiplier = 5,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "AlcoholMoonshine",
			'amount', 5000,
		}),
	},
	RequiredDeviceInterfaces = {
		"DistilleryInterface",
	},
	SortKey = 10,
	group = "Drinks",
	id = "Alcohol_Seeds",
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "CoffeeFromGrain",
	'TargetProp', "Prerequisites",
	'TargetValue', {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Negate = true,
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "Grain",
				}),
			},
		}),
	},
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "CoffeeFromGrain",
	'TargetProp', "TradePrerequisites",
	'EditType', "Append To Table",
	'TargetValue', {
		PlaceObj('CheckRegion', {
			Negate = true,
			Region = set( "Alba" ),
		}),
	},
}),
PlaceObj('ModItemTech', {
	Description = T(213963193519, --[[ModItemTech CoffeeFromGrain_Seeds Description]] "Various seeds are known to have some stimulative properties when properly tempered with. Seed coffee is not as good stimulant as the common coffee but does restore some energy in the tired body and it will slightly improve consciousness, movement and manipulation.\n\n<style TechSubtitleBlue>Unlocks</style>\n<tabulator><em>Coffee</em>: <recipe_cost('Coffee_Grain')>\n<tabulator>* boiled on Campfires or Cook stoves"),
	DisplayName = T(160452568722, --[[ModItemTech CoffeeFromGrain_Seeds DisplayName]] "Cereal coffee"),
	Icon = "UI/Icons/Research/cereal_coffee",
	LockPrerequisites = {
		PlaceObj('CheckResourceUnlocked', {
			Resource = "DrySeeds",
		}),
	},
	Prerequisites = {
		PlaceObj('CheckOR', {
			Conditions = {
				PlaceObj('CheckRegion', {
					Region = set( "Alba" ),
				}),
				PlaceObj('CheckResourceUnlocked', {
					Resource = "DrySeeds",
				}),
			},
		}),
	},
	ResearchPoints = 24000,
	SortKey = 61,
	TradePrerequisites = {
		PlaceObj('CheckRegion', {
			Region = set( "Alba" ),
		}),
	},
	group = "Resources",
	id = "CoffeeFromGrain_Seeds",
	money_value = 25000000,
	PlaceObj('RemoveLockedState', {
		Class = "Recipe",
		Group = "Drinks",
		LockState = "hidden",
		PresetId = "Coffee_Seeds",
	}),
	PlaceObj('RemoveLockedState', {
		Class = "Resource",
		Group = "OtherItems",
		LockState = "hidden",
		PresetId = "Coffee",
	}),
}),
PlaceObj('ModItemRecipe', {
	BuildCategory = "Boil",
	Description = T(518832049181, --[[ModItemRecipe Coffee_Seeds Description]] "Boil a rejuvenating seeds coffee."),
	DeviceWorkTime = 80000,
	DisplayName = T(222829488105, --[[ModItemRecipe Coffee_Seeds DisplayName]] "Coffee"),
	Icon = "UI/Icons/Items/coffee",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "DrySeeds",
			'amount', 20000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
		PlaceObj('ResAmount', {
			'resource', "PotableWater",
			'amount', 5000,
		}),
	},
	LockState = "hidden",
	MakeUntilMultiplier = 5,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "Coffee",
			'amount', 5000,
		}),
	},
	RequiredDeviceInterfaces = {
		"CauldronInterface",
		"PotInterface",
	},
	group = "Drinks",
	id = "Coffee_Seeds",
}),
}
