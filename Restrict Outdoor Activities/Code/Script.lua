function OnMsg.ClassesBuilt()
    local for_human = table.find_value(Presets.XTemplate["Infopanel Actions"].IPActionsHuman, "__context_of_kind", "Human")
    table.remove_entry(for_human, "ActionId", "idRestrictInside")
    for_human[#for_human + 1] = PlaceObj("XTemplateAction", {
        "RolloverTemplate", "Rollover",
        "RolloverText", T("Restrict activities only inside."),
        "ActionId", "idRestrictInside",
        "ActionName", T("Restrict outdoors"),
        "ActionIcon", CurrentModPath .. "UI/Icons/Infopanels/inside_locked",
        "ActionToolbar", "ip_actions",
        "ActionState", function(self, host)
            local obj = ResolvePropObj(host.context)
            if obj.RestrictedInside then
                self.ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/inside_unlocked"
                self.RolloverText = T("Allow activities indoors and outdoors.")
                self.RolloverHint = T("Shift + <left_click> Allow everyone outside")
                self.RolloverHintGamepad = T("<ButtonY> Allow everyone outside")
                self.ActionName = T("Allow outdoor")
            else
                self.ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/inside_locked"
                self.RolloverText = T("Allow activities only inside house.")
                self.RolloverHint = T("Shift + <left_click> Restrict everyone inside")
                self.RolloverHintGamepad = T("<ButtonY> Restrict everyone inside")
                self.ActionName = T("Stay inside")
            end
            return not obj.RestrictedInside and not MapGet("map", "Room") and "disabled"
        end,
        "OnAction", function(self, host, source, ...)
            local context = ResolvePropObj(host.context)
            if context:IsKindOf("Human") then
                local new_state = not context.RestrictedInside
                if terminal.IsKeyPressed(const.vkShift) then
                    for _, survivor in ipairs(context.player.labels.Survivors) do
                        survivor.RestrictedInside = new_state
                    end
                else
                    context.RestrictedInside = new_state
                end
            end
            RebuildInfopanel(context)
        end,
    })
end

local CreateBinActions_HumanAutoResolve = HumanAutoResolve.CreateBinActions
function HumanAutoResolve.CreateBinActions(bin, host, skip_bin_actions_for_work_actions)
    if skip_bin_actions_for_work_actions then return end
    XAction:new({
        ActionId = "idRestrictInside",
        ActionName = T("Restrict outdoors"),
        ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/inside_locked",
        ActionToolbar = "ip_actions",
        RolloverTemplate = "Rollover",
        RolloverText = T("Restrict activities only inside."),
        ActionState = function(self, host)
            local any_allowed_outside = false
            for _, obj in ipairs(bin) do
                if not obj.RestrictedInside then
                    any_allowed_outside = true
                end
            end
            if not any_allowed_outside then
                self.ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/inside_unlocked"
                self.RolloverText = T("Allow activities indoors and outdoors.")
                self.ActionName = T("Allow outdoor")
            else
                self.ActionIcon = CurrentModPath .. "UI/Icons/Infopanels/inside_locked"
                self.RolloverText = T("Allow activities only inside house.")
                self.ActionName = T("Stay inside")
            end
            return any_allowed_outside and not MapGet("map", "Room") and "disabled"
        end,
        OnAction = function(...)
            local any_allowed_outside = false
            for _, obj in ipairs(bin) do
                if not obj.RestrictedInside then
                    any_allowed_outside = true
                end
            end
            for _, survivor in ipairs(bin) do
                survivor.RestrictedInside = any_allowed_outside
                ObjModified(survivor)
            end
        end,
    }, host)
    return CreateBinActions_HumanAutoResolve(bin, host, skip_bin_actions_for_work_actions)
end

local CanRoamOutside_Human = Human.CanRoamOutside
function Human:CanRoamOutside()
    return not self.RestrictedInside and CanRoamOutside_Human(self)
end

--auto resolve method
function UnitActivity:IsAttackTargetIgnored(target)
    return not target.room
end

local FindActivity_UnitActivity = UnitActivity.FindActivity
function UnitActivity:FindActivity(set, range, queue_is_empty, search_until, reset)
    self.looking_for_activity = true
    local res_source, res_activity, res_target, res_param1, res_param2 = FindActivity_UnitActivity(self, set, range, queue_is_empty, search_until, reset)
    self.looking_for_activity = nil
    return res_source, res_activity, res_target, res_param1, res_param2
end

local DoFindFood_UnitEnergy = UnitEnergy.DoFindFood
function UnitEnergy:DoFindFood(...)
    self.looking_for_food = not self:IsAlmostStarving()
    local result = DoFindFood_UnitEnergy(self, ...)
    self.looking_for_food = nil
    return result
end

local CanUnitPerformActivity_DeliveryActivityObject = DeliveryActivityObject.CanUnitPerformActivity
function DeliveryActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    unit.looking_for_target = not direct_order
    local result = CanUnitPerformActivity_DeliveryActivityObject(self, unit, skill_level, direct_order)
    unit.looking_for_target = nil
    return result
end

local function GetTargetToCheck(target)
    if target:HasMember("IsOutside") then
        return target
    end
    local parent = GetTopmostParent(target)
    if parent:HasMember("IsOutside") then
        return parent
    end
end

local IsTargetUnreachable_Unit = Unit.IsTargetUnreachable
function Unit:IsTargetUnreachable(target)
    local looking_restricted = self.RestrictedInside and (self.looking_for_activity or self.looking_for_food or self.looking_for_target)
    if not looking_restricted then
        return IsTargetUnreachable_Unit(self, target)
    end
    local to_check = GetTargetToCheck(target)
    if not to_check then
        if GetTopmostParent(target):IsKindOfClasses("InteractiveSite", "MineableRock", "ForestDebris") then
            return true
        end
        if (target:IsKindOf("ActivityObject")) then
            local object = GetTargetToCheck(target:GetActivityTarget())
            if object and not object.room then
                return true
            end
        else
            print("Failed to check target reach for", _InternalTranslate(self:GetDisplayName()))
            return IsTargetUnreachable_Unit(self, target)
        end
    end
    if not to_check.room then
        return true
    end
    return IsTargetUnreachable_Unit(self, target)
end

