return {
PlaceObj('ModItemChangeProp', {
	'TargetClass', "Tech",
	'TargetId', "StorageSafes",
	'TargetProp', "AllowedInScenarios",
	'TargetValue', set( "Robots", "Trading" ),
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "ResearchGroup",
	'TargetId', "Construction",
	'TargetProp', "display_name",
	'TargetValue', T(548269621684, "CONSTRUCTION"),
}),
PlaceObj('ModItemCode', {
	'CodeFileName', "Code/Script.lua",
}),
PlaceObj('ModItemResearchGroup', {
	SortKey = 450,
	display_name = T(586495568281, --[[ModItemResearchGroup Other display_name]] "OTHER"),
	id = "Other",
}),
}
