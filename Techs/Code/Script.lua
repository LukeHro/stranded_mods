function OnMsg.ModsReloaded()
    XTemplates.ResearchDialog[1][10][4][5][2][2][5][1][2][1].condition = function (parent, context, item, i)
        return item.show_in_group == context.id or not item.show_in_group and item.group == context.id
    end
    XTemplates.ResearchDialog[1][10][4][5][2][2][5][1][3][1].condition = function (parent, context, item, i)
        return item.show_in_group == context.id or not item.show_in_group and item.group == context.id
    end
    XTemplates.ResearchDialog[1][10].MaxWidth = 1400
    XTemplates.ResearchDialog[1][10].MinWidth = 1400
    XTemplates.ResearchDialog[1][10].MaxHeight = 800
    XTemplates.ResearchDialog[1][10].MinHeight = 800
    Presets.Tech.Construction.FlareShielding.show_in_group = "Other"
    Presets.Tech.Construction.AnimalTaming.show_in_group = "Other"
    Presets.Tech.Construction.CampManagement.show_in_group = "Other"
    Presets.Tech.Construction.Deconstruction.show_in_group = "Other"
    Presets.Tech.Construction.Toolsmithing.show_in_group = "Other"
    Presets.Tech.Construction.EMUmbrellas.show_in_group = "Other"
    Presets.Tech.Construction.RespiratorMasks.show_in_group = "Other"
    Presets.Tech.Construction.NightGoggles.show_in_group = "Other"
    Presets.Tech.Construction.SignalFlares.show_in_group = "Other"
    Presets.Tech.Construction.Grenades.show_in_group = "Other"

    Presets.Tech.Defense.BasicTailoring.show_in_group = "Other"

    if Presets.Tech.Construction.EMPGrenades then
        Presets.Tech.Construction.EMPGrenades.show_in_group = "Other"
    end
end


function SavegameFixups.LH_UnlockSafeInRobots()
    if Game.scenario == "Robots" then
        RemovePresetLockStateReason("Tech", "Construction", "StorageSafes", "hidden", "Scenario")
    end
end