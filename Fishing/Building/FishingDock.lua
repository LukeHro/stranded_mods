UndefineClass('FishingDock')
DefineClass.FishingDock = {
	__parents = { "FishingDockBuilding" },
	__generated_by_class = "ModItemBuildingCompositeDef",


	object_class = "FishingDockBuilding",
	LockState = "hidden",
	unload_anim_hands = "standing_DropDown_Hands",
	load_anim_hands = "standing_PickUp_Hands",
	BuildMenuCategory = "Animals",
	display_name = T(794949828057, --[[ModItemBuildingCompositeDef FishingDock display_name]] "Fishing dock"),
	description = T(489205621343, --[[ModItemBuildingCompositeDef FishingDock description]] "Catch local <color TextEmphasis><color TextEmphasis>Fish</color><image 'C:/Users/Luke/AppData/Roaming/Stranded - Alien Dawn/Mods/Fishing/UI/Icons/Resources/res_raw_fish.dds' 1100></color> here."),
	menu_display_name = T(492858585492, --[[ModItemBuildingCompositeDef FishingDock menu_display_name]] "Fishing dock"),
	BuildMenuIcon = "UI/Icons/Build Menu/fishing_dock.dds",
	BuildMenuPos = 1,
	display_name_pl = T(475923597376, --[[ModItemBuildingCompositeDef FishingDock display_name_pl]] "Fishing Docks"),
	entity = "FishingDock_LH",
	labels = {
		"BerserkTargets",
	},
	update_interval = 5000,
	construction_cost = PlaceObj('ConstructionCost', {
		Wood = 30000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('WaterDockConstructionRule', nil),
	},
	can_be_placed_on_floor = false,
	RoomPlacement = "outdoors",
	CanPlaceInShelter = false,
	lock_block_box = box(-300, -300, 0, 300, 300, 600),
	forbid_clip_plane = true,
	CustomMaterial = "Wood",
	attack_attraction = 5,
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
}

