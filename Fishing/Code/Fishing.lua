function OnMsg.ModsReloaded()
    Presets.ActivitySet.Default.Work.Activities.Fishing = true

    PlaceObj('ConstructionRuleResult', {
        id = "DockTooClose",
        infopanel_text = T("Too close to another Dock."),
        inview_text = T("Too close to others"),
    })
    PlaceObj('ConstructionRuleResult', {
        id = "DockNotOnLand",
        infopanel_text = T("The Dock must start on land."),
        inview_text = T("Not starting on land"),
    })
    PlaceObj('ConstructionRuleResult', {
        id = "DockNotOnWater",
        infopanel_text = T("The Dock must end in water."),
        inview_text = T("Not ending in water"),
    })
end

function OnMsg.ConstructionComplete(_, obj)
    if obj:IsKindOf("FishingDockBuilding") then
        obj:SetHierarchyEnumFlags(const.efSelectable)
    end
end

DefineClass.FishingDockBuilding = {
    __parents = { "CompositeBuilding" },
    chance = 50,
    last_catch = 0,
    last_fisher = false,
    last_update = false,
}

function FishingDockBuilding:OnObjUpdate(time)
    if not IsConstructionFinished(self) then
        return
    end
    self:CreateFishingActivity()
    if self.last_update then
        if time - self.last_update > const.HourDuration then
            self:UpdateChance(1)
            self.last_update = self.last_update + const.HourDuration
        end
    else
        self.last_update = time;
    end
end

function FishingDockBuilding:UpdateChance(delta)
    self.chance = Clamp(self.chance + delta, 25, 75)
    ObjModified(self)
end

function FishingDockBuilding:CreateFishingActivity()
    if not IsValid(self) or IsBeingDestructed(self) then
        return
    end
    local obj = self:GetAttach("FishingActivityObject")
    if IsValid(obj) then
        return obj
    end
    obj = CreateActivityObject("FishingActivityObject", {
        player = self.player
    }, self)
    obj:SetActivityAvailable(true)
    return obj
end

function FishingDockBuilding:RemoveFishingActivity()
    local obj = self:GetAttach("FishingActivityObject")
    if IsValid(obj) then
        obj:delete()
    end
end

function FishingDockBuilding:GatherDirectOrders(unit, orders)
    if unit and self:CanWork() then
        CreateDirectOrderButtons(orders, DirectOrders.Fishing, self)
    end
end

function FishingDockBuilding:GetAssignedSurvivor()
    local activity = self:GetAttach("FishingActivityObject")
    return activity and activity.activity_unit
end

function FishingDockBuilding:CatchFish(unit, activity)
    self.last_catch = 0
    self.last_fisher = unit
    local roll = self:Random(100)
    local luck = unit:HasSurvivalTool("GoodLuckCharm") and 10 or 0
    local skill = activity and activity.Skill and unit:GetSkillLevel(activity.Skill) or 0
    if roll - skill < self.chance + luck then
        self.last_catch = Clamp(roll / 10 + skill, 5, 20)
        self:UpdateChance(-self.last_catch)
        unit:AddActivityExperience(activity, 100)
    end
    self:UpdateChance(-20 + skill)
    return self.last_catch * const.ResourceScale, "RawFish"
end

function FishingDockBuilding:GetIPDockInfo()
    return T([[Catch chance <left><tabulator><em><right><chance>%</em>

<left>Under 50% chance survivors won't use the dock on their own but can still be ordered to do so.]])
end

function FishingDockBuilding:GetIPCatchLog()
    if self.last_fisher then
        if self.last_catch > 0 then
            return T{"<last_fisher.FirstName> caught <res(fish,amount)> here", fish = "RawFish", amount = self.last_catch * const.ResourceScale}
        else
            return T("<last_fisher.FirstName> just scared the fish")
        end
    else
        return T("No one fished here yet")
    end
end

DefineClass.FishingActivityObject = {
    __parents = {
        "ActivityObject"
    },
    activity_id = "Fishing",
    retry_time = false,
}

function FishingActivityObject:GetActivityRange()
    return 40 * guic, 3 * const.SlabSizeX / 2
end

function FishingActivityObject:DoActivity(unit, activity, skill_level, efficiency)
    local building = self:GetActivityTarget()
    if not building then
        return
    end
    local dtors = unit:PushDestructor(function(self)
        self.fx_anim_target = nil
    end)
    unit.fx_anim_target = building
    local activity_time = MulDivRound(const.HourDuration, 100, efficiency)
    unit:PlayPrg(PrgAmbientLife.Fishing, activity_time)
    local amount, resource = building:CatchFish(unit, activity)
    unit:PopAndCallDestructor(dtors)
    self:delete()
    if amount > 0 then
        PlaceResourcePile(building:GetSpotLocPos(building:GetSpotBeginIndex("Resource")), resource, amount)
        CreateFloatingText(unit, T{787274489255, "<res(resource,amount)>", resource = resource, amount = amount}, nil, "Task")
    else
        CreateFloatingText(unit, T(182714092509, "Fail"), "FloatingNegative", "Task")
    end
    return true
end

function FishingActivityObject:CanUnitPerformActivity(unit, skill_level, direct_order)
    if direct_order then
        return true
    end
    if (self.retry_time or 0) > GameTime() then
        return
    end
    local target = self:GetActivityTarget()
    local area_is_locked = not target:IsSafeForApproaching(unit)
    if area_is_locked then
        self.retry_time = GameTime() + const.HourDuration
        return
    end
    return target.chance >= 50
end

function FishingActivityObject:Done()
end

function PrgAmbientLife.Fishing(unit, ...)
    local unit_orig_scale
    local speed
    local stick = PlaceObject("WoodenStick", nil, const.cofComponentAttach)
    NetTempObject(stick)
    stick:SetAttachOffset(0, -20, 0)
    unit:Attach(stick, unit:GetRandomSpot("Spoon"))
    unit:PushDestructor(function(unit)
        unit:SetScale(unit_orig_scale, unit:GetAnimDuration("standing_Cut_Tool_End"))
        unit:SetAnimSpeedModifier(speed)
        unit:PlayMomentTrackedAnim("standing_Cut_Tool_End", 1, nil, 200, nil)
        if IsValid(stick) then
            DoneObject(stick)
        end
    end)
    unit:StatusUIUpdateData("activity", {
        bar1_start_time = GameTime(),
        bar1_end_time = GameTime() + unit:VisitTimeLeft()
    })
    unit_orig_scale = unit:GetScale()
    speed = unit:GetAnimSpeedModifier()
    unit:SetScale(100, unit:GetAnimDuration("standing_Build_MineUp_Idle"))
    while unit:VisitTimeLeft() > 0 do
        local duration = unit:GetAnimDuration("standing_Build_MineUp_Idle") / 10
        unit:SetAnimSpeedModifier(speed / 2)
        stick:SetAxisAngle(axis_y, -4000, duration)
        unit:PlayMomentTrackedAnim("standing_Build_MineUp_Idle", 1, nil, 200, duration)
        duration = Min(unit:VisitTimeLeft(), unit:GetAnimDuration("standing_Build_Hammer2_Idle"))
        unit:SetAnimSpeedModifier(speed / 10)
        unit:PlayMomentTrackedAnim("standing_Build_Hammer2_Idle", 1, nil, 200, duration)
        if unit.visit_restart then
            return
        end
        duration = unit:GetAnimDuration("standing_Cut_Tool_End")
        unit:SetAnimSpeedModifier(speed)
        stick:SetAxisAngle(axis_y, 0, duration)
        unit:PlayMomentTrackedAnim("standing_Cut_Tool_End", 1, nil, 200, nil)
    end
    unit:PopAndCallDestructor()
end

DefineClass.WaterDockConstructionRule = {
    __parents = {
        "ConstructionObjectSnapToVoxel"
    },
}
function WaterDockConstructionRule:UpdateConstructionRule(controller)
    local cursor = controller.cursor
    local labels = controller.player.labels
    local cursor_pos = cursor:GetPos()
    for _, d in ipairs(labels.FishingDock) do
        if d ~= controller.moving_building and IsCloser2D(d, cursor_pos, 20 * guim) then
            return "DockTooClose", d
        end
    end
    local spot_land = cursor:GetSpotBeginIndex("Resource")
    if spot_land ~= -1 then
        local spot_pos = cursor:GetSpotLocPos(spot_land, cursor:TimeToInterpolationEnd())
        if (terrain.GetWaterHeight(spot_pos) > terrain.GetHeight(spot_pos)) then
            return "DockNotOnLand"
        end
    end
    local spot_water = cursor:GetSpotBeginIndex("Fishinginterface")
    if spot_water ~= -1 then
        local spot_pos = cursor:GetSpotLocPos(spot_water, cursor:TimeToInterpolationEnd())
        if (terrain.GetWaterHeight(spot_pos) < terrain.GetHeight(spot_pos)) then
            return "DockNotOnWater"
        end
    end
end

local UpdateSkillInclinations_UnitSkill = UnitSkill.UpdateSkillInclinations
function UnitSkill:UpdateSkillInclinations(...)
    if self.activity_forbidden_reasons then
        self.activity_forbidden_reasons["Fishing"] = self.activity_forbidden_reasons["Ranching"]
    end
    return UpdateSkillInclinations_UnitSkill(self, ...)
end
UnitSkill.SetActivityAllowedReason = UnitSkill.UpdateSkillInclinations

function SavegameFixups.FixHopeFishing()
    if Hope then
        Hope:UpdateSkillInclinations()
    end
end