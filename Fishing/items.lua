return {
PlaceObj('ModItemActivityGroup', {
	Activities = {
		"Fishing",
	},
	Description = T(544463293594, --[[ModItemActivityGroup Fish Description]] "Fish at the local ponds. Fishing is often governed by chance."),
	DisplayName = T(502111999917, --[[ModItemActivityGroup Fish DisplayName]] "Fish"),
	DisplayNameShortened = T(786846012483, --[[ModItemActivityGroup Fish DisplayNameShortened]] "Fish"),
	SortKey = 40,
	id = "Fish",
}),
PlaceObj('ModItemActivityType', {
	ActivityStatusText = T(834451896252, --[[ModItemActivityType Fishing ActivityStatusText |gender-variants]] "Fishing"),
	ActivityText = T(473315214794, --[[ModItemActivityType Fishing ActivityText |gender-variants]] "Fish here"),
	AddDeliveryToBatch = true,
	Description = T(209020628829, --[[ModItemActivityType Fishing Description]] "Fish at a fishing dock."),
	DisplayName = T(453861898807, --[[ModItemActivityType Fishing DisplayName]] "Fish"),
	EfficiencyFromConsciousness = true,
	EfficiencyFromManipulation = true,
	EfficiencyFromSkill = 50,
	GetActivityQueueStatusText = function (self, target, unit, param1, param2)
		        return GetTByGender(self.QueueStatusText, unit)
	end,
	GetDeliveryRes = function (self, target, unit)
		        return true
	end,
	GetDisplayedStatusText = function (self, unit, short)
		        return GetTByGender(self.ActivityStatusText, unit)
	end,
	GetIPDisplayedStatusText = function (self, unit)
		return T("Fishing")
	end,
	PickClosest = true,
	QueueStatusText = T(679693454901, --[[ModItemActivityType Fishing QueueStatusText |gender-variants]] "Fish"),
	Skill = "Farming",
	SortKey = 2605,
	StatusIcon = "UI/Hud/st_fish.dds",
	id = "Fishing",
}),
PlaceObj('ModItemBuildingCompositeDef', {
	BuildMenuCategory = "Animals",
	BuildMenuIcon = "UI/Icons/Build Menu/fishing_dock.dds",
	BuildMenuPos = 1,
	CanPlaceInShelter = false,
	CustomMaterial = "Wood",
	LockState = "hidden",
	RoomPlacement = "outdoors",
	attack_attraction = 5,
	can_be_placed_on_floor = false,
	construction_cost = PlaceObj('ConstructionCost', {
		Wood = 30000,
	}),
	construction_points = 10000,
	custom_constr_rules = {
		PlaceObj('WaterDockConstructionRule', nil),
	},
	description = T(489205621343, --[[ModItemBuildingCompositeDef FishingDock description]] "Catch local <color TextEmphasis><color TextEmphasis>Fish</color><image 'C:/Users/Luke/AppData/Roaming/Stranded - Alien Dawn/Mods/Fishing/UI/Icons/Resources/res_raw_fish.dds' 1100></color> here."),
	display_name = T(794949828057, --[[ModItemBuildingCompositeDef FishingDock display_name]] "Fishing dock"),
	display_name_pl = T(475923597376, --[[ModItemBuildingCompositeDef FishingDock display_name_pl]] "Fishing Docks"),
	enable_overlay_on_placement = {
		RoomsOverlay = true,
	},
	entity = "FishingDock_LH",
	forbid_clip_plane = true,
	id = "FishingDock",
	labels = {
		"BerserkTargets",
	},
	load_anim_hands = "standing_PickUp_Hands",
	lock_block_box = box(-300, -300, 0, 300, 300, 600),
	menu_display_name = T(492858585492, --[[ModItemBuildingCompositeDef FishingDock menu_display_name]] "Fishing dock"),
	object_class = "FishingDockBuilding",
	unload_anim_hands = "standing_DropDown_Hands",
	update_interval = 5000,
}),
PlaceObj('ModItemCode', {
	'name', "Fishing",
	'CodeFileName', "Code/Fishing.lua",
}),
PlaceObj('ModItemDeviceInterface', {
	id = "FishingInterface",
	prg_name = "Fishing",
	spot_name = "Fishinginterface",
}),
PlaceObj('ModItemDirectOrder', {
	Activate = function (self, unit, target, param, clear_queue, ...)
		        local activity_id = self.ActivityId
		        local activity = ActivityTypes[activity_id]
		        local skill_level, efficiency = unit:GetSkillAndEfficiency(activity)
		        local activity_target = target:GetAttach("FishingActivityObject")
		        unit:EnqueueAndTryExecuteActivity(activity_id, activity_target, skill_level, efficiency, clear_queue)
	end,
	ActivityId = "Fishing",
	CanTransfer = function (self, old_unit, new_unit, target, param, queue_cleared)
		        return self:GetAssignedUnit(target, param)
	end,
	EnableCanceling = false,
	GetAssignedUnit = function (self, target, param)
		        return target:GetAssignedSurvivor()
	end,
	GetButtonText = function (self, unit, target, param, status_text)
		        return GetTByGender(self.Text, unit)
	end,
	GetErrorStatus = function (self, unit, target, param)
		        return DirectOrder.GetErrorStatus(self, unit, target, param)
	end,
	RequireUnit = true,
	Transfer = function (self, old_unit, new_unit, target, param, queue_cleared)
		        local activity_target = target:GetAttach("FishingActivityObject")
		        local activity_unit = activity_target.activity_unit or activity_target.enqueued
		        if activity_unit == target.assigned_unit then
		            activity_target.transferring = true
		        end
		        self:Activate(new_unit, target, param, queue_cleared)
	end,
	id = "Fishing",
}),
PlaceObj('ModItemEntity', {
	'name', "FishingDock_LH",
	'class_parent', "AutoAttachObject",
	'fade_category', "Never",
	'entity_name', "FishingDock_LH",
	'material_type', "Wood",
}),
PlaceObj('ModItemRecipe', {
	Activity = "Cooking",
	ActivityDuration = 40000,
	ActivityXPGrade = "Cooking_Normal",
	BuildCategory = "RecipesTasty",
	Description = T(655267643116, --[[ModItemRecipe FriedFish Description]] "Fry fish meat for a quick meal."),
	DestroyOnFail = false,
	DeviceWorkTime = 12000,
	DisplayName = T(210510464107, --[[ModItemRecipe FriedFish DisplayName]] "Fried fish"),
	FailChance = 20,
	GuaranteedSuccessLevel = 5,
	Icon = "UI/Icons/Items/fried_fish.dds",
	InputResources = {
		PlaceObj('ResAmount', {
			'resource', "RawFish",
			'amount', 10000,
		}),
	},
	ManualWork = true,
	MinSkillLevel = 2,
	OutputDecayFromInput = true,
	OutputResources = {
		PlaceObj('ResAmount', {
			'resource', "FriedFish",
			'amount', 2000,
		}),
	},
	ProficiencyLevel = 5,
	RequiredDeviceInterfaces = {
		"PotInterface",
	},
	id = "FriedFish",
}),
PlaceObj('ModItemResource', {
	ConsumerTags = set( "Animal", "Human" ),
	DecayRateInside = 694,
	DecayRateOutside = 1042,
	DecayRateRefrigerated = 347,
	DecayTimeInside = 5760000,
	DecayTimeOutside = 3840000,
	DecayTimeRefrigerated = 11520000,
	FuelDescription = T(129854986715, --[[ModItemResource RawFish FuelDescription]] "This device uses <color TextEmphasis>Raw red meat</color><image 'UI/Icons/Resources/res_raw_meatt' 1100>. Raw red meat can be obtained through butchering of most hot-blooded animals."),
	LockState = "hidden",
	MinStorageCondition = 4,
	ShowDiscoveryNotification = false,
	SortKey = 202,
	alt_icon = "UI/Icons/Items/raw_fish.dds",
	anim_pile_load = "standing_PickUp_HandsClose",
	anim_pile_unload = "standing_DropDown_HandsClose_High",
	carry_amount = 200000,
	carry_entity = "ResourceBulk_PoultryMeat",
	carry_spot = "Tool",
	carry_type = "HandsClose",
	consumable = true,
	decay_fx = true,
	description = T(261642478123, --[[ModItemResource RawFish description]] "Fresh fish caught from the ponds around."),
	display_name = T(269472664698, --[[ModItemResource RawFish display_name]] "Fish"),
	display_name_pl = T(316988993942, --[[ModItemResource RawFish display_name_pl]] "Fishes"),
	food_entity = "Meal_01",
	happiness_factor = "AteRawFood",
	health_condition = "Poisoning_1_Mild",
	health_condition_chance = 5,
	icon = "UI/Icons/Resources/res_raw_fish.dds",
	id = "RawFish",
	in_groups = {
		"RawMeats",
		"Food",
		"FoodEdible",
		"FoodRaw",
		"FoodRaw_NoGroups",
		"FoodAnimalCarnivore",
	},
	money_value = 38000,
	preference = -25,
	progress = 200,
	res_entities_idle_state_count = 2,
	resource_entities = {
		"Resource_DryMeat_01",
		"Resource_DryMeat_02",
		"Resource_DryMeat_03",
		"Resource_DryMeat_04",
	},
	serving_amount = 10000,
	serving_nutrition = 1500000,
	stack_entity = "ResourceBulk_PoultryMeat",
	stack_idle_state_count = 5,
	stack_metas = {
		"drying_progress",
	},
	stack_size = 100000,
	trade_amount_multiplier = 10000,
	visible = false,
}),
PlaceObj('ModItemResource', {
	Comment = "Tasty",
	ConsumerTags = set( "Human" ),
	DecayRateInside = 1042,
	DecayRateOutside = 2083,
	DecayRateRefrigerated = 521,
	DecayTimeInside = 3840000,
	DecayTimeOutside = 1920000,
	DecayTimeRefrigerated = 7680000,
	LockState = "hidden",
	MinStorageCondition = 4,
	SortKey = 115,
	alt_icon = "UI/Icons/Items/fried_fish.dds",
	anim_pile_load = "standing_PickUp_HandsClose",
	anim_pile_unload = "standing_DropDown_HandsClose_High",
	carry_entity = "ResourceBulk_Pan",
	carry_rotation = 5400,
	carry_spot = "Tool",
	carry_type = "HandsClose",
	consumable = true,
	decay_fx = true,
	description = T(854057132501, --[[ModItemResource FriedFish description]] "Fried locally caught fish."),
	display_name = T(241029723192, --[[ModItemResource FriedFish display_name]] "Fried fish"),
	display_name_pl = T(615158027273, --[[ModItemResource FriedFish display_name_pl]] "Fried fishes"),
	food_entity = "Meal_01",
	food_entity_color = 4281341205,
	happiness_factor = "AteCookedFoodGood_2",
	icon = "UI/Icons/Resources/res_fried_fish.dds",
	id = "FriedFish",
	in_groups = {
		"FoodProcessed_Quick",
		"FoodProcessed",
		"Food",
		"FoodEdible",
		"EmergencyRationMaterials",
	},
	item_metas = {
		"exquisite",
	},
	money_value = 715000,
	preference = 200,
	progress = 10000,
	quality = 50,
	serving_nutrition = 1500000,
	stack_entity = "ResourceBulk_Pan",
	visible = false,
}),
PlaceObj('ModItemTech', {
	BuildMenuCategoryHighlights = {
		"Animals",
	},
	Description = T(856611412893, --[[ModItemTech Fishing Description]] "Fishing is one of the most accessible ways to procure some meat especially when there are ponds around, yet the catch depends on luck too...\n\n<style TechSubtitleBlue>Unlocks</style>\n   <color TextEmphasis>Fishing dock</color>: 10<image 'UI/Icons/Resources/res_wood_trunk' 1100>"),
	DisplayName = T(563657562310, --[[ModItemTech Fishing DisplayName]] "Fishing"),
	Icon = "UI/Icons/Build Menu/fishing_dock.dds",
	ResearchPoints = 24000,
	group = "Resources",
	id = "Fishing",
	money_value = 30000000,
	PlaceObj('RemoveLockedState', {
		Class = "BuildingCompositeDef",
		Group = "Default",
		LockState = "hidden",
		PresetId = "FishingDock",
	}),
}),
PlaceObj('ModItemXTemplate', {
	group = "Infopanel Sections",
	id = "tabOverview_FishingDock",
	PlaceObj('XTemplateTemplate', {
		'__context_of_kind', "FishingDock",
		'__template', "InfopanelSection",
	}, {
		PlaceObj('XTemplateWindow', {
			'__class', "XText",
			'Padding', box(0, 5, 7, 5),
			'FoldWhenHidden', true,
			'HandleMouse', false,
			'TextStyle', "InfopanelText",
			'Translate', true,
			'Text', T(690522952109, --[[ModItemXTemplate tabOverview_FishingDock Text]] "<IPDockInfo>"),
			'HideOnEmpty', true,
		}),
		PlaceObj('XTemplateWindow', {
			'__class', "XText",
			'Padding', box(0, 5, 7, 5),
			'FoldWhenHidden', true,
			'HandleMouse', false,
			'TextStyle', "InfopanelText",
			'Translate', true,
			'Text', T(713910698082, --[[ModItemXTemplate tabOverview_FishingDock Text]] "<IPCatchLog>"),
			'HideOnEmpty', true,
		}),
		}),
}),
}
