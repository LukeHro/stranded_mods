local SetFarmerStatus_Human = Human.SetFarmerStatus
function Human:SetFarmerStatus(status, time, money)
    if status == "hired" and (not time or time <= 0) then
        return
    else
        return SetFarmerStatus_Human(self, status, time, money)
    end
end


function SavegameFixups.LH_UnlockStuffInExperimental()
    if Game.scenario == "PlanetaryFoothold_allTags" then
        RemovePresetLockStateReason("Tech", "Resources", "SilicaEnergel", "hidden")
        RemovePresetLockStateReason("Recipe", "Workbench", "RepairParts", "hidden")
    end
end

function OnMsg.HumanGainTrait(unit, trait_id)
    if trait_id ~= "FarmWorker" then return end
    if not unit:IsHired() then
        unit:SetTrait("FarmWorker", false, "forced")
    end
end

local function AddHalfYearJoin(story_bit)
    if not story_bit.LH_StayHalfYear then
        story_bit.LH_StayHalfYear = true
        table.insert_unique(story_bit, PlaceObj('StoryBitReply', {
            CustomOutcomeText = T("They will stay around for a while"),
            Text = T("On second thought, you can stay for half a year"),
            unique_id = 99,
            HideIfDisabled = true,
            Prerequisites = {
                PlaceObj('CheckExpression', {
                    Expression = function (self, obj) return CheckGameScenarioTag("Foothold") end,
                }),
            }
        }))
        table.insert_unique(story_bit,PlaceObj('StoryBitOutcome', {
            Effects = {
                PlaceObj('PickUpHiredSurvivor', {
                    CalcPayment = function (self, context) return 0 end,
                    time = 23040000,
                }),
            },
        }))
    end
end

function OnMsg.ModsReloaded()
    AddHalfYearJoin(Presets.StoryBit.Guardians.NewRecruit_TradingPod)
    --TODO add more stories here
end
