return {
PlaceObj('ModItemChangeProp', {
	'TargetClass', "StoryBit",
	'TargetId', "NewRecruit_TradingPod",
	'TargetProp', "Image",
}),
PlaceObj('ModItemCode', {
	'CodeFileName', "Code/Script.lua",
}),
PlaceObj('ModItemGameRuleDef', {
	SortKey = 99,
	description = T(190021710878, --[[ModItemGameRuleDef AddHope description]] "Adds Hope to the team.\n\nNote that she's simply added, nothing done to fix her behavior for non Guardians scenario so use it on your own risk."),
	display_name = T(562316550727, --[[ModItemGameRuleDef AddHope display_name]] "Add Hope (experimental)"),
	effects = {
		PlaceObj('ExecuteCode', {
			Code = function (self, obj)
				if CheckGameScenarioTag("Robots") then
					return
				end
				table.insert_unique(BonusStartingSurvivors, "Hope")
			end,
			param_bindings = false,
		}),
	},
	id = "AddHope",
	new_in = "Robots",
}),
PlaceObj('ModItemScenarioDef', {
	DecreasePopMax = 8,
	DecreasePopMin = 6,
	GameLoseConds = {
		PlaceObj('CheckExpression', {
			Expression = function (self, player) return next(SpawnedSurvivorIds) and LiveSurvivorsCount() == 0 end,
			Params = "self, player",
		}),
	},
	GameLoseText = T(151535295452, --[[ModItemScenarioDef PlanetaryFoothold GameLoseText]] "The outpost has fallen. Mission failed. "),
	GameWinConds = {
		PlaceObj('PickFromLabel', {
			Filters = {
				PlaceObj('CheckExpression', {
					Expression = function (self, obj) return obj.communication_established end,
				}),
			},
			Label = "MilitaryWonder",
		}),
	},
	GameWinText = T(359713548887, --[[ModItemScenarioDef PlanetaryFoothold GameWinText]] "We proved colonization is possible. Mission successful. "),
	Image = "Mod/LH_PlanetaryFoothold/UI/Scenarios/PlanetaryFoothold.png",
	IncreasePopMax = 12,
	IncreasePopMin = 10,
	IntroClouds = true,
	KeepPlayingOnWin = true,
	LandingScene = function (self)
		Presets.ScenarioDef.Default.MilitaryOutpost.LandingScene(self)
	end,
	ScenarioTags = set( "Foothold", "MilitaryOutpost", "Trading" ),
	SortKey = 1400,
	StartPrefabTags = set( "Farming" ),
	StartTime = 320000,
	StartingCharacters = 8,
	StartingLootTemplate = "Trading_StartingLoot",
	description = T(136223099210, --[[ModItemScenarioDef PlanetaryFoothold description]] "A group of pioneers is sent to an alien planet where they must construct and defend a core communication relay which will facilitate future colonization.\n\nUp to 8 team members are dropped on the unexplored world to prove it may become a new home for humanity. While they can have support of the trading ships passing nearby, if they succeed constructing a trading pod, their mission to prove the planet's viability for colonization won't be easy...\n\n<color TextEmphasis>Their mission</color>: Build Ansible Relay #499 and establish communications"),
	display_name = T(686765469297, --[[ModItemScenarioDef PlanetaryFoothold display_name]] "Planetary Foothold"),
	display_name_caps = T(528883199152, --[[ModItemScenarioDef PlanetaryFoothold display_name_caps]] "PLANETARY FOOTHOLD"),
	effects = {
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Construction",
			LockState = "hidden",
			PresetId = "Trading",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "BuildingCompositeDef",
			Group = "Structures",
			LockState = "hidden",
			PresetId = "MilitaryWonder",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Power",
			LockState = "hidden",
			PresetId = "MechCores",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Defense",
			LockState = "hidden",
			PresetId = "SmallMech",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Defense",
			LockState = "hidden",
			PresetId = "CombatMech",
		}),
		PlaceObj('ExecuteCode', {
			Code = function (self, obj)
				AddPresetLockStateReason("Tech", "Power", "MatterGenerators", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "IndustrialGenerator", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "IndustrialGenerator_Fuel", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "IndustrialGenerator_Silicon", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "FoodGenerating", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "FoodGenerating_Tasty", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "FoodGenerating_Chefs", "hidden", "PlanetaryFoothold")
			end,
		}),
		PlaceObj('ExecuteCode', {
			Code = function (self, obj)
				AddPresetLockStateReason("Resource", "Other", "PlanetaryOwnership", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Resource", "Pseudo", "PlanetaryOwnership", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Resource", "Default", "PlanetaryOwnership", "hidden", "PlanetaryFoothold")
			end,
		}),
	},
	id = "PlanetaryFoothold",
	msg_reactions = {
		PlaceObj('MsgReaction', {
			Event = "GameStarted",
			Handler = function (self)
				if self.id ~= Game.scenario then
					return
				end
				for _, survivor in ipairs(AllSurvivors) do
					survivor:SetTrait("FarmOwner", false, "forced")
				end
			end,
		}),
	},
	new_in = "LukeH",
}),
PlaceObj('ModItemScenarioDef', {
	DecreasePopMax = 8,
	DecreasePopMin = 6,
	GameLoseConds = {
		PlaceObj('CheckExpression', {
			Expression = function (self, player) return next(SpawnedSurvivorIds) and LiveSurvivorsCount() == 0 end,
			Params = "self, player",
		}),
	},
	GameLoseText = T(679531327245, --[[ModItemScenarioDef PlanetaryFoothold_allTags GameLoseText]] "The outpost has fallen. Mission failed. "),
	GameWinConds = {
		PlaceObj('PickFromLabel', {
			Filters = {
				PlaceObj('CheckExpression', {
					Expression = function (self, obj) return obj.communication_established end,
				}),
			},
			Label = "MilitaryWonder",
		}),
	},
	GameWinText = T(840985095495, --[[ModItemScenarioDef PlanetaryFoothold_allTags GameWinText]] "We proved colonization is possible. Mission successful. "),
	Image = "Mod/LH_PlanetaryFoothold/UI/Scenarios/PlanetaryFoothold.png",
	IncreasePopMax = 12,
	IncreasePopMin = 10,
	IntroClouds = true,
	KeepPlayingOnWin = true,
	LandingScene = function (self)
		Presets.ScenarioDef.Default.MilitaryOutpost.LandingScene(self)
	end,
	ScenarioTags = set( "CrashLanding", "Foothold", "MilitaryOutpost", "Robots", "Trading" ),
	SortKey = 1401,
	StartPrefabTags = set( "Farming" ),
	StartTime = 320000,
	StartingCharacters = 8,
	StartingLootTemplate = "Trading_StartingLoot",
	description = T(973061638579, --[[ModItemScenarioDef PlanetaryFoothold_allTags description]] 'Same as Planetary Foothold in terms of startup setup and win condition but also adds the scenario "tags" of Crash Landing and Guardians.\n\nThis means that elements of those scenarios would popup during game play, like having Hope available and eventually (probably) the escape pod.\n\nAs I\'m looking to include such elements in the Planetary Foothold, this is for testing purposes in order to figure out what needs to be tuned if I do so.'),
	display_name = T(523402335078, --[[ModItemScenarioDef PlanetaryFoothold_allTags display_name]] "Planetary Foothold (experimental)"),
	display_name_caps = T(474927992041, --[[ModItemScenarioDef PlanetaryFoothold_allTags display_name_caps]] "PLANETARY FOOTHOLD (EXPERIMENTAL)"),
	effects = {
		PlaceObj('RemoveLockedState', {
			Class = "Recipe",
			Group = "Workbench",
			LockState = "hidden",
			PresetId = "RepairParts",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "BuildingCompositeDef",
			Group = "Structures",
			LockState = "hidden",
			PresetId = "MilitaryWonder",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Construction",
			LockState = "hidden",
			PresetId = "Trading",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Resources",
			LockState = "hidden",
			PresetId = "SilicaEnergel",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Power",
			LockState = "hidden",
			PresetId = "MechCores",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Defense",
			LockState = "hidden",
			PresetId = "SmallMech",
		}),
		PlaceObj('RemoveLockedState', {
			Class = "Tech",
			Group = "Defense",
			LockState = "hidden",
			PresetId = "CombatMech",
		}),
		PlaceObj('ExecuteCode', {
			Code = function (self, obj)
				AddPresetLockStateReason("Tech", "Power", "MatterGenerators", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "IndustrialGenerator", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "IndustrialGenerator_Fuel", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "IndustrialGenerator_Silicon", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "FoodGenerating", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "FoodGenerating_Tasty", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Tech", "Power", "FoodGenerating_Chefs", "hidden", "PlanetaryFoothold")
			end,
		}),
		PlaceObj('ExecuteCode', {
			Code = function (self, obj)
				AddPresetLockStateReason("Resource", "Other", "PlanetaryOwnership", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Resource", "Pseudo", "PlanetaryOwnership", "hidden", "PlanetaryFoothold")
				AddPresetLockStateReason("Resource", "Default", "PlanetaryOwnership", "hidden", "PlanetaryFoothold")
			end,
		}),
	},
	id = "PlanetaryFoothold_allTags",
	msg_reactions = {
		PlaceObj('MsgReaction', {
			Event = "GameStarted",
			Handler = function (self)
				if self.id ~= Game.scenario then
					return
				end
				for _, survivor in ipairs(AllSurvivors) do
					survivor:SetTrait("FarmOwner", false, "forced")
				end
			end,
		}),
	},
	new_in = "LukeH",
}),
}
